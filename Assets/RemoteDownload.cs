using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;
using Managers;
using UnityEngine.Events;

namespace Data.Scripts
{
    [System.Serializable]
    public class AssetReferenceGameScript : AssetReferenceT<GameScript>
    {
        [SerializeField]
        public AssetReferenceGameScript(string guid) : base(guid) { }
    }
    public class RemoteDownload : MonoBehaviour
    {
        GameScript obj;

        [SerializeField]
        private AssetReferenceGameScript[] references;
        [SerializeField]
        private AssetReferenceGameScript currGamescriptRef;

        public AssetReferenceGameScript dependencies;

        private List<object> _updateKeys = new List<object>();

        public Text log;
        private void Start()
        {
            Addressables.InitializeAsync().Completed += onDownloadCompleted;
            log.text = "initializing download";
        }

        public async void UpdateCatalog()
        {
            //Initialize Addressable
            var init = Addressables.InitializeAsync();
            await init.Task;

            //Start connecting to the server to check for updates
            var handle = Addressables.CheckForCatalogUpdates(false);
            await handle.Task;
            Debug.Log("check catalog status " + handle.Status);
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                List<string> catalogs = handle.Result;
                if (catalogs != null && catalogs.Count > 0)
                {
                    foreach (var catalog in catalogs)
                    {
                        Debug.Log("catalog  " + catalog);
                    }
                    Debug.Log("download catalog start ");
                    var updateHandle = Addressables.UpdateCatalogs(catalogs, false);
                    await updateHandle.Task;
                    foreach (var item in updateHandle.Result)
                    {
                        Debug.Log("catalog result " + item.LocatorId);
                        foreach (var key in item.Keys)
                        {
                            Debug.Log("catalog key " + key);
                        }
                        _updateKeys.AddRange(item.Keys);
                    }
                    log.text = "download catalog finish " + updateHandle.Status;
                }
                else
                {
                    log.text = "dont need update catalogs";
                }
            }
            Addressables.Release(handle);
        }

        public void DownLoad()
        {
            StartCoroutine(DownAssetImpl());
        }

        public IEnumerator DownAssetImpl()
        {
            var downloadsize = Addressables.GetDownloadSizeAsync(_updateKeys);
            yield return downloadsize;
            log.text = "start download size :" + downloadsize.Result;

            if (downloadsize.Result > 0)
            {
                var download = Addressables.DownloadDependenciesAsync(_updateKeys, Addressables.MergeMode.Union);
                yield return download;
                //await download.Task;
                Debug.Log("download result type " + download.Result.GetType());
                foreach (var item in download.Result as List<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>)
                {
                    var ab = item.GetAssetBundle();
                    Debug.Log("ab name " + ab.name);
                    foreach (var name in ab.GetAllAssetNames())
                    {
                        log.text = "\nasset name " + name;
                    }
                }
                Addressables.Release(download);
            }
            Addressables.Release(downloadsize);
        }
        private void onDownloadCompleted(AsyncOperationHandle<IResourceLocator> obj)
        {
            log.text = "ready to download";
            // newScript.LoadAssetAsync<GameScript>().Completed += (clip) =>
            //{

            //    log.text = "esto pasa";
            //    log.text = clip.Result.Brief;
            //};
        }
        public void CheckDownload(int currIndex, UnityEvent callback)
        {
            bool alreadyDownloaded = (PlayerPrefs.GetInt(currIndex.ToString()) == 1);
            if (alreadyDownloaded)
            {
                //obj =references[DataManager.main.GetCurrScript().scriptIndex].
                //DataManager.main.ReplaceRemoteScript(obj);
                //DataManager.main.SetQuestionsData();
                currGamescriptRef = references[currIndex];
                Debug.LogError(DataManager.main.GetCurrScript().scriptIndex);
                //TryToLoadScriptAuto(1,callback);
                StartCoroutine(LoadScriptAuto(callback, DataManager.main.GetCurrScript().scriptIndex));
            }
        }
        public void CheckDownload2(int currIndex, UnityEvent callback)
        {
            StartCoroutine(LoadScriptAuto(callback, DataManager.main.GetCurrScript().scriptIndex));
        }
        public void TryToLoadScriptAuto(int currIndex, UnityEvent callback)
        {
            currGamescriptRef = references[currIndex];
            StartCoroutine(DownloadDependencies(callback));
        }
        public void TryToLoadScript()
        {
            //StartCoroutine(DownloadDependencies());
        }
        public void GetLoadedText()
        {
            log.text = "script \n";

            log.text += obj.Brief;
            log.text += obj.Duration;
        }
        IEnumerator LoadScriptAuto(UnityEvent callback, int currIndex)
        {
            log.text = "loading auto script";
            //Load a GameObject
            AsyncOperationHandle<GameScript> goHandle = currGamescriptRef.LoadAssetAsync<GameScript>();
            yield return goHandle;
            if (goHandle.Status == AsyncOperationStatus.Succeeded)
            {
                callback.Invoke();
                obj = goHandle.Result;
                //etc...
                log.text = obj.Brief;
                PlayerPrefs.SetInt(currIndex.ToString(), 1);
                DataManager.main.ReplaceRemoteScript(obj);
                DataManager.main.SetQuestionsData();
            }
        }
        IEnumerator LoadDependenciesScriptAuto()
        {
            log.text = "loading auto script";
            //Load a GameObject
            AsyncOperationHandle<GameScript> goHandle = currGamescriptRef.LoadAssetAsync<GameScript>();
            yield return goHandle;
            if (goHandle.Status == AsyncOperationStatus.Succeeded)
            {
                obj = goHandle.Result;
                //etc...
                log.text = obj.Brief;
                //DataManager.main.ReplaceRemoteScript(obj);

                //DataManager.main.SetQuestionsData();
            }
        }
        public void TryToGetDependencies()
        {
            StartCoroutine(LoadDependenciesScriptAuto());
        }
        public IEnumerator DownloadDependencies(UnityEvent callback)
        {
            // Clear all cached AssetBundles
            // WARNING: This will cause all asset bundles to be re-downloaded at startup every time and should not be used in a production game
            // Addressables.ClearDependencyCacheAsync(key);

            //Check the download size
            AsyncOperationHandle<long> getDownloadSize = Addressables.GetDownloadSizeAsync(currGamescriptRef);
            yield return getDownloadSize;

            //If the download size is greater than 0, download all the dependencies.
            if (getDownloadSize.Result > 0)
            {
                AsyncOperationHandle downloadDependencies = Addressables.DownloadDependenciesAsync(currGamescriptRef);
                yield return downloadDependencies;
                log.text = "dependencies downloaded";
                StartCoroutine(LoadScriptAuto(callback, DataManager.main.GetCurrScript().scriptIndex));
            }

            //...
        }


        public void SetReferencesTest()
        {
            DataManager.main.ReplaceRemoteScript(obj);

            DataManager.main.SetQuestionsData();
        }
        IEnumerator LoadScript()
        {
            log.text = "trying to load script";
            //Load a GameObject
            AsyncOperationHandle<GameScript> goHandle = Addressables.LoadAssetAsync<GameScript>("MwAAAAALAAAAQmlsbGluZ01vZGUAIAAAADMxNDBmNjQyMTI2MGE5YTQwYWFlZTFkZDYwMzZhZGZhABAAAABGYWNlYm9va1NldHRpbmdzACAAAAA5N2Q0ZDUxOThmNjllODg0ZTgxMzllMmYwMGY2NmYwMQATAAAARmluZ2Vyc1NjcmlwdFByZWZhYgAgAAAAN2QwYmNmZTVlY2RiNGUwNGE4ZGZhYmZjMGJkMWMzMmQAFAAAAFBob3RvblNlcnZlclNldHRpbmdzACAAAABiOGY2NTIwOTc0Y2VmNmE0NWE0OGEzMjRkYTRmMjAzNwALAAAAQmlnQXN0ZXJvaWQAIAAAADE1ZjQzMzQzYmEwYTYwZTQ3YTYzZDI4NDk2MzczYzNmAA0AAABTbWFsbEFzdGVyb2lkACAAAAAxOGE3OWFlYTQ5YjYwZDM0MmI3YTY3MmI3YTM0ZWRiZQAJAAAAU3BhY2VzaGlwACAAAAA3NzVkZDBhMTVhZDI1YTU0NGIzM2FkNDBlZjBkYTFmMAARAAAAUGxheWVyIENvbnRyb2xsZXIAIAAAAGM5MjQ4ZDhlNTgyOTM0NzNiYjI3Y2YyZjU1MDVmODhiABQAAABNeSBSb2JvdCBLeWxlIC1kb25lLQAgAAAAZmU3NjQxMDJhYTVhYTQzMTc5NWRhMTVmYTJkZWMwZTIAFAAAAEVzc2VudGlhbEtpdFNldHRpbmdzACAAAAA3YmVkNjM4MmE5N2FkODM0NDkxYmMzODFmNzhhYmYwNwAJAAAAUmF0ZU15QXBwACAAAAA3MGIxODcwN2NjM2M4NGEzYWI3ZDE5OGUxYzk5ZTcyMAAhAAAATGluZUJyZWFraW5nIEZvbGxvd2luZyBDaGFyYWN0ZXJzACAAAABmYWRlNDJlOGJjNzE0YjAxOGZhYzUxM2MwNDNkMzIzYgAfAAAATGluZUJyZWFraW5nIExlYWRpbmcgQ2hhcmFjdGVycwAgAAAAZDgyYzFiMzFjN2U3NDIzOWJmZjEyMjA1ODU3MDdkMmIADAAAAFRNUCBTZXR0aW5ncwAgAAAAM2Y1YjVkZmY2N2E5NDIyODlhOWRlZmE0MTZiMjA2ZjMAMgAAAEZvbnRzICYgTWF0ZXJpYWxzL0xpYmVyYXRpb25TYW5zIFNERiAtIERyb3AgU2hhZG93ACAAAABlNzNhNThmNmUyNzk0YWU3YjFiN2U1MGI3ZmI4MTFiMAAvAAAARm9udHMgJiBNYXRlcmlhbHMvTGliZXJhdGlvblNhbnMgU0RGIC0gRmFsbGJhY2sAIAAAADJlNDk4ZDFjODA5NDkxMDQ3OWRjM2UxYjc2ODMwNmE0AC4AAABGb250cyAmIE1hdGVyaWFscy9MaWJlcmF0aW9uU2FucyBTREYgLSBPdXRsaW5lACAAAAA3OTQ1OWVmZWMxN2E0ZDAwYTMyMWJkY2MyN2JiYzM4NQAkAAAARm9udHMgJiBNYXRlcmlhbHMvTGliZXJhdGlvblNhbnMgU0RGACAAAAA4ZjU4NjM3OGI0ZTE0NGE5ODUxZTdiMzRkOWI3NDhlZQAWAAAAU3ByaXRlIEFzc2V0cy9FbW9qaU9uZQAgAAAAYzQxMDA1YzEyOWJhNGQ2NjkxMWI3NTIyOWZkNzBiNDUAIAAAAFN0eWxlIFNoZWV0cy9EZWZhdWx0IFN0eWxlIFNoZWV0ACAAAABmOTUyYzA4MmNiMDM0NTFkYWVkM2VlOTY4YWM2YzYzZQAHAAAAY29uc29sYQAgAAAAYjhhZjU4ODIwZjY5YjQ3NGU4ZjYzZGM0MGE3MDJmYmMACQAAAERvd25BcnJvdwAgAAAAM2UzNWU1NjMzMGYzZGJhNDJhZjkyNDRhMmE1ZTc1ZmYABQAAAFJlc2V0ACAAAABmYTQyNTRkY2NhYTFiZmQ0OGI3YTM3MjhhNDBhNTdhZgAHAAAAVXBBcnJvdwAgAAAAMWVhN2UwNDYyZDFmNjc4NDVhZjMwOTBhZTg1YjkxY2IACwAAAExvZ2luU2NyZWVuACAAAAAwN2E4MWI2MDU4NGZlODk0YzgwMjQ0YWRjYjg1ODZiNwQAAAAA"); ;
            yield return goHandle;
            if (goHandle.Status == AsyncOperationStatus.Succeeded)
            {
                GameScript obj = goHandle.Result;
                //etc...
                log.text = obj.Brief;
            }
        }
    }
}
