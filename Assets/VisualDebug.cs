using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualDebug : Singleton<VisualDebug>
{
    // Start is called before the first frame update
    public void SetScreenText(string _text)
    {
        GetComponent<Text>().text = _text;
    }
}
