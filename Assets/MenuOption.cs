using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class MenuOption : MonoBehaviour
{
    public int optionID;
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => Selected());
    }
    // Start is called before the first frame update
    public void Selected()
    {
        MenuOptionsHandler.main.OnInteraction(optionID);
        GetComponentInChildren<TextMeshProUGUI>().color = new Color32(235, 191, 92, 255);
    }
    public void UnSelected()
    {
        GetComponentInChildren<TextMeshProUGUI>().color = Color.white;

    }
}
