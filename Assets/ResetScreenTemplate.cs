using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Managers;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UIElements;

public class ResetScreenTemplate : ScreenTemplate
    {
        private Coroutine _rutine;
        public override void Init()
        {
            base.Init();
            // reinicio boton verde
         
            DebugMaster.debugInstance.AddMesaggeToConsole("Se abre resetScreen");
          //  PunManager.Instance.ReConnectToPun();
          gameObject.SetActive(true);
          VivoxVoiceManager.Instance.Logout();
        _rutine =  StartCoroutine("timer");
        }

        public override void Close()
        {
            Debug.Log("se cierra ");
            base.Close();
            StopTimer();
        }

        private void StopTimer()
        {
            if(_rutine != null) StopCoroutine(_rutine);
        }

        
        
        IEnumerator timer()
        {
            bool Internet =false;

            while (!Internet)
            {
                UnityWebRequest www = UnityWebRequest.Get("http://google.com");
                
                yield return www.SendWebRequest();
                if (www.error != null)
                {
                    Debug.Log("google false");
                    Internet = false; 
                } 
                else 
                {
                    Debug.Log("google true");
                    Internet =   true;
                }
                yield return new WaitForSeconds(5f);
            }
               
            
            Debug.Log("inicia corutina");
            if(!Internet)
            {
                    Debug.Log("no hay internet");
                    yield return new WaitForSeconds(5f); 
            }

            if (Internet) 
            {
                    Debug.Log("si hay internet");
                while (!PhotonNetwork.IsConnected) 
                {
                        yield return new WaitForSeconds(3f);
                        Debug.Log("try");
                        Button();
                        yield return new WaitForSeconds(3f);
                        Debug.Log("try to reconnect again");
                }
                if (PhotonNetwork.IsConnected) 
                {
                       
                    // se apagaba 
                 //  
                 Debug.Log("se manda a apagar la pantalla");
                        Button();
                        StopTimer();
                        ScreenManager.main.CloseScreen(SerializableScreen.Type.ResetScreen);
                        yield break;
                }
                yield return new WaitForSeconds(2f); 
            }
        }
        public void Button()
        {
           // Application.Quit(); 
           // PunManager.Instance.CheckInternet();
           PunManager.Instance.ReConnectToPun();
          
        }
        
        
        
    }

