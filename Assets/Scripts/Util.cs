using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Util : MonoBehaviour
{
    public static Util utilInstance;

    public List<Button> buttons = new List<Button>();

    private void Awake()
    {
        if (utilInstance == null)
        {  
            utilInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }


    public void ActiveButton(string active)
    {
        buttons[FindInButtonList(active)].interactable = true;
    }
    public void DisableButton(string disable)
    {
        buttons[FindInButtonList(disable)].interactable = false;
    }

    private int FindInButtonList(string buttonName)
    {
        foreach (Button item in buttons)
        {
            if (item.name == buttonName)
            {
                return buttons.IndexOf(item);
            }
        }
        return 0;
    }
}
