using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Managers;
using Utils.ScreensUtils;
using Screens;
using UnityEngine.UI;

public class SearchCluesOrganizer : MonoBehaviour
{
    [Header("Setup")]
    [SerializeField] private TMP_InputField input;
    [SerializeField] private Transform clueContent;
    [SerializeField] private Transform placeContent;
    [SerializeField] private OrganizerClue cluePrefab;
    [SerializeField] private Button clearTextBtn; 

    [SerializeField] CluesOrganizer organizer;

    public void SearchClueDyanmicly()
    {
        foreach (Transform item in clueContent)
        {
            Destroy(item.gameObject);
        }
        foreach (Transform item in placeContent)
        {
            Destroy(item.gameObject);
        }
        if (input.text == string.Empty) return;

        string value = input.text.ToLower();
        string result = "";
        if (DataManager.main.CluesFound.Length > 0)
        {
            organizer.SpawnClueCategory("Found clues", placeContent, "Search results");
        }
        foreach (var item in DataManager.main.CluesFound)
        {
            result = item.Description.ToLower();
            if (result.Contains(value))
            {           
                result = result.Replace(value, $"<mark=yellow>{value}</mark>");
                var clue = Instantiate(cluePrefab, clueContent);
                clue.Setup(item, result);
            }
        }
    }

    private void ClearText() => input.text = "";
    private void Awake()
    {
        clearTextBtn.onClick.AddListener(ClearText);
    }
}
