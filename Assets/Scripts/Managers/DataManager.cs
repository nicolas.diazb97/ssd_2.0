﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Data;
using Data.Scripts;
using Newtonsoft.Json;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using WebSocketSharp;
using Managers;


namespace Managers
{
    [Serializable]
    public class SavedPlayer
    {
        public string id;
        public string nickname;
        public Sprite profilePicture;
        public Image imagenProfile;
        public void llenado(Sprite sprite)
        {
            profilePicture = sprite;
            Consulta();
        }

        public void Consulta()
        {
            try
            {
                imagenProfile.sprite = profilePicture;
            }
            catch (Exception e)
            {

                Debug.Log(e.Message);
            }

        }
    }
    [Serializable]
    public class AssignationInfo
    {
        public string PlayerId;
        public GameScript.Character AssignedCharacter;
        public bool IsReady;
    }
    public class CharacterAssignation
    {
        public string Character;
        public string PlayerId;
    }
    public class FoundClue
    {
        public string ClueName;
        public string FoundByPlayer;
    }
    public class ReconnectionInfo
    {
        public FoundClue[] FoundClues;
        public CharacterAssignation[] SessionAssignations;
        public int CurrentRoundIndex;
    }
    [Serializable]
    public struct Assignation
    {
        public string name;
        public int votes;
    }
    [Serializable]
    public struct ReconnectionData
    {
        public List<SerializedResult> results;
        public List<AddVoteData> allVotes;
        public Assignation[] assignations;
        public int totalVotes;
    }
    public class PrivateRoomInfo
    {
        public string RoomSuffix;
        public List<string> PlayersInRoom = new List<string>();
    }
    public class DataManager : Singleton<DataManager>
    {
        [SerializeField] private List<GameScript> availableScripts;
        [SerializeField] private GameObject votingResult;
        [SerializeField] private GameObject evaluationScreen;
        [SerializeField] private GameObject GreenArrowExit;
        [SerializeField] private GameObject truthButton;
        public GameScript CurrentRoomScript { get; private set; }

        public IEnumerable<GameScript> AvailableScripts =>
            Debug.isDebugBuild ? availableScripts : availableScripts.Where(script => !script.IsDebugScript);

        public List<AddVoteData> AllVotes { get; set; } = new List<AddVoteData>();
        public List<string> CharactersInRoom { get; set; } = new List<string>();
        public Assignation[] Assignations;

        public Action OnMvpVote;
        public Action OnAcussedVote;
        public Action OnAllPlayersVote;
        public List<string> charactersOnScript = new List<string>();
        public int TotalVotes { get; set; }

        public void ReplaceRemoteScript(GameScript _script)
        {
            GameScript matchedScript = availableScripts.Where(scr => scr.ScriptId == _script.ScriptId).First();
            int index = availableScripts.IndexOf(matchedScript);
            availableScripts[index] = _script;
            //availableScripts.Add(_script);
            var scriptId = PhotonNetwork.CurrentRoom.CustomProperties[PunManager.ScriptKey].ToString();
            CurrentRoomScript = GetScriptById(int.Parse(scriptId));
        }
        public Question[] GetFullQuestions()
        {
            List<Question> tempQuestions = CurrentRoomScript.Questions.ToList();
            foreach (var item in CharacterAssigned.personalQuestions)
            {
                tempQuestions.Add(item);
            }
            return tempQuestions.ToArray();
        }
        public override void Awake()
        {
            base.Awake();
            PunManager.Instance.OnLeaveRoom += OnLeaveRoom;
            PunManager.Instance.OnEnterRoom += OnEnterRoom;
            PunManager.Instance.OnPlayerLeft += OnPlayerLeftRoom;
            PunManager.Instance.OnClueFoundByPlayer += HandleClueFound;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            PunManager.Instance.OnLeaveRoom -= OnLeaveRoom;
            PunManager.Instance.OnEnterRoom -= OnEnterRoom;
            PunManager.Instance.OnPlayerLeft -= OnPlayerLeftRoom;
            PunManager.Instance.OnClueFoundByPlayer -= HandleClueFound;
        }

        [Pure]
        public GameScript GetScriptById(int searchId)
        {
            var script = availableScripts.SingleOrDefault(script => script.ScriptId == searchId);

            if (script) return script;

            Debug.LogError($"No script identified with {searchId} was found.");
            return null;
        }
        public GameScript GetCurrScript()
        {
            var scriptId = PhotonNetwork.CurrentRoom.CustomProperties[PunManager.ScriptKey].ToString();
            Debug.Log("el script id de la reconect es  " + scriptId);
            CurrentRoomScript = GetScriptById(int.Parse(scriptId));
            return CurrentRoomScript;
        }
        public void AddMVPVote(string data)
        {
            Assignation assignation = JsonUtility.FromJson<Assignation>(data);
            Debug.LogError("saving assign" + assignation);

            for (int i = 0; i < Assignations.Length; i++)
            {
                if (Assignations[i].name == assignation.name)
                {
                    Assignations[i].votes += assignation.votes;
                    OnMvpVote?.Invoke();
                    PunManager.Instance.UpdateResultVotes();
                    return;
                }
            }
        }

        [ContextMenu("Clear Assignations")]
        public void ClearAssingnations()
        {
            Assignations = new Assignation[0];
            //charactersOnScript.Clear();
        }
        public void GeAssignations()
        {
            Assignations = new Assignation[PhotonNetwork.PlayerList.Length];

            for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
            {
                Assignations[i].name = PhotonNetwork.PlayerList[i].CustomProperties["Character"] as string;
            }
        }

        public Assignation GetMostVotedForMvp()
        {
            Assignation mostVoted = Assignations[0];
            bool draw = false;
            for (int i = 1; i < Assignations.Length; i++)
            {
                if (Assignations[i].votes == mostVoted.votes)
                {
                    draw = true;
                    continue;
                }
                if (Assignations[i].votes > mostVoted.votes)
                {
                    draw = false;
                    mostVoted = Assignations[i];
                }
            }

            return draw || mostVoted.votes == 0 ? new Assignation() { name = "Draw!" } : mostVoted;
        }

        public void GetPlayerPfpById(string id, Image playerreference)
        {
            var selectedPlayer = _players.FirstOrDefault(player => player.id == id);

            if (selectedPlayer != null)
            {
                selectedPlayer.imagenProfile = playerreference;
                selectedPlayer.Consulta();
            }

            // Debug.LogError($"No player matches id {id}");
        }

        [Pure]
        public string GetPlayerNameById(string playerId)
        {
            var selectedPlayer = _players.FirstOrDefault(player => player.id == playerId);
            if (selectedPlayer != null) return selectedPlayer.nickname;

            Debug.LogError($"No player matches id {playerId}");
            return null;
        }

        public string GetAnswerByCharacter(string voter, int index)
        {
            AddVoteData voteData = AllVotes.Find(v => v.voter == voter && v.index == index);
            return voteData != null ? voteData.accusedName : "does not vote";
        }

        public AnswerResutl GetMostVotedAnswer(string name)
        {
            Result result = results.Find(r => r.QuestionName == name);
            AnswerResutl mostVoted = result.Answers.OrderByDescending(a => a.Votes.Count).First();
            return mostVoted;
        }

        #region Clues System

        public List<Clue> _foundClues = new List<Clue>();
        private List<Clue> _foundCluesFromAssets = new List<Clue>();

        public event Action<Clue[]> OnClueFound;

        public Clue[] CluesFound => _foundClues.ToArray();
        private Clue[] CluesFoundFromAssets => _foundCluesFromAssets.ToArray();

        public void AddFoundClue(Clue clueFound)
        {
            Debug.Log("foundclue");
            if (_foundClues.Contains(clueFound)) return;

            _foundClues.Add(clueFound);
            OnClueFound?.Invoke(CluesFound);
        }

        public void ResetClues()
        {
            _foundClues = new List<Clue>();
            _foundCluesFromAssets = new List<Clue>();

        }

        private void HandleClueFound(string clueName, string foundByPlayer, string clueId)
        {
            var matchingClue = Resources.FindObjectsOfTypeAll<Clue>().FirstOrDefault(clue => clue.Title == clueName);
            if (!matchingClue) return;

            matchingClue.FoundBy = foundByPlayer;
            _foundCluesFromAssets.Add(matchingClue);
        }

        #endregion
        #region Result System


        private List<Result> results = new List<Result>();
        public List<Result> Results { get { return results; } set { results = value; } }

        public int GetCorrectAnswersAmount(string questionName)
        {
            Result result = Results.Find(r => r.QuestionName == questionName);
            return result.CorrectAnswersAmount();
        }
        public void AddResult(Result result)
        {
            if (results.Contains(result)) return;
            string questionName = result.QuestionName;

            for (int i = 0; i < results.Count; i++)
            {
                if (results[i].QuestionName == questionName)
                    return;
            }

            results.Add(result);
        }
        public Sprite GetCharacterPic(string characterName)
        {
            for (int i = 0; i < CurrentRoomScript.Characters.Length; i++)
            {
                if (CurrentRoomScript.Characters[i].name == characterName)
                {
                    return CurrentRoomScript.Characters[i].picture;
                }
            }
            return null;
        }
        public int CurrentAcusedId { get; set; } = 0;
        public void AddAccusedByResultIndex(string accusedData)
        {
            Debug.LogError("quejesto "+accusedData);
            AddAcusedData addAcusedData = JsonUtility.FromJson<AddAcusedData>(accusedData);
            if (AccusedAlreadyExist(addAcusedData.index, addAcusedData.accusedName)) return;

            AnswerResutl accused = new AnswerResutl
            {
                AnswerId = CurrentAcusedId++,
                Answer = addAcusedData.accusedName,
                AnswerPic = GetCharacterPic(addAcusedData.accusedName)
            };

            Results[addAcusedData.index].AddAccused(accused);
        }
        public void QuestionsClearConfirmations()
        {
            Debug.Log(nameof(QuestionsClearConfirmations));
            if (++TotalVotes >= PunManager.Instance.MaxRoomPlayers)
            {
                Debug.Log("All players voted");
                PunManager.Instance.NotifyNewRoundStarted();
            }
        }

        public void InvokeAllPlayersVoted()
        {
            OnAllPlayersVote?.Invoke();
        }

        public void AddVote(string voteData)
        {
            AddVoteData addVoteData = JsonUtility.FromJson<AddVoteData>(voteData);
            AnswerResutl accused = SearchAccusedByResultIndex(addVoteData.index, addVoteData.accusedName);

            if (accused != null)
            {
                AllVotes.Add(addVoteData);
                Vote vote = new Vote(GetCharacterPic(addVoteData.voter), addVoteData.voter);
                accused.AddVote(vote);
                OnAcussedVote?.Invoke();
            }
            PunManager.Instance.UpdateResultVotes();
        }

        public bool AccusedAlreadyExist(int index, string accusedName)
        {
            foreach (AnswerResutl result in Results[index].Answers)
            {
                if (result.Answer == accusedName)
                    return true;
            }
            return false;
        }

        public AnswerResutl SearchAccusedByResultIndex(int index, string accusedName)
        {
            AnswerResutl accusedFound = null;
            foreach (AnswerResutl accused in Results[index].Answers)
            {
                if (accused.Answer.Equals(accusedName))
                {
                    accusedFound = accused;
                }
            }
            return accusedFound;
        }
        public void SetResults()
        {
            foreach (Question q in QuestionManager.main.Questions)
            {
                Debug.LogError(q.QuestionName);
                Result r = new Result(q.QuestionName);
                AddResult(r);
            }
        }
        #endregion
        #region Player Assignations

        private readonly List<SavedPlayer> _players = new List<SavedPlayer>();

        public event Action<AssignationInfo[]> OnCharacterAssignationsUpdated;
        public AssignationInfo[] GameAssignations { get; private set; }

        public GameScript.Character CharacterAssigned
        {
            get
            {
                var localAssignation = GameAssignations.FirstOrDefault(info => info.PlayerId == PhotonNetwork.LocalPlayer.NickName);
                if (localAssignation != null) return localAssignation.AssignedCharacter;
                Debug.LogError("There is no character assigned to the local player");

                return null;

            }
        }

        public void UpdateCharacterAssignations()
        {
            var assignationInfos = new List<AssignationInfo>();

            foreach (var player in PhotonNetwork.PlayerList)
            {
                if (!player.CustomProperties.TryGetValue("Character", out var character) ||
                    !player.CustomProperties.TryGetValue("Ready", out var readyState)) continue;

                var assignedCharacter = CurrentRoomScript.GetCharacterByName(character.ToString());

                assignationInfos.Add(new AssignationInfo
                {
                    AssignedCharacter = assignedCharacter,
                    IsReady = bool.Parse(readyState.ToString()),
                    PlayerId = player.NickName
                });
            }

            OnCharacterAssignationsUpdated?.Invoke(GameAssignations = assignationInfos.ToArray());
        }

        [Pure]
        public string GetPlayerAssignedToCharacter(GameScript.Character character)
        {
            var assignedPlayer = GameAssignations.FirstOrDefault(info => info.AssignedCharacter == character);
            if (assignedPlayer != null) return assignedPlayer.PlayerId;

            Debug.LogError($"There is no player assigned to character {character.name} in the current game.");
            return null;
        }

        public void RegisterPlayer(Player newPlayer)
        {
            if (_players.Any(player => player.id == newPlayer.NickName)) return;

            var pfpUrl = newPlayer.CustomProperties["Pfp"].ToString();
            // Debug.Log($"Downloading pfp from source {pfpUrl}");
            // var storage = FirebaseStorage.DefaultInstance;
            // var screenshotReference = storage.GetReference(pfpUrl);
            // var downloadTask = screenshotReference.GetBytesAsync(long.MaxValue);
            // yield return new WaitUntil(() => downloadTask.IsCompleted);
            // var texture = new Texture2D(2, 2);
            // texture.LoadImage(downloadTask.Result);



            SavedPlayer savedPlayer = new SavedPlayer
            {
                nickname = newPlayer.CustomProperties["Nick"].ToString(),
                id = newPlayer.NickName
            };
            if (!pfpUrl.IsNullOrEmpty()) RealtimeDatabaseManager.rtdbmReference.DownloadPhoto(pfpUrl, savedPlayer);
            _players.Add(savedPlayer);
            Debug.Log($"Player registered on data manager: {newPlayer.NickName}");
        }

        public void OnPlayerReconnected(CharacterAssignation[] prevAssignations)
        {
            Debug.LogError($"Restoring previous character assignations: {JsonConvert.SerializeObject(prevAssignations)}");

            GameAssignations = prevAssignations.Select(assignation => new AssignationInfo
            {
                AssignedCharacter = PunManager.Instance.CurrentRoomScript.Characters.FirstOrDefault(character => character.name == assignation.Character),
                PlayerId = assignation.PlayerId
            }).ToArray();
            foreach (var VARIABLE in GameAssignations)
            {
                Debug.Log("character asignado" + VARIABLE.AssignedCharacter + "player id" + VARIABLE.PlayerId);
            }
            // hay que llamar todo el codigo que pide el character
            GameManager.main.EventNewRoundCall();
        }
        public void ResetCharactersOnScript()
        {
            charactersOnScript.Clear();
        }

        #endregion

        public void OnEnterRoom()
        {
            Results.Clear();
            ResetAssignations();

            PunManager.Instance.OnLeaveRoom += ResetCharactersOnScript;
            TotalVotes = 0;

            var scriptId = PhotonNetwork.CurrentRoom.CustomProperties[PunManager.ScriptKey].ToString();
            Debug.Log("el script id de la reconect es  " + scriptId);
            CurrentRoomScript = GetScriptById(int.Parse(scriptId));
            foreach (var player in PhotonNetwork.PlayerList)
            {
                RegisterPlayer(player);
            }
            UpdateCharacterAssignations();
        }
        public void SetQuestionsData()
        {
            Debug.LogError("setting questions data");
            QuestionManager.main.SetQuestions(GetFullQuestions());
            SetResults();
        }
        #region Callback Handles

        private void OnPlayerLeftRoom(Player leftPlayer)
        {
            _players.RemoveAll(player => player.id == leftPlayer.NickName);
            UpdateCharacterAssignations();
        }

        private void OnLeaveRoom()
        {
            CurrentRoomScript = null;
            GameAssignations = null;
            Results = new List<Result>();
            _players.Clear();
        }

        /* public string[] ScriptTexts()
          {
              var Script = PunManager.Instance.CurrentRoomScript;

              var background = Script.Background;
              var characterstory = CharacterAssigned.personalStory;

              //return new []{background.ToString(),characterstory};
          }
         */
        public GameScript.ParagraphStruct[] BackgroundText()
        {
            var Script = PunManager.Instance.CurrentRoomScript;
            return Script.Background;
        }
        public string BriefText()
        {
            var Script = PunManager.Instance.CurrentRoomScript;
            return Script.Brief.ToString();
        }
        public GameScript.ParagraphStruct[] PersonalStoryText()
        {
            //return  CharacterAssigned.personalStory;
            return CharacterAssigned.personalStory;
        }
        public GameScript.ParagraphStruct[] PersonaltodayText()
        {
            //return  CharacterAssigned.personalStory;
            return CharacterAssigned.today;
        }
        public GameScript.ParagraphStruct[] Truth()
        {
            var Script = PunManager.Instance.CurrentRoomScript;
            return Script.Truth;
        }


        #endregion
        #region Reconnection Logic

        public UnityEvent OnReconnectionDone;

        [ContextMenu("TrySaveData")]
        public void TrySaveData()
        {
            Debug.Log(GetSavedData());
        }

        public string GetSavedData()
        {
            ReconnectionData data = new ReconnectionData
            {
                totalVotes = TotalVotes,
                assignations = Assignations,
                allVotes = AllVotes
            };

            List<SerializedResult> serializedResults = new List<SerializedResult>();

            for (int i = 0; i < Results.Count; i++)
            {
                serializedResults.Add(Results[i].CreateSerialized());
            }

            data.results = serializedResults;
            string json = JsonUtility.ToJson(data);
            Debug.Log($"JSON: {json}");
            return json;
        }

        [SerializeField] ReconnectionData currentData;
        public void LoadData(string data)
        {

            if (!string.IsNullOrEmpty(data))
            {
                ClearAssingnations();
                ReconnectionData parsedData = JsonUtility.FromJson<ReconnectionData>(data);

                currentData = parsedData;
                TotalVotes = parsedData.totalVotes;

                Assignations = parsedData.assignations;
                AllVotes = new List<AddVoteData>();
                AllVotes.AddRange(parsedData.allVotes);
                Results.Clear();
                for (int i = 0; i < parsedData.results.Count; i++)
                {
                    Results.Add(new Result());
                    Debug.Log($"<color=green>{parsedData.results[i].questionName}</ color>");
                    Results[i].CreateFromSerialized(parsedData.results[i]);
                }

                GameManager.main.HasVoted = true;
            }
            else
            {
                Debug.LogError("reseting");
                Results.Clear();
                ResetAssignations();
                TotalVotes = 0;
                SetResults();
            }

            OnReconnectionDone?.Invoke();
            if (GameManager.main.CurrentRound == Round.Results)
            {
                votingResult.gameObject.SetActive(true);
                evaluationScreen.gameObject.SetActive(true);
                GreenArrowExit.gameObject.SetActive(true);
                truthButton.gameObject.SetActive(true);
            }

        }


        private void ResetAssignations()
        {
            ClearAssingnations();
            GeAssignations();
        }

        #endregion

    }
}
