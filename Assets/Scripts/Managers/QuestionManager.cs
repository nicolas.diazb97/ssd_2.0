using System;
using Managers;
using UnityEngine;
using System.Linq;

public class QuestionManager : Singleton<QuestionManager>
{
    [SerializeField] private InGameQuestions inGameQuestionsScreen;

    private Question[] questions;
    private Question currentQuestion;
    public int questionIndex;
    public Action<int> OnSubmitQuestion;

    public Question CurrentQuestion { get { return currentQuestion; } }
    public Question[] Questions { get { return questions; } }

    public Question GetQuestion(string questionName) => questions.FirstOrDefault(q => q.QuestionName == questionName);

    public bool IsLastQuestion => questionIndex == questions.Length - 1;

    public void SetQuestions(Question[] questions)
    {
        foreach (Question question in questions)
        {
            question.WasCorrect = false;
            Debug.Log("q " + question.QuestionName);
        }
        questionIndex = 0;
        currentQuestion = questions[questionIndex];
        this.questions = questions;
        //DataManager.main.GetCurrScript().Questions = this.questions;
        Debug.LogError("questions setted");
    }

    public void CheckAnswer(int index)
    {
        currentQuestion.WasCorrect = currentQuestion.CorretAnswerIndex == index;
        if (!IsLastQuestion)
        {
            questionIndex++;
            currentQuestion = questions[questionIndex];
        }
        else
        {
        Debug.LogError("last q");
            inGameQuestionsScreen.OnLastQuestion();
            GameManager.main.HasVoted = true;
        }
    }
}
