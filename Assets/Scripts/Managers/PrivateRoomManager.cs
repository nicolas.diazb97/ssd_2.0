﻿using UnityEngine;
using UnityEngine.Events;

namespace Managers
{
    public class PrivateRoomManager : Singleton<PrivateRoomManager>
    {
        [SerializeField] private UnityEvent onJoinedPrivateRoom;
        [SerializeField] private UnityEvent onLeftPrivateRoom;
        
        public override void Awake()
        {
            base.Awake();
            GameManager.main.OnNewRoundStarted += HandleNewRoundStarted;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            GameManager.main.OnNewRoundStarted -= HandleNewRoundStarted;
        }

        private void HandleNewRoundStarted(RoundConfig round)
        {
            if (round.IsSearchRound) return;
            var voiceManager = VivoxVoiceManager.Instance;
            if (!voiceManager.CurrentChannelId.Contains("_")) return;
            LeavePrivateRoom();
        }
        
        public void JoinPrivateRoom(string roomSuffix, SpriteRenderer roomMap)
        {
            VivoxVoiceManager.Instance.JoinPrivateRoom(roomSuffix);
            CameraManager.main.MapRenderer = roomMap;
            onJoinedPrivateRoom?.Invoke();
        }

        
        public void LeavePrivateRoom()
        {
            VivoxVoiceManager.Instance.LeavePrivateRoom();
            CameraManager.main.MapRenderer = DataManager.main.CurrentRoomScript.SearchMap;
            onLeftPrivateRoom?.Invoke();
        }
    }
}