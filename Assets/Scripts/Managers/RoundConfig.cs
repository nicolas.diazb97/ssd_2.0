﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Managers
{
    [CreateAssetMenu(fileName = "New Round Config", menuName = "Configs/Rounds", order = 0)]
public class RoundConfig : ScriptableObject
{
    [SerializeField] private Round roundName;
    [SerializeField] private bool isSearchRound;
    [SerializeField] private UnityEvent onRoundStarted;
    [SerializeField] [TextArea(6, 10)] private string roundDescription;

    public Round RoundName => roundName;

    public UnityEvent OnRoundStarted => onRoundStarted;

    public string RoundDescription => roundDescription;

    public bool IsSearchRound => isSearchRound;

    public static string RoundEnumToString(Round round)
    {
        return round switch
        {
            Round.ScriptReading => "Script Reading",
            Round.SelfIntroduction => "Self Introduction",
            Round.SearchForClues1 => "Clue Search Round 1",
            Round.SearchForCluesU => "Clue Search",
            Round.SearchForClues2 => "Clue Search Round 2",
            Round.Discussion => "Discussion",
            Round.Voting => "Voting",
            Round.Results => "Voting Results",
            Round.Truth => "Truth",
            _ => throw new ArgumentOutOfRangeException(nameof(round), round, null)
        };
    }

}
}