using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppleAuth;
using AppleAuth.Native;
using AppleAuth.Enums;
using AppleAuth.Interfaces;
using System.Text;
using AppleAuth.Extensions;
using System;
using System.Security.Cryptography;
using Firebase.Auth;
using Firebase.Extensions;
using Firebase;
using System.Threading.Tasks;
using UnityEngine.UI;


public class AppleLogin : MonoBehaviour
{
    [HideInInspector] public string userName;
    [HideInInspector] public string email;
    [HideInInspector] public string id;
    [HideInInspector] public string phoUrl;

    private IAppleAuthManager appleAuthManager;
    private FirebaseAuth firebaseAuth;

    public Button signInButton;
    // Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

    public string AppleUserIdKey { get; private set; }
    public LoginData loginData;
    private void Awake()
    {

    }
    void Start()
    {

#if UNITY_IOS
     signInButton.gameObject.SetActive(true);
#else
        GetComponent<AppleLogin>().enabled = false;
#endif

        if (AppleAuthManager.IsCurrentPlatformSupported)
        {
            // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
            var deserializer = new PayloadDeserializer();
            // Creates an Apple Authentication manager with the deserializer
            this.appleAuthManager = new AppleAuthManager(deserializer);
            DebugMaster.debugInstance.AddMesaggeToConsole("Platform is supported: AppleID");
        }
        else
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("Platform is NOT supported: AppleID");
        }
        CheckFirebaseDependencies();
    }
    void Update()
    {

        if (this.appleAuthManager != null)
        {
            this.appleAuthManager.Update();
        }

    }
    private void CheckFirebaseDependencies()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                if (task.Result == DependencyStatus.Available)
                {
                    firebaseAuth = FirebaseAuth.DefaultInstance;// auth = FirebaseAuth.DefaultInstance;
                }
                else
                    DebugMaster.debugInstance.AddMesaggeToConsole("Could not resolve all Firebase dependencies: " + task.Result.ToString());
            }
            else
            {
                DebugMaster.debugInstance.AddMesaggeToConsole("Dependency check was not completed. Error : " + task.Exception.Message);
            }
        });
    }
    public void SignUpWithAppleId()
    {
        PerformLoginWithAppleIdAndFirebase(FirebaseStructure);

        DebugMaster.debugInstance.AddMesaggeToConsole("Trying SignUpWithAppleIdTwo");
        signInButton.interactable = false;
    }

    public void FirebaseStructure(FirebaseUser firebUser)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("Starting CallBack Firebase");

        var name = firebUser.DisplayName;
        var email = firebUser.Email;
        var photoUrl = firebUser.PhotoUrl.ToString();
        var id = firebUser.UserId;

        DebugMaster.debugInstance.AddMesaggeToConsole($"name ={name}\n email ={email}\n photoUrl ={photoUrl}\n id ={id}\n");
    }


    public void PerformLoginWithAppleIdAndFirebase(Action<FirebaseUser> firebaseAuthCallback)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("PerformLoginWithAppleIdAndFirebase()");
        var rawNonce = GenerateRandomString(32);
        var nonce = GenerateSHA256NonceFromRawNonce(rawNonce);

        var loginArgs = new AppleAuthLoginArgs(
            LoginOptions.IncludeEmail | LoginOptions.IncludeFullName,
            nonce);

        this.appleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
                var appleIdCredential = credential as IAppleIDCredential;
                if (appleIdCredential != null)
                {
                    this.PerformFirebaseAuthentication(appleIdCredential, rawNonce, firebaseAuthCallback);
                }
            },
            error =>
            {
                DebugMaster.debugInstance.AddMesaggeToConsole("Fail PerformLoginWithAppleIdAndFirebase()");
            });
    }
    public void PerformQuickLoginWithFirebase(Action<FirebaseUser> firebaseAuthCallback)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("PerformQuickLoginWithFirebase()");
        var rawNonce = GenerateRandomString(32);
        var nonce = GenerateSHA256NonceFromRawNonce(rawNonce);

        var quickLoginArgs = new AppleAuthQuickLoginArgs(nonce);

        this.appleAuthManager.QuickLogin(
            quickLoginArgs,
            credential =>
            {
                var appleIdCredential = credential as IAppleIDCredential;
                if (appleIdCredential != null)
                {
                    this.PerformFirebaseAuthentication(appleIdCredential, rawNonce, firebaseAuthCallback);
                }
            },
            error =>
            {
                DebugMaster.debugInstance.AddMesaggeToConsole("Fail PerformQuickLoginWithFirebase()");
            });
    }
    private void PerformFirebaseAuthentication(

        IAppleIDCredential appleIdCredential,
        string rawNonce,
        Action<FirebaseUser> firebaseAuthCallback)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("PerformFirebaseAuthentication()");
        var identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken);
        var authorizationCode = Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode);
        var firebaseCredential = OAuthProvider.GetCredential(
            "apple.com",
            identityToken,
            rawNonce,
            authorizationCode);

        this.firebaseAuth.SignInWithCredentialAsync(firebaseCredential)
            .ContinueWithOnMainThread(task => HandleSignInWithUser(task, firebaseAuthCallback));
    }

    private void HandleSignInWithUser(Task<FirebaseUser> task, Action<FirebaseUser> firebaseUserCallback)
    {
        if (task.IsCanceled)
        {
            Debug.Log("Firebase auth was canceled");
            DebugMaster.debugInstance.AddMesaggeToConsole("Firebase auth was canceled");
            firebaseUserCallback(null);
        }
        else if (task.IsFaulted)
        {
            Debug.Log("Firebase auth failed");
            DebugMaster.debugInstance.AddMesaggeToConsole("Firebase auth failed");
            firebaseUserCallback(null);
        }
        else
        {
            var firebaseUser = task.Result;
            firebaseUserCallback(firebaseUser);
            Debug.Log("Firebase auth completed | User ID:" + firebaseUser.DisplayName);
            DebugMaster.debugInstance.AddMesaggeToConsole("Firebase auth completed | User Name:" + firebaseUser.DisplayName);
            RestManager.main.DataSession(firebaseUser.DisplayName, firebaseUser.Email, firebaseUser.UserId, firebaseUser.PhotoUrl.ToString());
            signInButton.interactable = true;

        }
    }


    //  Debug.Log("Firebase auth completed | User ID:" + firebaseUser.UserId);
    // DebugMaster.debugInstance.AddMesaggeToConsole("Firebase auth completed | User Name:" + firebaseUser.DisplayName);
    //RestManager.restManagerInstance.DataSession(firebaseUser.DisplayName, firebaseUser.Email, firebaseUser.UserId, firebaseUser.PhotoUrl.ToString());

    private static string GenerateRandomString(int length)
    {
        if (length <= 0)
        {
            throw new Exception("Expected nonce to have positive length");
        }

        const string charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._";
        var cryptographicallySecureRandomNumberGenerator = new RNGCryptoServiceProvider();
        var result = string.Empty;
        var remainingLength = length;

        var randomNumberHolder = new byte[1];
        while (remainingLength > 0)
        {
            var randomNumbers = new List<int>(16);
            for (var randomNumberCount = 0; randomNumberCount < 16; randomNumberCount++)
            {
                cryptographicallySecureRandomNumberGenerator.GetBytes(randomNumberHolder);
                randomNumbers.Add(randomNumberHolder[0]);
            }

            for (var randomNumberIndex = 0; randomNumberIndex < randomNumbers.Count; randomNumberIndex++)
            {
                if (remainingLength == 0)
                {
                    break;
                }

                var randomNumber = randomNumbers[randomNumberIndex];
                if (randomNumber < charset.Length)
                {
                    result += charset[randomNumber];
                    remainingLength--;
                }
            }
        }

        return result;
    }
    private static string GenerateSHA256NonceFromRawNonce(string rawNonce)
    {
        var sha = new SHA256Managed();
        var utf8RawNonce = Encoding.UTF8.GetBytes(rawNonce);
        var hash = sha.ComputeHash(utf8RawNonce);

        var result = string.Empty;
        for (var i = 0; i < hash.Length; i++)
        {
            result += hash[i].ToString("x2");
        }
        return result;
    }


    private void OtherAppleId(string appleIdToken)
    {/*
        this.appleAuthManager.LoginWithAppleId(

LoginOptions.IncludeEmail | LoginOptions.IncludeFullName,

(credential) =>

{

    // success

    var appleIDCredential = credential as IAppleIDCredential;

    string authCode = System.Text.Encoding.UTF8.GetString(appleIDCredential.AuthorizationCode);

    string idToken =
    System.Text.Encoding.UTF8.GetString(appleIDCredential.IdentityToken);

    string rawNonce = _appleAuthManager.RawNonce;

    Credential credential = OAuthProvider.GetCredential("apple.com", idToken, rawNonce, authCode);

},

(error) =>

{

    Debug.LogWarning("Sign in with Apple failed: " + error.ToString());

});
        var rawNonce = GenerateRandomString(32);
        Firebase.Auth.Credential credential =
    Firebase.Auth.OAuthProvider.GetCredential("apple.com", appleIdToken, rawNonce, null);
        auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });*/
    }

}


