﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Managers
{
    public enum Round
    {
        ScriptReading, SelfIntroduction, SearchForClues1,  SearchForCluesU ,SearchForClues2, Discussion, Voting, Results, Truth
    }

    public class GameManager : Singleton<GameManager>
    {
        private RoundConfig[] _configsPerRound;

        public int RoundIndex { get; private set; }
        public bool HasVoted { get; set; }

        public Round CurrentRound => CurrentRoundConfig.RoundName;
        public RoundConfig CurrentRoundConfig => RoundIndex == 0 ? null : _configsPerRound[RoundIndex - 1];

        public event Action<RoundConfig> OnNewRoundStarted;

        public bool HasGameStarted => RoundIndex >= 0;

        // [SerializeField] private AudioSource gameMusic;
        // [SerializeField] private Button btnMute;
        public override void Awake()
        {
            base.Awake();
            PunManager.Instance.OnEnterRoom += ResetGameDefaults;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;  // Doesn't allow screen dimming
            Debug.Log("Screen dimming disabled");
            // btnMute.onClick.AddListener(muteUnMute);
        }


        public override void OnDestroy()
        {
            base.OnDestroy();
             PunManager.Instance.OnEnterRoom -= ResetGameDefaults;
        }

        public void StartNewRound()
        {
            Debug.Log(_configsPerRound);
            if (RoundIndex >= _configsPerRound.Length)
            {
                Debug.Log("Round index reached max configs per round. The game is over.");
                return;
            }

            var currentRoundConfig = _configsPerRound[RoundIndex++];
            ChangeCurrentRoundConfig(currentRoundConfig);
        }

        private void ChangeCurrentRoundConfig(RoundConfig currentRoundConfig, bool OnRecconect = true)
        {
            Debug.Log($"New round started: {currentRoundConfig.RoundName}");
            if(OnRecconect) OnNewRoundStarted?.Invoke(currentRoundConfig);
            currentRoundConfig.OnRoundStarted.Invoke();
            
        }

        public void EventNewRoundCall()
        {
            OnNewRoundStarted?.Invoke(CurrentRoundConfig);
        }

        private void ResetGameDefaults()
        {
            _configsPerRound = PunManager.Instance.CurrentRoomScript.ConfigsPerRound;
            
            if(!PunManager.Instance.ReconectRoom)  RoundIndex = 0;
            HasVoted = false;
        }

        public void OnPlayerReconnected(int roundIndex)
        {
            Debug.Log("se entra en on players reconnect");
            _configsPerRound = PunManager.Instance.CurrentRoomScript.ConfigsPerRound;
            
             Debug.Log($"Restoring previous round index: {roundIndex} " + _configsPerRound[0]);

             ChangeCurrentRoundConfig(_configsPerRound[(RoundIndex = roundIndex) - 1],false);
        }
        

        // private void muteUnMute()
        // {
        //     gameMusic.mute = !gameMusic.mute;
        //     var image = btnMute.gameObject.GetComponent<Image>();
        //     if (gameMusic.mute)
        //     {
        //         image.color = new Color(255, 0, 0, 128);
        //     }
        //     else
        //     {
        //         image.color = new Color(255, 255, 255, 255);
        //     }
        // }
    }
}