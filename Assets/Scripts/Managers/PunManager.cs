using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Data;
using Data.Scripts;
using ExitGames.Client.Photon;
using Managers;
using Newtonsoft.Json;
using Photon.Pun;
using Photon.Realtime;
using Screens;
using UnityEngine;
using UnityEngine.Android;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;
using VivoxUnity;

//[RequireComponent(typeof(PhotonView))]
public class PunManager : MonoBehaviourPunCallbacks
{
    [SerializeField, Range(3, 10)] private int roomIdDigits = 7;

    public static PunManager Instance;

    #region Events

    public event Action<List<RoomInfo>> OnRoomCreated;
    public event Action<Player> OnPlayerJoined;
    public event Action<Player> OnPlayerLeft;
    public event Action OnLeaveRoom;
    public event Action OnEnterRoom;
    public event Action<string, bool> OnPlayerReadyOnInterphase;
    public event Action<string, string, string> OnClueFoundByPlayer;
    public event Action<string[]> OnJoinedPrivateRoom;

    #endregion

    public const string CreatorKey = "Creator";
    public const string ScriptKey = "Script";

    private bool fs;
    public RoomInfo[] RoomList => _roomList.ToArray();
    public string CurrentRoomId => PhotonNetwork.CurrentRoom.Name;
    public int PlayersInRoom => PhotonNetwork.CurrentRoom.PlayerCount;
    public int MaxRoomPlayers => PhotonNetwork.CurrentRoom.MaxPlayers;
    public bool IsLocalPlayerReady => bool.Parse(CustomPlayerProp["Ready"].ToString());
    private List<RoomInfo> lisIndex = new List<RoomInfo>();
    public bool Ingame = false;

    private string _currentPrivateRoomId;

    public string[] PlayersInPrivateRoom
    {
        get
        {
            if (!(PhotonNetwork.CurrentRoom.CustomProperties["PrivateRooms"] is List<PrivateRoomInfo> privateRooms))
                throw new NullReferenceException();

            var matchingRoom = privateRooms.FirstOrDefault(info => info.RoomSuffix == _currentPrivateRoomId);
            return matchingRoom?.PlayersInRoom.ToArray();
        }
    }

    public bool ReconectRoom = false;

    public GameScript CurrentRoomScript
    {
        get
        {
            var scriptId = PhotonNetwork.CurrentRoom.CustomProperties[ScriptKey].ToString();
            return DataManager.main.GetScriptById(int.Parse(scriptId));
        }
    }

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        fs = true;

        DataManager.Instancer.OnClueFound += UpdateCluesRoomProperty;
        GameManager.Instancer.OnNewRoundStarted += InitRoomProperty;
    }

    private void OnDestroy()
    {
        DataManager.main.OnClueFound -= UpdateCluesRoomProperty;
        GameManager.main.OnNewRoundStarted -= InitRoomProperty;
    }

    public override void OnDisable()
    {
        PhotonNetwork.Disconnect();
    }

    public void Start()
    {
        //Debug.Log("Connecting to Master");
        DebugMaster.debugInstance.AddMesaggeToConsole("Connecting to Master");
        Connect();
    }

    public void Connect()
    {
        if (PhotonNetwork.IsConnected)
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("is conected");
            Debug.Log("is connected");
            ScreenManager.main.OpenScreen(SerializableScreen.Type.Login, false);
        }

        PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "us";
        var x = PhotonNetwork.ConnectUsingSettings();

    }

    public void ReConnectToPun()
    {
        /*   PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "us";
           var x = PhotonNetwork.ConnectUsingSettings();
          */
        Debug.Log("se ejecuta reconnect "+ PhotonNetwork.IsConnected +" con valor" + PhotonNetwork.IsConnectedAndReady);
        if (PhotonNetwork.IsConnected)
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("is conected");
            ScreenManager.main.OpenScreen(SerializableScreen.Type.Home, false);
            ScreenManager.main.OpenScreen(SerializableScreen.Type.Menu, false);
            VivoxVoiceManager.Instance.Login(PhotonNetwork.LocalPlayer.NickName);
            Debug.Log("is connected to pun and vivox"); 
            
        }

        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "us";
            var x = PhotonNetwork.ConnectUsingSettings();
        }
        
    }

    public override void OnConnectedToMaster()
    {
        // Debug.Log("Connected to Master");
        DebugMaster.debugInstance.AddMesaggeToConsole("Connected to Master" + ReconectRoom);
       Debug.Log("Connected to Master" + ReconectRoom);
        if (ReconectRoom)
        {
            if (PhotonNetwork.IsConnectedAndReady)
            {
              //  ScreenManager.main.OpenScreen(SerializableScreen.Type.Home, false);
            }
            DebugMaster.debugInstance.AddMesaggeToConsole("Se abre open home 1");
        }
        else
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("Se conecta al lobby 2");
            PhotonNetwork.JoinLobby();
        }


        PhotonNetwork.AutomaticallySyncScene = true;
        if (fs && !ReconectRoom)
        {
            ScreenManager.main.OpenScreen(SerializableScreen.Type.Login, false);
            fs = false;
        }

    }

    public override void OnJoinedLobby()
    {
        // Debug.Log("Conected to l
        DebugMaster.debugInstance.AddMesaggeToConsole("Conected to lobby");
        Debug.Log("Conected to lobby");
        if (RealtimeDatabaseManager.rtdbmReference == null)
        {
            PhotonNetwork.NickName = "Player " + Random.Range(0, 1000).ToString("0000");
            CustomPlayerProp["Nick"] = "empty";
            Debug.Log("nullllll");
        }
        else
        {
            Debug.Log("not null 11111111111111111111");
            PhotonNetwork.NickName = RealtimeDatabaseManager.rtdbmReference.UserId;
            CustomPlayerProp["Nick"] = RealtimeDatabaseManager.rtdbmReference.UserName;
        }


        PhotonNetwork.LocalPlayer.CustomProperties = CustomPlayerProp;
    }

    public static bool ConsistsOfWhiteSpace(string s)
    {
        return s.Any(c => c == ' ');
    }

    private Hashtable CustomPlayerProp = new Hashtable();
    private Hashtable CustomRoomProp = new Hashtable();
    private List<RoomInfo> _roomList = new List<RoomInfo>();

    public string LocalPlayerNickname => CustomPlayerProp["Nick"].ToString();

    public void SetPlayerCustomProperties(string nickname1, string ID, string pfpUrl)
    {
        Debug.Log("llamado al set nickname ---------------------------------------------");
        string nickname;
        if (ConsistsOfWhiteSpace(nickname1))
        {
            var x = nickname1.Replace(" ", "-");
            nickname = x;

        }
        else
        {
            nickname = nickname1;
        }

        CustomPlayerProp["Nick"] = nickname;
        CustomPlayerProp["Pfp"] = pfpUrl;
        PhotonNetwork.LocalPlayer.CustomProperties = CustomPlayerProp;
        PhotonNetwork.NickName = ID;
        Debug.Log("nickname: " + nickname + " id: " + ID + "urlpick: " + pfpUrl);
    }

    [ContextMenu("Create testing room")]
    public void CreateTestingRoom()
    {
        CreateRoom("2", 1, 123.ToString());
    }

    public void CreateRoom(string numberPlayers, int gameScript, string pass)
    {
        Debug.Log($"Room created. Num players = {numberPlayers}, selected script id = {gameScript}");
        // if (string.IsNullOrEmpty(roomName1))
        // {
        //  return;
        // }
        //
        // string roomName;
        // if (!ConsistsOfWhiteSpace(roomName1))
        // {
        //  var x = roomName1.Replace(" ", "-");
        //  roomName = x;
        // }
        // else
        // {
        //  roomName = roomName1;
        // }

        var roomId = GetNewRoomId();

        var options = new RoomOptions
        {
            CustomRoomPropertiesForLobby = new[] { CreatorKey, ScriptKey, "Password", "Reconnection", "Vote" },
            MaxPlayers = byte.Parse(numberPlayers)
        };

        var customOpt = new Hashtable
        {
            {CreatorKey, PhotonNetwork.LocalPlayer.CustomProperties["Nick"]},
            {"Password", pass},
            {ScriptKey, gameScript},
            {"Reconnection", null},
            {"Vote", null}
        };
        options.CustomRoomProperties = customOpt;

        Debug.Log("se crea sala custom " + options.CustomRoomProperties["Password"]);
        PhotonNetwork.CreateRoom(roomId, options);
        ScreenManager.main.CloseScreen();

    }

    public void ChangeReadyStatus(bool newState)
    {
        CustomPlayerProp["Ready"] = newState;
        PhotonNetwork.LocalPlayer.SetCustomProperties(CustomPlayerProp);
        NotifyUpdateCharacterAssignations();
    }

    public void JoinRoom(string roomName)
    {
        if (!Application.isEditor)
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
                    {
                        return;
                    }

                    break;
                case RuntimePlatform.IPhonePlayer:
                    if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
                    {
                        return;
                    }

                    break;
            }
        }

        cachedRoomList.Clear();
        PhotonNetwork.JoinRoom(roomName);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("player enter room");
        DebugMaster.debugInstance.AddMesaggeToConsole("pun:player enter room");
        DataManager.main.RegisterPlayer(newPlayer);
        OnPlayerJoined?.Invoke(newPlayer);
    }

    public override void OnJoinedRoom()
    {
        if (ReconectRoom)
        {
            Debug.Log("se inicia la reconeccion ");
            CustomPlayerProp["Ready"] = false;
            PhotonNetwork.LocalPlayer.SetCustomProperties(CustomPlayerProp);
            DataManager.main.OnEnterRoom();
            VivoxVoiceManager.Instance.JoinChannel(CurrentRoomId, ChannelType.NonPositional,
                VivoxVoiceManager.ChatCapability.AudioOnly);
            VivoxVoiceManager.Instance.OnUserLoggedInEvent += HandleUserLoggedIn; 
            Ingame = true;
            
          // DataManager.main.CurrentRoomScript = 
            CameraManager.main.MapRenderer = DataManager.main.CurrentRoomScript.SearchMap;
            Debug.Log("se llega al paso de data manager");
            SetRoomProperty();
            Debug.Log("Entramos en una sala correctamente " + PhotonNetwork.CurrentRoom.CustomProperties[CreatorKey] + " y jsonvotos" +
                      PhotonNetwork.CurrentRoom.CustomProperties["Vote"]);
            ScreenManager.main.CloseScreen(SerializableScreen.Type.Menu);
            ScreenManager.main.CloseScreen(SerializableScreen.Type.Home);
             var ScreenGame=  ScreenManager.main.OpenScreen(SerializableScreen.Type.GameScreen);
            OnEnterRoom?.Invoke();
            Debug.Log("Se llega al paso final votacion");
            LoadDataFromjson();
           // ScreenGame.GetComponent<GameScreen>().SetupPlayersHandler();
            GameManager.main.EventNewRoundCall();
            Debug.Log("se finaliza la reconnect");
           
            ReconectRoom = false;
            ScreenManager.main.CloseScreen(SerializableScreen.Type.ResetScreen);
          //  ScreenManager.main.CloseScreen(SerializableScreen.Type.ResetScreen);
           
        }
        else
        {
            if (PhotonNetwork.IsMasterClient &&
                PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                PhotonNetwork.CurrentRoom.IsVisible = false;
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }

            Debug.Log("Entramos en una sala correctamente " + PhotonNetwork.CurrentRoom.CustomProperties[CreatorKey] +
                      PhotonNetwork.CurrentRoom.CustomProperties[ScriptKey]);
            ScreenManager.main.OpenScreen(SerializableScreen.Type.GameRoom);
            CustomPlayerProp["Ready"] = false;
            PhotonNetwork.LocalPlayer.SetCustomProperties(CustomPlayerProp);
            OnEnterRoom?.Invoke();
            DebugMaster.debugInstance.AddMesaggeToConsole("on joined room good");
            VivoxVoiceManager.Instance.JoinChannel(CurrentRoomId, ChannelType.NonPositional,
                VivoxVoiceManager.ChatCapability.AudioOnly);
            VivoxVoiceManager.Instance.OnUserLoggedInEvent += HandleUserLoggedIn;

        }
            MusicManager.main.PlayMusic(false);

        ReconectRoom = false;
    }

    public void LoadDataFromjson()
    {
        var dataManagerInfo = PhotonNetwork.CurrentRoom.CustomProperties["Vote"];

        if (dataManagerInfo != null)
        {
            Debug.Log("paso 3 se inicia load data from json "+dataManagerInfo.ToString());
            DataManager.main.LoadData(dataManagerInfo.ToString());
        }
        else 
        {
            //Set default info when data comes null
            DataManager.main.LoadData(string.Empty);
            Debug.Log("<color=red>dataManagerInfo </color>: null data");
        }


    }



    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (PhotonNetwork.IsMasterClient && !Ingame)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount != PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                PhotonNetwork.CurrentRoom.IsVisible = true;
                PhotonNetwork.CurrentRoom.IsOpen = true;
            }
        }

        if (PhotonNetwork.IsMasterClient && Ingame)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount != PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                PhotonNetwork.CurrentRoom.IsVisible = false;
                PhotonNetwork.CurrentRoom.IsOpen = true;
                Debug.Log("se abre la sala para que entren los jugadores");
            }
        }

        OnPlayerLeft?.Invoke(otherPlayer);
        Debug.Log(
            $"A player left the game. Reconnection info: {PhotonNetwork.CurrentRoom.CustomProperties["Reconnect"]}");
    }

    public void LeaveRoom()
    {
        Debug.Log("se abre leave room");
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.JoinLobby();
        Ingame = false;
        VivoxVoiceManager.Instance.DisconnectAllChannels();
        ScreenManager.main.OpenScreen(SerializableScreen.Type.GameLobby);
        Debug.Log("USER LEAVER ROOM");
        DebugMaster.debugInstance.AddMesaggeToConsole("user leaver room");
        // se reinician las pistas
        DataManager.main.ResetClues();
    }

    public Dictionary<string, RoomInfo> cachedRoomList = new Dictionary<string, RoomInfo>();

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        if (roomList != null)
        {
            for (int i = 0; i < roomList.Count; i++)
            {
                RoomInfo info = roomList[i];
                if (info.RemovedFromList)
                {
                    cachedRoomList.Remove(info.Name);
                    lisIndex.Remove(info);
                }
                else
                {
                    lisIndex.Add(info);
                    cachedRoomList[info.Name] = info;
                } // Debug.Log("orden0 i " + i + info.CustomProperties[CreatorKey].ToString());
            }
        }

        OnRoomCreated?.Invoke(cachedRoomList.Values.ToList());
        Debug.Log("lista de salas =>" + roomList.Count);
    }

    public int Indice(RoomInfo room)
    {
        if (room == null) return 0;
        int i = 0;
        foreach (var RoomList in lisIndex)
        {
            if (RoomList.Name == room.Name)
            {
                Debug.Log("se devuelve valor indice " + i + " " + room.Name);
                return i;
            }

            i++;
        }

        return 0;

    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Dc error :=> " + cause);
        PhotonNetwork.Disconnect();
        if (cause == DisconnectCause.DisconnectByClientLogic) return;
        
        ScreenManager.main.CloseScreen(SerializableScreen.Type.Menu);
        ScreenManager.main.OpenScreen(SerializableScreen.Type.ResetScreen);
        // if (cause == DisconnectCause.DisconnectByClientLogic) return;
        /*  RestManager.main.ForceOnSignOut();
          DebugMaster.debugInstance.AddMesaggeToConsole("Dc error :=> " + cause);
          RealtimeDatabaseManager.rtdbmReference.UpdateOnlineStatus(false); */

    }



    public override void OnLeftRoom()
    {
        CustomPlayerProp.Remove("Character");
        PhotonNetwork.SetPlayerCustomProperties(CustomPlayerProp);
        OnLeaveRoom?.Invoke();
        // DebugMaster.debugInstance.AddMesaggeToConsole("on left room");
        VivoxVoiceManager.Instance.OnUserLoggedInEvent -= HandleUserLoggedIn;
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Room Creation Failed: " + message);
        DebugMaster.debugInstance.AddMesaggeToConsole("room creation failed" + message);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        if (returnCode.Equals(ErrorCode.GameDoesNotExist))
        {
            RealtimeDatabaseManager.rtdbmReference.UpdateRoomId("");
            ReconectRoom = false;
            var dialoguePopup = ScreenManager.main.OpenScreen<DialoguePopup>(false);
            dialoguePopup.Setup("The room you attempted to reconnect is either full or closed.");

        }
        else
        {
            Debug.Log("Join RoomFailed: " + message);
            DebugMaster.debugInstance.AddMesaggeToConsole("join room failed" + message);
            ScreenManager.main.OpenScreen(SerializableScreen.Type.GameLobby);
            var dialoguePopup = ScreenManager.main.OpenScreen<DialoguePopup>(false);
            dialoguePopup.Setup("The room you attempted to join is either full or closed.");
        }

    }

    private void HandleUserLoggedIn()
    {
        var voiceManagerInstance = VivoxVoiceManager.Instance;
        voiceManagerInstance.JoinChannel(voiceManagerInstance.CurrentChannelId, ChannelType.NonPositional,
            VivoxVoiceManager.ChatCapability.AudioOnly);
    }

    [ContextMenu("ValidatePlayerList")]
    void ValidatePlayerList()
    {
        Debug.Log("lista de jugadores " + PhotonNetwork.PlayerList.Length);
        // foreach (var VARIABLE in PhotonNetwork.PlayerList)
        // {
        //     Debug.Log("id del jugador: " + VARIABLE.CustomProperties["Nick"] + "character: " + VARIABLE.CustomProperties["Character"] + "picture: " + VARIABLE.CustomProperties["Pfp"]
        //
        //     + " custom ready status: " + VARIABLE.CustomProperties["Ready"]
        //     );
        // }
        Debug.Log($"Player pfp: {PhotonNetwork.LocalPlayer.CustomProperties["Pfp"]}");
        Debug.Log(" custom property de contraseña =>" + PhotonNetwork.CurrentRoom.CustomProperties["Password"]);

    }

    private void UpdateCluesRoomProperty(Clue[] cluesFound)
    {
        if (!PhotonNetwork.IsMasterClient) return;
      
        var serializedReconnection = PhotonNetwork.CurrentRoom.CustomProperties["Reconnect"].ToString();
        var reconnectionInfo = JsonConvert.DeserializeObject<ReconnectionInfo>(serializedReconnection);

        reconnectionInfo.FoundClues = cluesFound.Select(clue => new FoundClue
        {
            ClueName = clue.Title,
            FoundByPlayer = clue.FoundBy
        }).ToArray();
        CustomRoomProp["Reconnect"] = JsonConvert.SerializeObject(reconnectionInfo);
        PhotonNetwork.CurrentRoom.SetCustomProperties(CustomRoomProp);
    }

    public void UpdateResultVotes()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        CustomRoomProp["Vote"] = DataManager.main.GetSavedData();
        PhotonNetwork.CurrentRoom.SetCustomProperties(CustomRoomProp);
        Debug.Log("se actuliza el json " + CustomRoomProp.ToString());
    }

    // public void AddPrivateRoom(string roomSuffix)
    // {
    //     if (!PhotonNetwork.InRoom) return;
    //     if (!PhotonNetwork.IsMasterClient) return;
    //     if (!(PhotonNetwork.CurrentRoom.CustomProperties["PrivateRooms"] is List<PrivateRoomInfo> roomsInfo)) return;
    //     if (roomsInfo.Any(info => info.RoomSuffix == roomSuffix)) return;
    //     
    //     roomsInfo.Add(new PrivateRoomInfo
    //     {
    //         RoomSuffix = roomSuffix
    //     });
    //
    //     CustomRoomProp["PrivateRooms"] = roomsInfo.ToArray();
    //     PhotonNetwork.CurrentRoom.SetCustomProperties(CustomRoomProp);
    // }

    // public void JoinPrivateRoom(string roomSuffix, SpriteRenderer bgRoom)
    // {
    //     if (!(PhotonNetwork.CurrentRoom.CustomProperties["PrivateRooms"] is List<PrivateRoomInfo> roomsInfo)) return;
    //
    //     var matchingRoom = roomsInfo.FirstOrDefault(info => info.RoomSuffix == roomSuffix);
    //     if (matchingRoom == null) return;
    //
    //     var localNick = PhotonNetwork.LocalPlayer.NickName;
    //     if (matchingRoom.PlayersInRoom.Contains(localNick)) return;
    //
    //     matchingRoom.PlayersInRoom.Add(localNick);
    //     VivoxVoiceManager.Instance.JoinChannel(roomSuffix, ChannelType.NonPositional, VivoxVoiceManager.ChatCapability.AudioOnly);
    //     CameraManager.main.MapRenderer = bgRoom;
    //     CustomRoomProp["PrivateRooms"] = roomsInfo;
    //     _currentPrivateRoomId = roomSuffix;
    //     PhotonNetwork.CurrentRoom.SetCustomProperties(CustomRoomProp);
    //     OnJoinedPrivateRoom?.Invoke(PlayersInPrivateRoom);
    // }

    private void InitRoomProperty(RoundConfig currentRound)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        ReconnectionInfo roomReconnection;
        if (currentRound.RoundName == Round.ScriptReading)
        {
            roomReconnection = new ReconnectionInfo
            {
                SessionAssignations = Assignations
            };
            roomReconnection.CurrentRoundIndex = 1;
            Debug.Log("se pone current rount index 1");

        }
        else
        {
            roomReconnection =
                JsonConvert.DeserializeObject<ReconnectionInfo>(PhotonNetwork.CurrentRoom.CustomProperties["Reconnect"]
                    .ToString());
            roomReconnection.CurrentRoundIndex = GameManager.main.RoundIndex;
        }
        Debug.Log("se serializa " + roomReconnection.CurrentRoundIndex);
        CustomRoomProp["Reconnect"] = JsonConvert.SerializeObject(roomReconnection);

        PhotonNetwork.CurrentRoom.SetCustomProperties(CustomRoomProp);
    }

    private void SetRoomProperty()
    {
        var reconnectionInfo = PhotonNetwork.CurrentRoom.CustomProperties["Reconnect"].ToString();
        var deserializedInfo = JsonConvert.DeserializeObject<ReconnectionInfo>(reconnectionInfo);
        var VotesInfo = PhotonNetwork.CurrentRoom.CustomProperties["Vote"];
        // no funciona aun
        // DataManager.main.LoadData(VotesInfo.ToString());
        Debug.Log("paso 1 de set room " + deserializedInfo.CurrentRoundIndex);
        GameManager.main.OnPlayerReconnected(deserializedInfo.CurrentRoundIndex);
        Debug.Log("paso 2 de set room " + deserializedInfo.SessionAssignations);
        SetRoomProperty2();
        
    }

    public void SetRoomProperty2()
    {
        var reconnectionInfo = PhotonNetwork.CurrentRoom.CustomProperties["Reconnect"].ToString();
        var deserializedInfo = JsonConvert.DeserializeObject<ReconnectionInfo>(reconnectionInfo);
        DataManager.main.OnPlayerReconnected(deserializedInfo.SessionAssignations);
    }
    

    private static CharacterAssignation[] Assignations => DataManager.main.GameAssignations.Select(gameAssignation => new CharacterAssignation { Character = gameAssignation.AssignedCharacter.name, PlayerId = gameAssignation.PlayerId }).ToArray();

    public ReconnectionInfo ReconnectionInfo
    {
        get
        {
            if (!PhotonNetwork.InRoom) return null;
            var reconnectionRaw = PhotonNetwork.CurrentRoom
                .CustomProperties["Reconnect"];

            if (reconnectionRaw == null) return null;

            return JsonConvert.DeserializeObject<ReconnectionInfo>(reconnectionRaw
                .ToString());
        }
    }

    [ContextMenu("Validatecustom properties")]
    void CheckSaveState()
    {

        if (PhotonNetwork.CurrentRoom != null)
        {
            Debug.Log("custom de savestate del current room" + PhotonNetwork.CurrentRoom.CustomProperties["Reconnect"].ToString());

        }
        foreach (var VARIABLE in cachedRoomList)
        {
            Debug.Log("Sala " + VARIABLE.Value.Name + " con script " + VARIABLE.Value.CustomProperties[ScriptKey] + " mas valor de reconexion " + VARIABLE.Value.CustomProperties["Reconnect"].ToString());

        }

        Debug.Log(" y jsonvotos" + PhotonNetwork.CurrentRoom.CustomProperties["Vote"]);
    }


    public void CheckInternet()
    {
        //Connect();
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log("Error. Check internet connection!");
            return;
        }
        DebugMaster.debugInstance.AddMesaggeToConsole("Error. Check internet connection!");
        SceneManager.LoadScene("ResetScene");
    }

    [Pure]
    private string GetNewRoomId()
    {
        var roomId = string.Empty;

        bool isIdDuplicated;
        do
        {
            for (var i = 0; i < roomIdDigits; i++)
            {
                roomId += Random.Range(0, 10).ToString();
            }

            isIdDuplicated = RoomList.Any(room => room.Name == roomId);

            if (isIdDuplicated) roomId = string.Empty;

        } while (isIdDuplicated);

        return roomId;
    }

    #region Messages

    public void NotifyAssignPlayerToCharacter(string characterName)
    {
        foreach (var player in PhotonNetwork.PlayerList)
        {
            if (player.CustomProperties.TryGetValue("Character", out var value) && value.ToString() == characterName)
            {
                Debug.LogError($"Character {characterName} is already assigned to player {player.NickName}");
                return;
            }
        }

        Debug.Log("llenado " + characterName);
        CustomPlayerProp["Character"] = characterName;
        PhotonNetwork.LocalPlayer.SetCustomProperties(CustomPlayerProp);
        NotifyUpdateCharacterAssignations();
    }

    public void NotifyUpdateCharacterAssignations()
    {
        photonView.RPC(nameof(RpcUpdateCharacterAssignations), RpcTarget.All);
    }

    public void NotifyPlayerReadyOnInterphase(bool isReady)
    {
        photonView.RPC(nameof(RpcPlayerReadyOnInterphase), RpcTarget.All, DataManager.main.CharacterAssigned.name, isReady);
    }

    public void NotifyClueFoundByPlayer(string foundClue, string clueId)
    {
        photonView.RPC(nameof(RpcClueFound), RpcTarget.All, foundClue, PhotonNetwork.LocalPlayer.NickName,clueId);
    }

    public void AddAcusedByIndex(string accusedData)
    {
        photonView.RPC(nameof(SendAccusedByIndex), RpcTarget.All, accusedData);
    }

    public void AddVote(string voteData)
    {
        photonView.RPC(nameof(SendVote), RpcTarget.All, voteData);
    }
    public void QuestionsClearConirmation()
    {
        photonView.RPC(nameof(SendQuestionsClearConirmation), RpcTarget.All);
    }


    public void AddMVPVote(string mvpVote)
    {
        photonView.RPC(nameof(SendMVPVote), RpcTarget.All, mvpVote);
    }

    public void NotifyNewRoundStarted()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        photonView.RPC(nameof(RpcStartNewRound), RpcTarget.All);
    }
    #endregion

    #region RPCs

    [PunRPC]
    public void SendMVPVote(string mvpVote)
    {
        DataManager.main.AddMVPVote(mvpVote);
    }

    [PunRPC]
    public void SendAccusedByIndex(string accusedData)
    {
        DataManager.main.AddAccusedByResultIndex(accusedData);
    }
    [PunRPC]
    public void SendVote(string voteData)
    {
        DataManager.main.AddVote(voteData);
    }

    [PunRPC]
    public void SendQuestionsClearConirmation()
    {
        DataManager.main.QuestionsClearConfirmations();
    }

    [PunRPC]
    private void RpcUpdateCharacterAssignations()
    {
        DataManager.main.UpdateCharacterAssignations();
    }

    [PunRPC]
    private void RpcPlayerReadyOnInterphase(string playerId, bool isReady)
    {
        OnPlayerReadyOnInterphase?.Invoke(playerId, isReady);
    }

    [PunRPC]
    private void RpcClueFound(string clueName, string foundById, string clueId)
    {
        OnClueFoundByPlayer?.Invoke(clueName, foundById, clueId);
    }

    [PunRPC]
    private void RpcStartNewRound()
    {
        GameManager.main.StartNewRound();
    }

    #endregion
}
