using UnityEngine;
using Managers;
public class AddAcusedData 
{
    public int index;
    public string accusedName;
}
[System.Serializable]
public class 
    AddVoteData
{
    public int index;
    public string accusedName;
    public string voter;
}

public class ResultsManager : Singleton<ResultsManager>
{
    public static int accusedIDCount = 0;

    public void AddAcused(string accusedName)
    {
        AddAcusedData addAcusedData = new AddAcusedData
        {
            index = QuestionManager.main.questionIndex,
            accusedName = accusedName
        };

        PunManager.Instance.AddAcusedByIndex(JsonUtility.ToJson(addAcusedData));
    }

    public void AddVoteToAccused(string accusedName)
    {
        AddVoteData addvoteData = new AddVoteData
        {
            index = QuestionManager.main.questionIndex,
            accusedName = accusedName,
            voter = DataManager.main.CharacterAssigned.name
        };
        PunManager.Instance.AddVote(JsonUtility.ToJson(addvoteData));
    }
}
