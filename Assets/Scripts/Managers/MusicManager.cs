using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : Singleton<MusicManager>
{
    private AudioSource audioSource;
    public bool currState = false;
    private Coroutine fadein;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlayMusic(bool _state)
    {
        if (currState != _state)
        {
            if (_state)
                fadein = StartCoroutine(FadeIn(1.5f));
            else
                StartCoroutine(FadeOut(1.5f));
        }
        currState = _state;
    }
    public IEnumerator FadeOut(float FadeTime)
    {
        StopCoroutine(fadein);
       // Debug.LogError("fade out");
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0 )
        {
         //   Debug.LogError("on fading out");
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }
    public IEnumerator FadeIn(float FadeTime)
    {
       // Debug.LogError("fade in");
        float startVolume = audioSource.volume;
        audioSource.volume = 0;
        audioSource.Play();

        while (audioSource.volume < 1)
        {
           // Debug.LogError("on fading in");
            audioSource.volume += startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.volume = startVolume;
    }
}
