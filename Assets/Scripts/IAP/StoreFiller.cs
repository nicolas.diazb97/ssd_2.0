using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class StoreFiller : MonoBehaviour
{
    public static StoreFiller storeFillerInstance;

    [SerializeField] List<TextMeshProUGUI> sName = new List<TextMeshProUGUI>();
    [SerializeField] List<TextMeshProUGUI> sPrice = new List<TextMeshProUGUI>();

    [SerializeField] List<Sprite> sSprite = new List<Sprite>();
    [SerializeField] List<Image> sImage = new List<Image>();

    List<string> sNameText = new List<string>();
    List<string> sPriceText = new List<string>();

    private void Awake()
    {
        if (storeFillerInstance == null)
        {
            storeFillerInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void FillPairScript(string name, string price)
    {
#if UNITY_ANDROID
        name = RemoveCom(name);
#endif

        Debug.Log($"Fill {name} {price}");
        sNameText.Add(name);
        sPriceText.Add("$ " + price);

    }

    public void SetPairScript()
    {
        for (int i = 0; i < sName.Count; i++)
        {
            Debug.Log($"Fill {sNameText[i]} {sPriceText[i]}");
            sName[i].text = sNameText[i];
            sPrice[i].text = sPriceText[i];
            sImage[i].sprite = SetImg(sNameText[i]);
        }
    }

    private Sprite SetImg(string name)
    {
        Debug.Log($"valued {name}");

        switch (name)
        {
            case string a when a.Contains("Murder"): return sSprite[0];
            case string a when a.Contains("Match"): return sSprite[1];
            case string a when a.Contains("Demon"): return sSprite[2];
            default: return sSprite[0];
        }
    }

    private string RemoveCom(string lName)
    {
        char[] b = lName.ToCharArray();

        string shortUserName = "";

        foreach (char item in b)
        {
            if (item != '(')
            {
                shortUserName += item;
            }
            else
            {
                return shortUserName;
            }
        }
        return shortUserName;
    }
}