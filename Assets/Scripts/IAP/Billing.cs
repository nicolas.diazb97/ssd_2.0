﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.EssentialKit;
using VoxelBusters.CoreLibrary;
using System.Linq;
using Managers;

public class Billing : MonoBehaviour
{
    public static Billing sharedInstence;
    [SerializeField] Button[] purchaseButton;

    private Dictionary<string, string> scriptPrices = new Dictionary<string, string>();

    private void Awake()
    {
        if (sharedInstence == null)
        {
            sharedInstence = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        if (BillingServices.IsAvailable())
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("Store is available");
            BillingServices.InitializeStore();
        }
#if UNITY_EDITOR
        SortButtons();
#endif
    }

    void SortButtons()
    {
        Button[] sortButtons = new Button[3];
        sortButtons[0] = purchaseButton[1];
        sortButtons[1] = purchaseButton[2];
        sortButtons[2] = purchaseButton[0];

        purchaseButton = sortButtons;

    }
    private void OnEnable()
    {
        // register for events
        BillingServices.OnInitializeStoreComplete += OnInitializeStoreComplete;
        BillingServices.OnTransactionStateChange += OnTransactionStateChange;
        BillingServices.OnRestorePurchasesComplete += OnRestorePurchasesComplete;
    }
    private void OnDisable()
    {
        // unregister from events
        BillingServices.OnInitializeStoreComplete -= OnInitializeStoreComplete;
        BillingServices.OnTransactionStateChange -= OnTransactionStateChange;
        BillingServices.OnRestorePurchasesComplete -= OnRestorePurchasesComplete;
    }
    private void OnInitializeStoreComplete(BillingServicesInitializeStoreResult result, Error error)
    {
        if (error == null)
        {
            // update UI
            // show console messages
            var products = result.Products;
            DebugMaster.debugInstance.AddMesaggeToConsole("Store initialized successfully.");
            //  DebugMaster.debugInstance.AddMesaggeToConsole("Total products fetched: " + products.Length);
            DebugMaster.debugInstance.AddMesaggeToConsole("Below are the available products:");
            for (int iter = 0; iter < products.Length; iter++)
            {
                //product => "Product you got from OnInitializeStoreComplete event (result.Products)" 
                purchaseButton[iter].interactable = !BillingServices.IsProductPurchased(products[iter]);
                var product = products[iter];
                DebugMaster.debugInstance.AddMesaggeToConsole(string.Format("[{0}]: {1}", iter, product));
                Debug.Log(string.Format("[{0}]: {1}", iter, product));
                scriptPrices.Add(product.Id, product.LocalizedPrice);
            }
        }
        else
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("Store initialization failed with error. Error: " + error);
        }

        var invalidIds = result.InvalidProductIds;
        DebugMaster.debugInstance.AddMesaggeToConsole("Total invalid products: " + invalidIds.Length);
        if (invalidIds.Length > 0)
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("Here are the invalid product ids:");
            for (int iter = 0; iter < invalidIds.Length; iter++)
            {
                DebugMaster.debugInstance.AddMesaggeToConsole(string.Format("[{0}]: {1}", iter, invalidIds[iter]));
            }
        }

        if (Debug.isDebugBuild) return;

        foreach (var script in DataManager.main.AvailableScripts)
        {
            DebugMaster.debugInstance.AddMesaggeToConsole($"{script.Title} price: <color=yellow>{script.PurchasePrice}</color>");
        }
    }

    [ContextMenu("DebugGetScriptPrice")]
    public string GetScriptPrice(string scriptId)
    {
        string price = "";
        Debug.Log("id de script " +scriptId);
        scriptPrices.TryGetValue(scriptId, out string value);
        Debug.Log("id de script " + value);
        if (value == null)
        {
            Debug.Log("id de precio free");
            price = "Free";
        }
        else
        {
            Debug.Log("id de precio paid");
            price = value;
        }
        DebugMaster.debugInstance.AddMesaggeToConsole($"GetScriptPrice {price}");
        return price;
    }
    public int GetScriptPriceInt(string scriptId)
    {
        int price;
        scriptPrices.TryGetValue(scriptId, out string value);
        var notPrice = new string(value.Where(c => char.IsDigit(c)).ToArray());
        notPrice = notPrice.Remove(notPrice.Length - 2);
        price = int.Parse(notPrice);
        DebugMaster.debugInstance.AddMesaggeToConsole($"GetScriptPriceInt {price}");
        return price;
    }


    public static string FormatSName(string sName)
    {
        sName = sName.ToLower();
        char[] b = sName.ToCharArray();
        string scriptItem = "";

        foreach (char item in b)
        {

            if (item == ' ')
            {
                scriptItem += '_';
            }
            else
            {
                scriptItem += item;
            }
        }

        return scriptItem;
    }
    private void OnTransactionStateChange(BillingServicesTransactionStateChangeResult result)
    {
        var transactions = result.Transactions;
        for (int iter = 0; iter < transactions.Length; iter++)
        {
            var transaction = transactions[iter];
            switch (transaction.TransactionState)
            {
                case BillingTransactionState.Purchased:
                    if (transaction.ReceiptVerificationState == BillingReceiptVerificationState.Success)
                    {
                        RewardPurchase(transaction.Payment.ProductId);
                    }
                    DebugMaster.debugInstance.AddMesaggeToConsole(string.Format("Buy product with id:{0} finished successfully.", transaction.Payment.ProductId));
                    break;

                case BillingTransactionState.Failed:
                    DebugMaster.debugInstance.AddMesaggeToConsole(string.Format("Buy product with id:{0} failed with error. Error: {1}", transaction.Payment.ProductId, transaction.Error));
                    break;
            }
        }
    }
    public void Purchase(int productIndex)
    {
#if UNITY_EDITOR
        UpdateBuyOnFirebase(BillingServices.Products[productIndex].Id);
#else
        Debug.Log("Start Purchase");
        if (BillingServices.CanMakePayments())
        {
            BillingServices.BuyProduct(BillingServices.Products[productIndex]);
        }
#endif
    }
    void RewardPurchase(string productID)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole($"Trying to buy {productID}");
        Debug.Log($"Trying to buy {productID}");
        switch (productID)
        {
            case "murder_at_the_amusement_park":
                UpdatePurchaseState(productID, 0);
                break;
            case "millionaire_match":
                UpdatePurchaseState(productID, 1);
                break;
            case "demon_realm":
                UpdatePurchaseState(productID, 2);
                break;
        }
    }

    public void UpdatePurchaseState(string productID, int itemID)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("Reward product and save to cloud");

        purchaseButton[itemID].interactable = false;
        UpdateBuyOnFirebase(productID);
    }
    public void UpdateBuyOnFirebase(string sName)
    {
        RealtimeDatabaseManager.rtdbmReference.UpdateScripts(sName);
    }

    private void OnRestorePurchasesComplete(BillingServicesRestorePurchasesResult result, Error error)
    {
        if (error == null)
        {
            var transactions = result.Transactions;
            DebugMaster.debugInstance.AddMesaggeToConsole("Request to restore purchases finished successfully.");
            DebugMaster.debugInstance.AddMesaggeToConsole("Total restored products: " + transactions.Length);
            for (int iter = 0; iter < transactions.Length; iter++)
            {
                var transaction = transactions[iter];
                if (transaction.ReceiptVerificationState == BillingReceiptVerificationState.Success)
                {
                    RewardPurchase(transaction.Payment.ProductId);
                }
                DebugMaster.debugInstance.AddMesaggeToConsole(string.Format("[{0}]: {1}", iter, transaction.Payment.ProductId));
            }
        }
        else
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("Request to restore purchases failed with error. Error: " + error);
        }
    }
    public void Restore()
    {
        DebugMaster.debugInstance.AddMesaggeToConsole($"Restore Purchases");
        Debug.Log($"Restore Purchases");
        BillingServices.RestorePurchases();
    }


}