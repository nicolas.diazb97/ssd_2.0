using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class SearchTag
{
    private string _tagValue;
    private float _floatValue;
    private Type _tagType;

    public SearchTag(string tagValue)
    {
        _tagType = Type.Alphabetic;
        _tagValue = tagValue;
    }
    
    public SearchTag(float tagValue)
    {
        _tagType = Type.Ranged;
        _floatValue = tagValue;
    }

    public bool TagMatchesFilter(SearchFilter filter)
    {
        if (filter.FilterType != _tagType) return false;

        return filter.FilterType switch
        {
            Type.Ranged => _floatValue >= filter.MinFloatValue && _floatValue <= filter.MaxFloatValue,
            Type.Alphabetic => _tagValue == filter.FilterValue,
            _ => throw new ArgumentOutOfRangeException()
        };
    }

    public enum Type
    {
        Ranged, Alphabetic
    }
}

public class FilterItem : MonoBehaviour
{
    [SerializeField] private UnityEvent<bool> onFiltered;
    private SearchTag[] _tags;

    private FilterManager _filterManagerRef;

    private void Awake()
    {
        _filterManagerRef = GetComponentInParent<FilterManager>();
        if (!_filterManagerRef)
        {
            Debug.LogError($"There must exist a parent filter manager for {name}.");
            return;
        }

        _filterManagerRef.OnActiveFiltersUpdated += HandleActiveFiltersUpdated;
    }

    private void OnDestroy()
    {
        _filterManagerRef.OnActiveFiltersUpdated -= HandleActiveFiltersUpdated;
    }

    public void Init(params SearchTag[] tags)
    {
        _tags = tags;
    }

    private void HandleActiveFiltersUpdated(SearchFilter[] filters)
    {
        if (filters.Length < 1)
        {
            onFiltered?.Invoke(true);
            return;
        }

        var isFilterActive = (from filter in filters from searchTag in _tags where searchTag.TagMatchesFilter(filter) select filter).Any();
        onFiltered?.Invoke(isFilterActive);
    }
}
