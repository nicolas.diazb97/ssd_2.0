﻿using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "Clue", menuName = "Data/Clue", order = 0)]
    public class Clue : ScriptableObject
    {
        [SerializeField] private Sprite thumbnail;
        [SerializeField] private string title;
        [SerializeField] private string description;
        [SerializeField] private string NestedID;
        [SerializeField] private List<Clue> nestedClue;
        [SerializeField] private string tagFound;


        public string TagFound => tagFound;
        public SearchPlace FoundIn { get; set; }
        public string FoundBy { get; set; }

        public Sprite Thumbnail => thumbnail;

        public List<Clue> NestedClue => nestedClue;

        public string Title => title;

        public string Description => description;
        public string ClueID => NestedID;
        public bool Found => FoundIn;
    }
}