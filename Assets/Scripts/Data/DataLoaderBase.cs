using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Data
{
    public class DataLoaderBase : MonoBehaviour
    {
        [SerializeField] private TextAsset dataAsset;
        private JArray _loadedData;

        public event Action<Func<int, string, string>> OnDataLoaded;

        public int DataCount => _loadedData.Count;

        private void Awake()
        {
            _loadedData = JArray.Parse(dataAsset.text);

            if (_loadedData == null)
            {
                Debug.LogError($"Unable to load data from {dataAsset.name}");
                return;
            }
            
            OnDataLoaded?.Invoke(GetValue);
        }

        private string GetValue(int index, string key)
        {
            return _loadedData[index][key]?.Value<string>();
        }
    }
}