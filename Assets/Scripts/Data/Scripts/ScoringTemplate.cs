﻿using Managers;
using UnityEngine;

namespace Data.Scripts
{
    public abstract class ScoringTemplate : ScriptableObject, IScoringStrategy
    {
        protected const byte WIN_POINTS = 6;
        protected const byte DRAW_POINTS = 3;
        protected const byte LOOSE_POINTS = 0;

        protected GameScript gameScript;
        public AnswerResutl GetMostVotedCharacter(int result)
        {
            AnswerResutl mostVoted = DataManager.main.Results[result].Answers[byte.MinValue];
            for (int i = 1; i < DataManager.main.Results[result].Answers.Length; i++)
            {
                if (DataManager.main.Results[result].Answers[i].Votes.Count > mostVoted.Votes.Count)
                {
                    mostVoted = DataManager.main.Results[result].Answers[i];
                }
            }
            return mostVoted;
        }
        public void SetUp(GameScript gameScript) => this.gameScript = gameScript;
        public abstract float CalculateScore(string currentCharacter);
    }
}