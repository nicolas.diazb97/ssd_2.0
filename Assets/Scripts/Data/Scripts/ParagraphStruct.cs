﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Data.Scripts
{
    public partial class GameScript
    {
        [Serializable]
        public class ParagraphStruct
        {

            [FormerlySerializedAs("personalstory")] [SerializeField, TextArea(6, 20)] public string Text;
            [SerializeField]
            public float ScrollRectPosition;
        }
       
    }
}