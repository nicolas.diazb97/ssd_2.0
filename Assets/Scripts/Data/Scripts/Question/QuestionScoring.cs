using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestionScoring
{
    [SerializeField]
    public int correctScore;
    [SerializeField]
    public int incorrectScore;
    [SerializeField]
    public ScoringResult[] scoringResults;
}
