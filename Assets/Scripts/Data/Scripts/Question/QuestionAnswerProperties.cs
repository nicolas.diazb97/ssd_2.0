using System;
using UnityEngine;

[Serializable]
public class QuestionAnswerProperties: MonoBehaviour
{
    [SerializeField] private int index;
    private string accusedName;
    private bool isSelected;

    public int Index { get { return index; } set { index = value; } }
    public bool IsSelected { get { return isSelected; } set { isSelected = value; } }
    public string AccusedName { get { return accusedName; } set { accusedName = value; } }
}
