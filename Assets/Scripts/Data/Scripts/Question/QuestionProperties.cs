using UnityEngine;
using System;

[Serializable]
public struct QuestionProperties
{
    [SerializeField] private string questionText;
    [SerializeField] private string[] questionAnswers;
    [SerializeField] private int correctAnswerIndex;
    [SerializeField] private bool wasCorrect;
    public string QuestionText { get=> questionText; set { questionText = value; } }
    public string[] QuestionAnswers { get => questionAnswers; set { questionAnswers = value; } }
    public int CorretAnswerIndex { get => correctAnswerIndex; set { correctAnswerIndex = value; } }
    public bool WasCorrect { get => wasCorrect; set { wasCorrect = value; } }
}
