using Managers;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Answer
{
    public string text = string.Empty;
    public Sprite correctSprite = null;
    public Sprite incorrectSprite = null;

    public bool HasTags => correctSprite && incorrectSprite;
    public Sprite CorrectSprite => correctSprite;
    public Sprite IncorrectSprite => incorrectSprite;
}
[CreateAssetMenu(fileName = "New Game Question", menuName = "Data/Game Question", order = 0)]
public class Question : ScriptableObject
{
    [TextArea(3, 10)]
    [SerializeField] private string questionName;
    [SerializeField] private string questionText;
    [SerializeField] private Answer[] answers;
    [SerializeField] private int correctAnswerIndex;
    [SerializeField] private bool wasCorrect;
    [SerializeField] private int reward = 3;
    [SerializeField] private ScoringConditions[] scoringConditions;


    public string QuestionName { get => questionName; set { questionName = value; } }
    public string QuestionText { get => questionText; set { questionText = value; } }
    public Answer[] Answers { get => answers; set { answers = value; } }
    public Answer GetAnserByName (string text) => answers.FirstOrDefault(a => a.text == text);
    public int CorretAnswerIndex { get => correctAnswerIndex; set { correctAnswerIndex = value; } }
    public int Reward => reward;

    public ScoringConditions[] ScoringConditions { get => scoringConditions; set { scoringConditions = value; } }
    public Answer CorrectAnswer => answers[correctAnswerIndex];
    public bool WasCorrect { get => wasCorrect; set { wasCorrect = value; } }

    public int GetScoreForCharacter(List<string> tags)
    {
        int currentScore = tags.Contains("Users Right") ? reward : 0;

        for (int i = 0; i < scoringConditions.Length; i++)
        {
            for (int j = 0; j < scoringConditions[i].results.Length; j++)
            {
                ScoringResult currentResul = scoringConditions[i].results[j];


                for (int k = 0; k < tags.Count; k++)
                {
                    if (currentResul.characterName == tags[k])
                    {
                        AnswerResutl mostVoted = DataManager.main.GetMostVotedAnswer(questionName);
                        switch (scoringConditions[i].conditionType)
                        {
                            case ConditionType.MajorityVotesCorrect:
                                if (mostVoted.Answer == CorrectAnswer.text && mostVoted.Votes.Count >= scoringConditions[i].minVotesForCorrect)
                                    currentScore = currentResul.score;
                                break;
                            case ConditionType.MajorityVotesIncorrect:
                                if (mostVoted.Answer != CorrectAnswer.text && (PunManager.Instance.MaxRoomPlayers - mostVoted.Votes.Count) >= scoringConditions[i].minVotesForIncorrect)
                                    currentScore = currentResul.score;
                                break;
                            case ConditionType.MostVotedCharacter:
                                if (DataManager.main.GetMostVotedAnswer(questionName).Answer == scoringConditions[i].mostVotedCharacter)
                                    currentScore = currentResul.score;
                                break;
                        }


                    }
                }
            }
        }

        return currentScore;
    }
}
