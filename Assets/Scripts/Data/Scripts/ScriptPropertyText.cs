﻿using System;
using TMPro;
using UnityEngine;

namespace Data.Scripts
{
    [Serializable]
    public class ScriptPropertyText
    {
        [SerializeField] private string format = PropertyTemplate;
        [SerializeField] private TMP_Text txtProperty;

        private const string PropertyTemplate = "&{Property}";

        public string Property
        {
            get => txtProperty.text;
            set
            {
                if (txtProperty) txtProperty.text = format.Replace(PropertyTemplate, value);
            }
        }
    }
}