using System.Collections.Generic;
using UnityEngine;
using System;
using Managers;

[Serializable]
public class SerializedAccused 
{
    public int accusedId;
    public Sprite accusedImage;
    public string accusedName;
    public List<Vote> votes = new List<Vote>();
}

[Serializable]
public class AnswerResutl
{
    private int answerID;
    private Sprite picture;
    private string answer;
    private List<Vote> votes = new List<Vote>();
    private const string TO_STRING_FORMTAT = "Name {0}, ID {1}";
    public int AnswerId { get { return answerID; } set { answerID = value; } }
    public Sprite AnswerPic { get { return picture; } set { picture = value; } }
    public string Answer { get { return answer; } set { answer = value; } }
    public List<Vote> Votes { get { return votes; } }
    public void SetWithSerialized(SerializedAccused data) 
    {
        answerID = data.accusedId;
        answer = data.accusedName;
        picture = DataManager.main.GetCharacterPic(answer);
        votes = data.votes;
    }

    public SerializedAccused CreateSerializedReference() 
    {
        SerializedAccused serializedAccused = new SerializedAccused();
        serializedAccused.accusedId = answerID;
        serializedAccused.accusedImage = picture;
        serializedAccused.accusedName = answer;
        serializedAccused.votes = votes;
        return serializedAccused;
    }

    public void AddVote(Vote vote)
    {
        votes.Add(vote);
    }

    public override string ToString() => string.Format(TO_STRING_FORMTAT, answer, answerID);
}
