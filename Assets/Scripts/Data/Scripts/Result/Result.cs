using System.Collections.Generic;
using System;
using Managers;

[Serializable]
public class SerializedResult
{
    public string questionName;
    public List<SerializedAccused> accuseds = new List<SerializedAccused>();
}

[Serializable]
public class Result 
{
    public Result() { }
    public Result(string questionName) => this.questionName = questionName;
    private string questionName;
    private List<AnswerResutl> answers = new List<AnswerResutl>();

    public string QuestionName { get { return questionName; } set { questionName = value; } }
    public AnswerResutl[] Answers { get { return answers.ToArray(); } }

    public AnswerResutl GetAccused(string accusedName) => answers.Find(a => a.Answer == accusedName);

    public int CorrectAnswersAmount() 
    {
        string corretcAnswer = QuestionManager.main.GetQuestion(questionName).CorrectAnswer.text;
        AnswerResutl accused = GetAccused(corretcAnswer);
        return accused.Votes.Count;
    }  

    public void AddAccused(AnswerResutl accused) => answers.Add(accused);

    public void CreateFromSerialized(SerializedResult serializedResult) 
    {
        questionName = serializedResult.questionName;
        answers.Clear();

        for (int i = 0; i < serializedResult.accuseds.Count; i++)
        {
            answers.Add(new AnswerResutl());
            answers[i].SetWithSerialized(serializedResult.accuseds[i]);
        }
    }

    public SerializedResult CreateSerialized() 
    {
        SerializedResult serializedResult = new SerializedResult();

        serializedResult.questionName = questionName;

        for (int i = 0; i < answers.Count; i++)
        {
            serializedResult.accuseds.Add(answers[i].CreateSerializedReference());
        }

        return serializedResult;
    }

    public void ClearData() 
    {
        answers.Clear();
    }
}
