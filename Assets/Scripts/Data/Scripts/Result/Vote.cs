using UnityEngine;
using System;

[Serializable]
public struct Vote 
{
    public Sprite CharacterImage;
    public string UserName;

    public Vote(Sprite sprite, string name) 
    {
        CharacterImage = sprite;
        UserName = name;    
    }
}
