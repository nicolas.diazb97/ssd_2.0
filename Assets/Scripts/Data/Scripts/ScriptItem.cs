using Data.Scripts;
using Screens;
using UnityEngine;
using UnityEngine.UI;
using Utils.ScreensUtils;

namespace Data.ScriptsData
{
    [RequireComponent(typeof(ScriptPropertiesHandler), typeof(FilterItem))]
    public class ScriptItem : MonoBehaviour
    {
        [SerializeField] private Button btnOpen;
        private ScriptPropertiesHandler _propertiesHandler;
        private GameScript _scriptData;
        private string titleItem;
        [SerializeField] private GameObject isPurchasedLabel;
        private FilterItem _filterItem;

        public Button OpenButton => btnOpen;

        private void Awake()
        {
            _filterItem = GetComponent<FilterItem>();
            _propertiesHandler = GetComponent<ScriptPropertiesHandler>();
        }

        public void Setup(GameScript script)
        {
            _propertiesHandler.Setup(_scriptData = script);
            _filterItem.Init(
                new SearchTag(_scriptData.Theme),
                new SearchTag(_scriptData.ScriptDifficulty.ToString()),
                new SearchTag(_scriptData.Duration),
                new SearchTag(_scriptData.MinPlayers),
                new SearchTag(_scriptData.PriceType.ToString()),
                new SearchTag(_scriptData.DurationRange)
            );
            titleItem = FormatSName(script.Title);
        }
        public string GetTitle()
        {
            return titleItem;
        }

        public void MarkAsPurchased()
        {
            DebugMaster.debugInstance.AddMesaggeToConsole($"{titleItem} MarkAsPurchased");
           // Debug.Log($"{titleItem} MarkAsPurchased");
            isPurchasedLabel.SetActive(true);
        }
        public void MarkAsNotPurchased()
        {
            DebugMaster.debugInstance.AddMesaggeToConsole($"{titleItem} MarkAsNotPurchased");
            //Debug.Log($"{titleItem} MarkAsNotPurchased");
            isPurchasedLabel.SetActive(false);
        }

        private void OnEnable()
        {
            btnOpen.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            btnOpen.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            var itemTable = ScreenManager.main.OpenScreen<ScriptItemTable>();
            itemTable.SetupInfo(_scriptData);
            ScreenManager.main.CloseScreen(SerializableScreen.Type.Menu);
        }

        private string FormatSName(string sName)
        {
            sName = sName.ToLower();
            char[] b = sName.ToCharArray();
            string scriptItem = "";

            foreach (char item in b)
            {

                if (item == ' ')
                {
                    scriptItem += '_';
                }
                else
                {
                    scriptItem += item;
                }
            }

            return scriptItem;
        }
    }
}