﻿using Data.ScriptsData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Data.Scripts
{
    public class ScriptCharacter : MonoBehaviour
    {
        [SerializeField] private TMP_Text txtName;
        [SerializeField] private Image imgThumbnail;

        public void Setup(GameScript.Character characterInfo)
        {
            txtName.text = characterInfo.name;
            imgThumbnail.sprite = characterInfo.picture;
        }
    }
}