﻿using System;
using UnityEngine;
using SSD.GameData;

namespace Data.Scripts
{
    public partial class GameScript
    {
        [Serializable]
        public class Character
        {
            public string name;
            public Gender gender;
            public int age;
            public string occupation;
            public Sprite picture;
            [SerializeField, TextArea(6, 10)] public string personalStoryText;
            [SerializeField, TextArea(6, 10)] public string personalTodayText;
            public ParagraphStruct[] personalStory;
            public ParagraphStruct[] today;
            [SerializeField, TextArea(6, 10)] public string task;
            public SearchPlace.RoundSetup[] setupsPerRound;
            public Question[] personalQuestions;

            public void SetPersonToday()
            {
                string[] dividedString = personalTodayText.Split('.');

                if (today.Length == byte.MinValue)
                {
                    today = new ParagraphStruct[dividedString.Length];
                    for (int i = 0; i < dividedString.Length; i++)
                    {
                        if (string.IsNullOrEmpty(dividedString[i])) continue;
                        today[i] = new ParagraphStruct
                        {
                            Text = string.Concat(dividedString[i], ". "),
                            ScrollRectPosition = 0,
                        };
                    }
                }
                else 
                {
                    int lenght = today.Length + dividedString.Length;

                    ParagraphStruct[] aux = today;
                    today = new ParagraphStruct[lenght];

                    for (int i = 0; i < aux.Length; i++)
                    {
                        today[i] = aux[i];
                    }
                    int j = 0;
                    for (int i = aux.Length; i < (aux.Length + dividedString.Length); i++)
                    {
                        if (string.IsNullOrEmpty(dividedString[j])) continue;
                        today[i] = new ParagraphStruct
                        {
                            Text = string.Concat(dividedString[j], ". "),
                            ScrollRectPosition = 0,
                        };
                        j++;
                    }
                }
            }

            public void SetPersonStory()
            {
                string[] dividedString = personalStoryText.Split('.');


                if (personalStory.Length == byte.MinValue)
                {
                    personalStory = new ParagraphStruct[dividedString.Length];
                    for (int i = 0; i < dividedString.Length; i++)
                    {
                        if (string.IsNullOrEmpty(dividedString[i])) continue;
                        personalStory[i] = new ParagraphStruct
                        {
                            Text = string.Concat(dividedString[i], ". "),
                            ScrollRectPosition = 0,
                        };
                    }
                }
                else 
                {
                    int lenght = personalStory.Length + dividedString.Length;

                    ParagraphStruct[] aux = personalStory;
                    personalStory = new ParagraphStruct[lenght];

                    for (int i = 0; i < aux.Length; i++)
                    {
                        personalStory[i] = aux[i];
                    }
                    int j = 0;
                    for (int i = aux.Length; i < (aux.Length + dividedString.Length); i++)
                    {
                        if (string.IsNullOrEmpty(dividedString[j])) continue;
                        personalStory[i] = new ParagraphStruct
                        {
                            Text = string.Concat(dividedString[j], ". "),
                            ScrollRectPosition = 0,
                        };
                        j++;
                    }
                }
            }
        }
    }
}