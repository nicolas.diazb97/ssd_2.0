﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Data.Scripts
{
    [Serializable]
    public class ScriptProperties
    {
        [Header("Single properties")]
        [SerializeField] private Image imgThumbnail;
        [SerializeField] private ScriptPropertyText txtTitle;
        [SerializeField] private ScriptPropertyText txtPlayers;
        [SerializeField] private ScriptPropertyText txtDuration;
        [SerializeField] private ScriptPropertyText txtWords;
        [SerializeField] private ScriptPropertyText txtTheme;
        [SerializeField] private ScriptPropertyText txtDifficulty;
        [SerializeField] private ScriptPropertyText txtAuthor;

        [Header("Multiple properties")]
        [SerializeField] private Transform charactersParent;
        [SerializeField] private ScriptCharacter characterPrefab;

        public void SetProperties(GameScript script)
        {
            if (imgThumbnail) imgThumbnail.sprite = script.Thumbnail;
            txtTitle.Property = script.Title;
            txtPlayers.Property = script.MinPlayers.ToString();
            txtDuration.Property = script.Duration.ToString();
            txtWords.Property = script.WordCount.ToString();
            txtTheme.Property = script.Theme;
            txtDifficulty.Property = script.ScriptDifficulty.ToString();
            txtAuthor.Property = script.Author;

            if (!charactersParent) return;

            foreach (Transform child in charactersParent)
            {
                Object.Destroy(child);
            }

            foreach (var character in script.Characters)
            {
                var characterInstance = Object.Instantiate(characterPrefab, charactersParent);
                characterInstance.Setup(character);
            }
        }
    }
}