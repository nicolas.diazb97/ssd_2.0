﻿using System.Diagnostics.Contracts;
using System.Linq;
using Managers;
using UnityEngine;
using UnityEngine.Serialization;
using SSD.GameData;
using System;
using UnityEditor;
using System.Collections.Generic;

namespace Data.Scripts
{

#if UNITY_EDITOR
    [CustomEditor(typeof(GameScript), true)]
    public class CharacterCustomInspector : Editor
    {
        private GameScript gameScript;
        void OnEnable()
        {
            gameScript = (GameScript)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Refresh Characters"))
            {
                gameScript.CreateNumberList();
            }

            if (GUILayout.Button("Append text to current character story"))
            {
                gameScript.SetCurrentCharacterPersonalStory();
            }
            if (GUILayout.Button("Append text to current character today"))
            {
                gameScript.SetCurrentCharacterToday();
            }
        }
    }
#endif
    public interface IScoringStrategy
    {
        public float CalculateScore(string currentCharacter);
    }
    public enum TimelineTitle 
    {
        Timeline,
        Today,
        Tonight
    }

    [CreateAssetMenu(fileName = "New Game Script", menuName = "Data/Game Script", order = 0)]
    public partial class GameScript : ScriptableObject
    {
        [SerializeField] public int scriptIndex;
        [SerializeField] private int scriptId;
        [SerializeField] private Sprite thumbnail;
        [SerializeField] private string title;

        [SerializeField, Tooltip("Duration in minutes")]
        private float duration = 20;

        [SerializeField] private TimelineTitle timelineTitle;
        [SerializeField] private string durationRange = "1-2 hrs";
        [SerializeField] private int wordCount = 700;
        [SerializeField] private string theme;
        [SerializeField] private Difficulty difficulty;
        [SerializeField, TextArea(6, 20)] private string brief;
        [SerializeField] private ParagraphStruct[] background;
        [SerializeField] private ParagraphStruct[] truth;
        [SerializeField] private Character[] characters;
        [SerializeField] private string author;
        [FormerlySerializedAs("price")] [SerializeField] private Price priceType;
        [SerializeField] private GameScriptType scriptType;
        [SerializeField] private SpriteRenderer searchMap;
        [SerializeField] private bool isDebugScript;
        [SerializeField] private Question[] questions;
        [SerializeField] private int accusedQuestionIndex = 0;
        [SerializeField] private RoundConfig[] configsPerRound;
        [SerializeField] private AudioClip backgroundMusic;
        [SerializeField] public bool isUnderDev;
        [SerializeField] private string currentCharacter;

        public static List<string> myList;
#if UNITY_EDITOR
        [ListToPopUp(typeof(GameScript), "myList")]
#endif
        public string popup;
        [ContextMenu("RefreshCharacters")]
        public void CreateNumberList()
        {
            myList = new List<string>();
            foreach (var character in characters)
            {
                myList.Add(character.name);

            }
            myList.Add("Users Right");
            myList.Add("Users Wrong");
        }

        public void SetCurrentCharacterPersonalStory() 
        {
            Character character = characters.FirstOrDefault(c => c.name == currentCharacter);
            character.SetPersonStory();
        }

        public void SetCurrentCharacterToday()
        {
            Character character = characters.FirstOrDefault(c => c.name == currentCharacter);
            character.SetPersonToday();
        }

        public int PurchasePrice => Billing.sharedInstence.GetScriptPriceInt(Billing.FormatSName(title));

        public Question[] Questions => questions;
        public Question AccusedQuestion => questions[accusedQuestionIndex];
        public int AccusedQuestionIndex => accusedQuestionIndex;
        public TimelineTitle TimelineTitle => timelineTitle;

        public int ScriptId => scriptId;

        public Sprite Thumbnail => thumbnail;

        public string Title => title;

        public int MinPlayers => characters.Length;

        public float Duration => duration;

        public int WordCount => wordCount;

        public string Theme => theme;

        public Difficulty ScriptDifficulty => difficulty;

        public string Brief => brief;
        public ParagraphStruct[] Background => background;

        public ParagraphStruct[] Truth => truth;
        public Character[] Characters => characters;
        public string Author => author;

        public Price PriceType => priceType;

        public string DurationRange => durationRange;

        public GameScriptType ScriptType => scriptType;

        public SpriteRenderer SearchMap => searchMap;

        public bool IsDebugScript => isDebugScript;

        public RoundConfig[] ConfigsPerRound => configsPerRound;

        public AudioClip BackgroundMusic => backgroundMusic;

        [Pure]
        public Character GetCharacterByName(string characterName)
        {
            var selectedCharacter = characters.SingleOrDefault(character => character.name == characterName);
            if (selectedCharacter != null) return selectedCharacter;

            Debug.LogError($"Unable to find a character named {characterName} in script {title}.");
            return null;
        }
        
#if UNITY_EDITOR
        public class ListToPopUpAttribute : PropertyAttribute
        {
            public Type myType;
            public string propertyName;
            public ListToPopUpAttribute(Type _mytype, string _propertyName)
            {
                myType = _mytype;
                propertyName = _propertyName;
            }
        }
        [CustomPropertyDrawer(typeof(ListToPopUpAttribute))]
        public class listToPopUpDrawer : PropertyDrawer
        {
            public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                ListToPopUpAttribute popUpAttribute = attribute as ListToPopUpAttribute;
                List<string> stringList = null;
                if (popUpAttribute.myType.GetField(popUpAttribute.propertyName) != null)
                {
                    stringList = popUpAttribute.myType.GetField(popUpAttribute.propertyName).GetValue(popUpAttribute.myType) as List<string>;
                }
                if (stringList != null && stringList.Count != 0)
                {
                    int selectedIndex = Mathf.Max(stringList.IndexOf(property.stringValue), 0);
                    selectedIndex = EditorGUI.Popup(position, property.name, selectedIndex, stringList.ToArray());
                    property.stringValue = stringList[selectedIndex];
                }
                else EditorGUI.PropertyField(position, property, label);
            }
        }
#endif
    }
}