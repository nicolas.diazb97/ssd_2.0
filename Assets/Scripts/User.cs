using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User
{
    public string Id;
    public string Email;
    public string Name;
    public string PhotoUrl;
    public string Scripts;
    public string Online;
    public string RoomId;
    public List<string> playedScripts = new List<string>();
}
