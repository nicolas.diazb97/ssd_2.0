using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[Serializable]
public class SerializableScreen
{
    public ScreenTemplate screenInstance;
    public Type screenType;

    public enum Type
    {
        Login, TermsConditions, PolicyPrivacy, Home, Menu,
        GameLobby, ScriptCollection, Profile, HelpPage,
        WhatIs, UiTutorial, ScriptDetail, GameRoom, GameScreen, ResetScreen,
        CreateRoom, PasswordPopup, DialoguePopup, CluesOrganizer, SearchPlace,
        CluePopup, ConfirmPopup, ScriptsOrganizer, RoundsPopup, MenuScriptsOrganizer,
        VotingResults, InGameQuestions, Evaluation, CharacterSearchPlace, CharacterInfo

    }
}

public class ScreenManager : Singleton<ScreenManager>
{
    [SerializeField] private SerializableScreen[] screens;
    private readonly Stack<ScreenTemplate> _openedScreens = new Stack<ScreenTemplate>();

    public override void Awake()
    {
        base.Awake();

        foreach (var screenTemplate in screens.Select(screen => screen.screenInstance.gameObject))
        {
            screenTemplate.SetActive(false);
        }
    }

    /// <summary>
    /// Opens a new screen.
    /// </summary>
    /// <param name="screenType">The type of the opened screen.</param>
    /// <param name="closeCurrent">Should the current screen be closed once the new one opens?</param>
    /// <remarks>The opened screen must be assigned in <see cref="screens"/> from within the editor.</remarks>
    public ScreenTemplate OpenScreen(SerializableScreen.Type screenType, bool closeCurrent = true)
    {
        var screen = screens.FirstOrDefault(screen => screen.screenType == screenType);
        if (screen != null) return HandleOpenScreen(screen, closeCurrent);
        
        Debug.LogError($"No screen is assigned for screen type {screenType}");
        return null;

    }

    public void TurnScreen(SerializableScreen.Type screenType)
    {
        var screen = screens.FirstOrDefault(screen => screen.screenType == screenType);
        var instance = screen.screenInstance;

        if (instance.gameObject.activeSelf == false)
        {
            instance.Init();
        }
        else
        {
            instance.Close();
        }
    }

    public TScreen OpenScreen<TScreen>(bool closeCurrent = true) where TScreen : ScreenTemplate
    {
        var screen = screens.FirstOrDefault(screen => screen.screenInstance.GetType() == typeof(TScreen));

        if (screen != null) return HandleOpenScreen(screen, closeCurrent) as TScreen;

        Debug.LogError($"No screen is assigned for screen type {typeof(TScreen).Name}");
        return null;
    }

    private ScreenTemplate HandleOpenScreen(SerializableScreen screen, bool closeCurrent)
    {
        if (screen.screenInstance.IsOpen)
        {
            Debug.LogWarning($"The screen {screen.screenType} is already open.");
            return null;
        }
        
        Debug.Log($"Opening screen {screen.screenType}");
        if (closeCurrent)
        {
            CloseScreen();
        }

        var instance = screen.screenInstance;
        instance.Init();
        _openedScreens.Push(instance);

        return instance;
    }
    [ContextMenu("DebugStack")]
    private void PrintStackState()
    {
        string items = "";
        int a = 0;
        foreach (var item in _openedScreens)
        {
            a++;
            items += ($"{a} {item.name}\n");
        }
        Debug.Log($"----STACK----\n{items}\n---------");

    }

    private void CloseAllScreens()
    {
        if (_openedScreens.Count < 1) return;

        foreach (var screenTemplate in _openedScreens)
        {
            screenTemplate.Close();
            _openedScreens.Pop();
        }
    }

    /// <summary>
    /// Close the currently opened screen.
    /// </summary>
    public void CloseScreen()
    {
        if (_openedScreens.Count < 1) return;

        var screenTemplate = _openedScreens.Pop();
        if (!screenTemplate.IsOpen)
        {
            Debug.LogWarning($"The screen {screenTemplate.GetType().Name} is already closed.");
            return;
        }
        screenTemplate.Close();
    }

    /// <summary>
    /// Close a specific screen identified with its type, whether it's opened or not.
    /// </summary>
    /// <param name="screenType">The type identifier of the screen to be closed.</param>
    public void CloseScreen(SerializableScreen.Type screenType)
    {
        var selected = screens.FirstOrDefault(screen => screen.screenType == screenType);

        if (selected == null)
        {
            Debug.LogError($"No screen is assigned for screen type {screenType}");
            return;
        }
        
        if (!selected.screenInstance.IsOpen)
        {
            Debug.LogWarning($"The screen {selected.screenType} is already closed.");
            return;
        }

        selected.screenInstance.Close();
    }

    public void ResetAllScreens() 
    {
        for (int i = 0; i < screens.Length; i++)
        {
            screens[i].screenInstance.ResetIt();
        }
    }
}
