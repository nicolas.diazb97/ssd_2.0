using Facebook.Unity;
using Firebase;
using Firebase.Auth;
using Google;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;


public class RestManager : Singleton<RestManager>
{
    public GameObject testImage;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private GoogleSignInConfiguration configuration;

    [Header("Config")]
    private string webClientId = "950475093865-cipa5c4moinbnentf6mpmakv8vjr2rn8.apps.googleusercontent.com";

    [SerializeField] private RealtimeDatabaseManager dataBase;

    [Header("DataSet")]
    public LoginData loginData;

    private string userName;
    private string userEmail;
    private string userId;
    private string userPic;

    private bool googleLogIn = false;


    public override void Awake()
    {
        base.Awake();
        configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestEmail = true, RequestIdToken = true };
        CheckFirebaseDependencies();
        CheckFBDependencies();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public string GetNickName()
    {
        if (userName != "")
        {
            return userName;
        }
        else
        {
            return "noAuth";
        }
    }

    public string Getid()
    {
        if (userId != "")
        {
            return userId;
        }
        else
        {
            return "Player " + UnityEngine.Random.Range(0, 1000).ToString("0000");
        }
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
       // Debug.LogError("esto hace algo gvn" + auth.CurrentUser);
        if (auth.CurrentUser != user)
        {
       // Debug.LogError("es diferente esta monda" );
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.LogError("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.LogError("Signed in " + user.UserId);
                StartCoroutine(StartSession(user.UserId, user.DisplayName, user.Email, user.PhotoUrl.ToString() + "?width=256&height=256"));
                //displayName = user.DisplayName ?? "";
                //emailAddress = user.Email ?? "";
                //userID = user.UserId ?? "";
                //if (!newuser)
                //{
                //    SceneManager.LoadScene("Home");
                //}
            }
        }
    }

    private void CheckFBDependencies()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isgameshown)
    {
        if (!isgameshown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void Facebook_Login()
    {

        FB.LogOut();
        var permission = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(permission, AuthCallBack);
        Util.utilInstance.DisableButton("Facebook");
        Util.utilInstance.DisableButton("Google");
    }

    private void AuthCallBack(ILoginResult result)
    {
        /* if (FB.IsLoggedIn)
         {
             var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
             //  debug.text = (aToken.UserId);

             string accesstoken;
             string[] data;
             string acc;
             string[] some;
 #if UNITY_EDITOR
             Debug.Log("this is raw access " + result.RawResult);
             data = result.RawResult.Split(',');
             Debug.Log("this is access" + data[3]);
             acc = data[3];
             some = acc.Split('"');
             Debug.Log("this is access " + some[3]);
             infoText.text = infoText.text + "Facebook Works" + some[3].ToString();
             accesstoken = some[3];

 #elif UNITY_ANDROID || UNITY_IOS
             Debug.Log("this is raw access "+result.RawResult);
             data = result.RawResult.Split(',');
             Debug.Log("this is access"+data[0]);
              acc = data[0];
              some = acc.Split('"');
             Debug.Log("this is access " + some[3]);


             //infoText.text = infoText.text +"Facebook Works"+ some[3].ToString();
              accesstoken = some[3];
 #endif
             OnFBAuthFinished(aToken.TokenString);
         }
         else
         {
             Debug.Log("User Cancelled login");
         }
         FB.API("/me?fields=id,first_name,last_name,gender,email", HttpMethod.GET, GraphCallback);
         */

        if (FB.IsLoggedIn)
        {
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            //  debug.text = (aToken.UserId);


#if UNITY_EDITOR
            Debug.Log(aToken.TokenString);
            Debug.Log(aToken.UserId);
#elif UNITY_ANDROID || UNITY_IOS
             Debug.Log(aToken.TokenString );
          Debug.Log(aToken.UserId);
           
#endif
            OnFBAuthFinished(aToken.TokenString);
        }
        else
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("User Cancelled login");
        }
        FB.API("/me?fields=id,first_name,last_name,gender,email", HttpMethod.GET, GraphCallback);
    }

    // testing

    private void GraphCallback(IGraphResult result)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("hi= " + result.RawResult);

        /*
        string id;
        string firstname;
        string lastname;
        string gender;
        string email;
     
       if (result.ResultDictionary.TryGetValue("id", out id))
       {
           infoText.text += "ID = " + id.ToString();
       }
       if (result.ResultDictionary.TryGetValue("first_name", out firstname))
       {
           infoText.text += "firstname= " + firstname.ToString();
       }
       if (result.ResultDictionary.TryGetValue("last_name", out lastname))
       {
           infoText.text += "lastname= " + lastname.ToString();
       }
       if (result.ResultDictionary.TryGetValue("gender", out gender))
       {
           infoText.text += "gender= " + gender.ToString();
       }
       if (result.ResultDictionary.TryGetValue("email", out email))
       {
           infoText.text += "email= " + email.ToString();
       }
        */
    }

    public void OnFBAuthFinished(string accesstoken)
    {
        auth = FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
        Firebase.Auth.Credential credential = Firebase.Auth.FacebookAuthProvider.GetCredential(accesstoken);
        auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                DebugMaster.debugInstance.AddMesaggeToConsole("singin encountered error" + task.Exception);
            }
            Firebase.Auth.FirebaseUser newuser = task.Result;

            DebugMaster.debugInstance.AddMesaggeToConsole("Logging in with facebook");
            googleLogIn = false;
            StartCoroutine(StartSession(newuser.UserId, newuser.DisplayName, newuser.Email, newuser.PhotoUrl.ToString() + "?width=256&height=256"));

        });
    }

    private void CheckFirebaseDependencies()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                if (task.Result == DependencyStatus.Available)
                {
                    auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
                 //   Debug.LogError("Firebase initialized");
                    auth.StateChanged += AuthStateChanged;
                    AuthStateChanged(this, null);
                    //auth.StateChanged += AuthStateChanged;
                    //AuthStateChanged(this, null);

                }
                else
                    DebugMaster.debugInstance.AddMesaggeToConsole("Could not resolve all Firebase dependencies: " + task.Result.ToString());
            }
            else
            {
                DebugMaster.debugInstance.AddMesaggeToConsole("Dependency check was not completed. Error : " + task.Exception.Message);
            }
        });
    }

    public void SignInWithGoogle() { OnGoogleSignIn(); }

    public void SignOutFromGoogle() { OnSignOut(); }

    IEnumerator FixGoogleSignIn()
    {
#if UNITY_EDITOR
        ForceOnSignOut();
#endif
        yield return new WaitForEndOfFrame();
        OnGoogleSignIn();
        yield return new WaitForEndOfFrame();
    }

    private void OnGoogleSignIn()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        TestUser();
#elif UNITY_ANDROID || UNITY_IOS
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnGoogleAuthFinished);
#endif
    }

    private void OnSignOut()
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("SignOut");
        auth.SignOut();
        if (googleLogIn)
        {
            GoogleSignIn.DefaultInstance.SignOut();
            googleLogIn = false;
        }

    }

    public void ForceOnSignOut()
    {
        auth.SignOut();
        DebugMaster.debugInstance.AddMesaggeToConsole("SignOut");
        GoogleSignIn.DefaultInstance.SignOut();
        googleLogIn = false;
    }

    public void OnDisconnect()
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("Calling Disconnect");
        GoogleSignIn.DefaultInstance.Disconnect();
    }

    internal void OnGoogleAuthFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            using (IEnumerator<Exception> enumerator = task.Exception.InnerExceptions.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    FailUser();
                    GoogleSignIn.SignInException error = (GoogleSignIn.SignInException)enumerator.Current;
                    DebugMaster.debugInstance.AddMesaggeToConsole("Got Error: " + error.Status + " " + error.Message);
                    Util.utilInstance.ActiveButton("Facebook");
                    Util.utilInstance.ActiveButton("Google");

                }
                else
                {
                    DebugMaster.debugInstance.AddMesaggeToConsole("Got Unexpected Exception?!?" + task.Exception);
                }
            }
        }
        else if (task.IsCanceled)
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("Canceled");
        }
        else
        {
            DebugMaster.debugInstance.AddMesaggeToConsole("Logging in with google");
            SignInWithGoogleOnFirebase(task.Result.IdToken);
            googleLogIn = true;
            StartCoroutine(StartSession(task.Result.UserId, task.Result.DisplayName, task.Result.Email, task.Result.ImageUrl.ToString() + "?width=256&height=256"));
        }
    }

    public void DataSession(string name, string email, string id, string pic)
    {
        StartCoroutine(StartSession(id, name, email, pic));
    }

    //1.0: Función encargada de tomar los datos desde el login o los perfiles de prueba para iniciar sesión.

    IEnumerator StartSession(string id, string name, string email, string pic)
    {
        Util.utilInstance.DisableButton("Facebook");
        Util.utilInstance.DisableButton("Google");

        DebugMaster.debugInstance.AddMesaggeToConsole("Trying to start session");
        loginData = new LoginData(id, name, email, pic);
        yield return new WaitForEndOfFrame();
        dataBase.gameObject.SetActive(true);
        yield return new WaitForEndOfFrame();
        dataBase.GetUserDataInfo(loginData);

        //Esta función activa el objeto que tiene el script que gestiona la base de datos y le pasa, a través del LoginData.cs, los datos del usuario.
    }

    private void SignInWithGoogleOnFirebase(string idToken)
    {
        Credential credential = GoogleAuthProvider.GetCredential(idToken, null);

        auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        {
            AggregateException ex = task.Exception;
            if (ex != null)
            {
                if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
                    DebugMaster.debugInstance.AddMesaggeToConsole("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
            }
            else
            {
                DebugMaster.debugInstance.AddMesaggeToConsole("Sign In Successful.");
            }
        });
    }
    public void OnSignInSilently()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        DebugMaster.debugInstance.AddMesaggeToConsole("Calling SignIn Silently");

        GoogleSignIn.DefaultInstance.SignInSilently().ContinueWith(OnGoogleAuthFinished);

    }

    public void OnGamesSignIn()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = true;
        GoogleSignIn.Configuration.RequestIdToken = false;

        DebugMaster.debugInstance.AddMesaggeToConsole("Calling Games SignIn");

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnGoogleAuthFinished);

    }

    //1.1: Perfiles de prueba accesibles desde el editor.

    [ContextMenu("FailUser")]
    public void FailUser()
    {
        StartCoroutine(StartSession("FailId12345FaiL", "FailUser", "Fail@email.com", "https://i.imgur.com/QsFAQso.jpeg"));
    }
    [ContextMenu("TestUser")]
    public void TestUser()
    {
        StartCoroutine(StartSession("TestId12345Test", "TestUser", "Test@email.com", "https://i.imgur.com/JKhvFzm.jpeg"));
    }

    [ContextMenu("PanDorado")]
    public void PanDorado()
    {
        StartCoroutine(StartSession("DoradoId12345Dorado", "PanDorado", "PanDorado@email.com", "https://i.imgur.com/On4mANU.jpeg"));
    }
    [ContextMenu("PanDeAvena")]
    public void PanDeAvena()
    {
        StartCoroutine(StartSession("AvenaId12345Avena", "PanDeAvena", "PanDeAvena@email.com", "https://i.imgur.com/gasMrbn.jpeg"));
    }
    [ContextMenu("PanDeAjo")]
    public void PanDeAjo()
    {
        StartCoroutine(StartSession("AjoId12345Ajo", "PanDeAjo", "PanDeAjo@email.com", "https://i.imgur.com/gwFGiZ1.jpeg"));
    }
    [ContextMenu("PanCook")]
    public void PanCook()
    {
        StartCoroutine(StartSession("CookId12345Cook", "PanCook", "PanCook@email.com", "https://i.imgur.com/KH7D1Uv.jpeg"));
    }
    [ContextMenu("PanRoscon")]
    public void PanRoscon()
    {
        StartCoroutine(StartSession("RosconId12345Roscon", "PanRoscon", "PanRoscon@email.com", "https://i.imgur.com/2nzw9D0.jpeg"));
    }
    [ContextMenu("PanDe200")]
    public void PanDe200()
    {
        StartCoroutine(StartSession("200Id12345", "PanDe200", "PanDe200@email.com", "https://i.imgur.com/Z43r6CK.jpeg"));
    }

    [ContextMenu("Helen")]
    public void Helen()

    {
        StartCoroutine(StartSession("103604396327097688589", "Helen", "baobaohu10@gmail.com", "/profilePics/103604396327097688589/5c4a05e0-b914-4f96-9a39-7173b5d3b878.png"));
    }
    [ContextMenu("Helen2")]
    public void Helen2()

    {
        StartCoroutine(StartSession("rNWkPek8kXQxrC1KMNQckP9A1jQ2", "Helen", "", "/profilePics/rNWkPek8kXQxrC1KMNQckP9A1jQ2/Helen HuHelen Huab371e81-a3e6-499b-85e4-012fcd25b480.png"));
    }

}
