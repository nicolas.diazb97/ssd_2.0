using System;
using System.Collections;
using Firebase.Storage;
using UnityEngine;
using UnityEngine.UI;
public class StorageImage : MonoBehaviour
{
    public Image originImg;
    public Image destinyImg;

    public void StartUpload()
    {
        StartCoroutine(UploadCoroutine(TextureToTexture2D(originImg.sprite.texture)));
    }
    private IEnumerator UploadCoroutine(Texture2D screenshot)
    {
        var storage = FirebaseStorage.DefaultInstance;
        string path = $"/profilePics/{Guid.NewGuid()}.png";
        var screenshotReference = storage.GetReference(path);
        var bytes = screenshot.EncodeToPNG();
        var uploadTask = screenshotReference.PutBytesAsync(bytes);
        yield return new WaitUntil(predicate: () => uploadTask.IsCompleted);
        if (uploadTask.Exception != null)
        {
            Debug.LogError($"Failed to upload because {uploadTask.Exception}");
            yield break;
        }
        var getUrlTask = screenshotReference.GetDownloadUrlAsync();
        yield return new WaitUntil(predicate: () => getUrlTask.IsCompleted);

        if (getUrlTask.Exception != null)
        {
            Debug.LogError($"Failed to get download url with {getUrlTask.Exception}");
            yield break;
        }
        Debug.Log($"Download from {getUrlTask.Result}");
        StartCoroutine(DownloadScreenshotCoroutine(path));
    }

    private IEnumerator DownloadScreenshotCoroutine(string path)
    {
        var storage = FirebaseStorage.DefaultInstance;
        var screenshotReference = storage.GetReference(path);

        var downloadTask = screenshotReference.GetBytesAsync(long.MaxValue);
        yield return new WaitUntil(predicate: () => downloadTask.IsCompleted);

        // StorageReference httpsReference = storage.GetReferenceFromUrl("https://firebasestorage.googleapis.com/b/bucket/o/images%20stars.jpg");

        var texture = new Texture2D(width: 2, height: 2); texture.LoadImage(downloadTask.Result);
        var mySprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
        destinyImg.sprite = mySprite;
    }

    private Texture2D TextureToTexture2D(Texture texture)
    {
        Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture renderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 32);
        Graphics.Blit(texture, renderTexture);

        RenderTexture.active = renderTexture;
        texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture2D.Apply();

        RenderTexture.active = currentRT;
        RenderTexture.ReleaseTemporary(renderTexture);
        return texture2D;
    }




}
