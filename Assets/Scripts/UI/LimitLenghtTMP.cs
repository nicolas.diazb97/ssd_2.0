using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class LimitLenghtTMP : MonoBehaviour
{
    public TextMeshProUGUI textToChange;
    public int maxCharacters;
    public UnityEvent OnMassiveText;
    // Start is called before the first frame update
    void Start()
    {
        CheckLenght();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CheckLenght()
    {
        if (textToChange.text.ToString().Length >= maxCharacters)
        {
            OnMassiveText.Invoke();
        }
    }
}
