﻿using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Screens.Utils
{
    [RequireComponent(typeof(Button))]
    public class LeaveRoomButton : MonoBehaviour
    {
        [SerializeField] private string leaveMsg;
        [SerializeField] private SerializableScreen.Type closeScreenOnLeave = SerializableScreen.Type.GameScreen;
        private Button _btnLeave;

        private void Awake()
        {
            _btnLeave = GetComponent<Button>();
        }

        private void OnEnable()
        {
            _btnLeave.onClick.AddListener(OnClickLeave);
        }

        private void OnDisable()
        {
            _btnLeave.onClick.RemoveListener(OnClickLeave);
        }

        public async void OnClickLeave()
        {
            ScreenManager.main.ResetAllScreens();
            DataManager.main.CurrentAcusedId = 0;
            for (int i = 0; i < DataManager.main.Results.Count; i++)
            {
                DataManager.main.Results[i].ClearData();
            }
            RealtimeDatabaseManager.rtdbmReference.SavePlayedScript(DataManager.main.CurrentRoomScript.Title);
            var confirmPopup = ScreenManager.main.OpenScreen<ConfirmPopup>(false);
            await confirmPopup.WaitUntilConfirmedAsync(leaveMsg);
            
            CbOnConfirm();
        }

        private void CbOnConfirm()
        {
            RealtimeDatabaseManager.rtdbmReference.UpdateRoomId("");
            PunManager.Instance.ReconectRoom = false;
            ScreenManager.main.CloseScreen(closeScreenOnLeave);
            PunManager.Instance.LeaveRoom();
        }
    }
}