﻿using System;
using System.Linq;
using Data.Scripts;
using Managers;
using Photon.Pun;
using Screens;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.ScreensUtils
{
    public class AssignableCharacter : MonoBehaviour
    {
        [SerializeField] private Button btnAssignPlayer;
        [SerializeField] private Button btnShowCharacterInfo;
        [SerializeField] private GameObject readyObj;
        
        [Header("Character Info")]
        [SerializeField] private Image imgCharacter;
        [SerializeField] private TMP_Text txtCharacterName;

        [Header("Player Info")] 
        [SerializeField] private Image imgPlayerPfp;
        [SerializeField] private TMP_Text txtPlayerName;

        private GameScript.Character _characterInfo;
        private string _initialPlayerNameValue;
        private Sprite _initialPlayerPfpValue;
        private Action _onAssignationUpdatedEvt;
        
        public bool IsAssigned { get; private set; }

        public void Init(Action cbOnPlayerAssigned, GameScript.Character character)
        {
            _initialPlayerNameValue = txtPlayerName.text;
            _initialPlayerPfpValue = imgPlayerPfp.sprite;
            
            txtCharacterName.text = (_characterInfo = character).name;
            imgCharacter.sprite = _characterInfo.picture;
            _onAssignationUpdatedEvt = cbOnPlayerAssigned;
            readyObj.gameObject.SetActive(false);
        }

        private void CheckPlayerAssigned(AssignationInfo[] assignations)
        {
            var selectedAssignation =
                assignations.FirstOrDefault(info => info.AssignedCharacter.name == _characterInfo.name);
            
            if (selectedAssignation != null)
            {
                Debug.Log($"Assignation info. Player id: {selectedAssignation.PlayerId}, Status: {selectedAssignation.IsReady}, Character: {selectedAssignation.AssignedCharacter.name}");
                UpdateVisuals(selectedAssignation.PlayerId);
                readyObj.SetActive(selectedAssignation.IsReady);
            }
            else
            {
                ResetDefaultVisuals();
            }
        }

        private void UpdateVisuals(string playerId)
        {
            IsAssigned = true;
            txtPlayerName.text = DataManager.main.GetPlayerNameById(playerId);
            btnAssignPlayer.interactable = false;
            DataManager.main.GetPlayerPfpById(playerId,imgPlayerPfp);

            _onAssignationUpdatedEvt?.Invoke();
        }

        private void ResetDefaultVisuals()
        {
            IsAssigned = false;
            readyObj.gameObject.SetActive(false);
            txtPlayerName.text = _initialPlayerNameValue;
            imgPlayerPfp.sprite = _initialPlayerPfpValue;
            btnAssignPlayer.interactable = true;
            
            _onAssignationUpdatedEvt?.Invoke();
        }

        public void SetInteractable(bool value)
        {
            if (IsAssigned) return;

            btnAssignPlayer.interactable = value;
        }

        #region Callback Handles

        public void AssignPlayerToCharacter()
        {
            if (PunManager.Instance.IsLocalPlayerReady) return;

            PunManager.Instance.NotifyAssignPlayerToCharacter(_characterInfo.name);
        }

        private void ShowCharacterInfo()
        {
            var popupInstance = ScreenManager.main.OpenScreen<CharacterInfoPopup>(false);
            popupInstance.Setup(_characterInfo);
        }

        #endregion

        #region Unity Callbacks

        private void OnEnable()
        {
            DataManager.main.OnCharacterAssignationsUpdated += CheckPlayerAssigned;
            btnAssignPlayer.onClick.AddListener(AssignPlayerToCharacter);
            btnShowCharacterInfo.onClick.AddListener(ShowCharacterInfo);
        }

        private void OnDisable()
        {
            DataManager.main.OnCharacterAssignationsUpdated -= CheckPlayerAssigned;
            btnAssignPlayer.onClick.RemoveListener(AssignPlayerToCharacter);
            btnShowCharacterInfo.onClick.RemoveListener(ShowCharacterInfo);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                var pfpUrl = PhotonNetwork.LocalPlayer.CustomProperties["Pfp"].ToString();
              
                Debug.Log($"Downloading pfp from source {pfpUrl}");
                //  var storage = FirebaseStorage.DefaultInstance;
                // StartCoroutine(BajarFoto(pfpUrl));
             
                // RealtimeDatabaseManager.rtdbmReference.DownloadPhoto(pfpUrl,imgPlayerPfp);
            }
        }

        #endregion
    }
}