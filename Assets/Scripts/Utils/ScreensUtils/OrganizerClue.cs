﻿using Data;
using Managers;
using Screens;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.ScreensUtils
{
    [RequireComponent(typeof(FilterItem))]
    public class OrganizerClue : MonoBehaviour
    {
        [SerializeField] private Image imgThumbnail;
        [SerializeField] private TMP_Text txtDesc;
        [SerializeField] private Button btnShowClueInfo;

        private FilterItem _filterHandler;
        private Clue _clueInfo;

        private void Awake()
        {
            _filterHandler = GetComponent<FilterItem>();
        }

        private void OnEnable()
        {
            btnShowClueInfo.onClick.AddListener(OnClickShowInfo);
        }

        private void OnDisable()
        {
            btnShowClueInfo.onClick.RemoveListener(OnClickShowInfo);
        }

        private void OnClickShowInfo()
        {
            Debug.LogError("click show "+_clueInfo.Title);
            var foundPanel = ScreenManager.main.OpenScreen<ClueFoundPanel>(false);
            if (_clueInfo.NestedClue.Count > 0)
            {
                List<Clue> matchedClues = new List<Clue>();
                matchedClues.Clear();
                foreach (var item in _clueInfo.NestedClue)
                {
                    List<Clue> tempMatchedClues = new List<Clue>();
                    tempMatchedClues.Clear();
                    tempMatchedClues = DataManager.main._foundClues.Where(fc => fc.Title.Equals(item.Title)).ToList();
                    if (tempMatchedClues.Count > 0)
                    {
                        matchedClues.Add(tempMatchedClues[0]);
                    }

                }
                if (matchedClues.Count >= _clueInfo.NestedClue.Count)
                {
                    foundPanel.Setup(_clueInfo, false);
                }
                else
                {
                    foundPanel.Setup(_clueInfo, true);
                }
            }
            else
            {
                foundPanel.Setup(_clueInfo, false);
            }
            Debug.LogError("finaliza ejecucion");
        }

        public void Setup(Clue clue)
        {
            _clueInfo = clue;
            if (imgThumbnail) imgThumbnail.sprite = clue.Thumbnail;
            if (txtDesc) txtDesc.text = clue.Description;

            if (string.IsNullOrEmpty(clue.TagFound))
            {
                _filterHandler.Init(new SearchTag(clue.FoundIn.PlaceName), new SearchTag(clue.FoundBy));
            }
            else
            {
                _filterHandler.Init(new SearchTag(clue.TagFound), new SearchTag(clue.FoundBy));
            }
        }

        public void Setup(Clue clue, string customText)
        {
            _clueInfo = clue;
            if (imgThumbnail) imgThumbnail.sprite = clue.Thumbnail;
            if (txtDesc) txtDesc.text = customText;

            _filterHandler.Init(new SearchTag(clue.FoundIn.PlaceName), new SearchTag(clue.FoundBy));
        }
    }
}