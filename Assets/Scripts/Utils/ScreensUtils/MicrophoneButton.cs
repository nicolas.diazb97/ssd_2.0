﻿using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

namespace Utils.ScreensUtils
{
    [RequireComponent(typeof(StateButton))]
    public class MicrophoneButton : MonoBehaviour
    {
        [SerializeField] private Image imgMic;
        [SerializeField] private Sprite micUnmuted;
        [SerializeField] private Sprite micMuted;
        
        private StateButton _btnLocalState;

        private void Awake()
        {
            _btnLocalState = GetComponent<StateButton>();
        }

        private void OnEnable()
        {
            var micPermission = Permission.HasUserAuthorizedPermission(Permission.Microphone);
            var isAudioOn = !VivoxVoiceManager.Instance.AudioInputDevices.Muted && micPermission;
            if (!micPermission)
            {
                VivoxVoiceManager.Instance.AudioInputDevices.Muted = false;
            }
            _btnLocalState.SetState(isAudioOn);
            imgMic.sprite = isAudioOn ? micUnmuted : micMuted;
            _btnLocalState.OnStateToggled += HandleMuteToggled;
        }

        private void OnDisable()
        {
            _btnLocalState.OnStateToggled -= HandleMuteToggled;
        }

        private void HandleMuteToggled(bool value)
        {
            if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
            {
                Permission.RequestUserPermission(Permission.Microphone);
                _btnLocalState.SetState(false);
                return;
            }
            
            VivoxVoiceManager.Instance.AudioInputDevices.Muted = !value;
            imgMic.sprite = value ? micUnmuted : micMuted;
        }
    }
}