﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Utils.ScreensUtils
{
    [RequireComponent(typeof(Button))]
    public class StateButton : MonoBehaviour
    {
        [SerializeField] private UnityEvent<bool> onStateToggle;
        private Button _btnToggleState;
        private bool _currentState;

        public event Action<bool> OnStateToggled;

        private void Awake()
        {
            _btnToggleState = GetComponent<Button>();
        }

        private void OnEnable()
        {
            _btnToggleState.onClick.AddListener(OnToggleStateClick);
        }

        private void OnDisable()
        {
            _btnToggleState.onClick.RemoveListener(OnToggleStateClick);
            _currentState = false;
            onStateToggle?.Invoke(false);
        }

        public void SetState(bool newState)
        {
            onStateToggle.Invoke(_currentState = newState);
        }

        private void OnToggleStateClick()
        {
            _currentState = !_currentState;

            OnStateToggled?.Invoke(_currentState);
            onStateToggle?.Invoke(_currentState);
        }

        public void ShowTheTruth()
        {
            _btnToggleState.onClick.AddListener(() => print("Show the true"));
        }
    }
}