﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Utils.ScreensUtils
{
    [Serializable]
    public class SlideOption
    {
        [SerializeField] private string optionText;
        [SerializeField] private UnityEvent onOptionSelected;

        public string SetOptionSelected()
        {
            onOptionSelected?.Invoke();
            return optionText;
        }
    }
    
    public class OptionSlider : BaseSlider<SlideOption>
    {
        [SerializeField] private TMP_Text txtTarget;

        protected override void OnCurrentSelected(SlideOption currentItem)
        {
            txtTarget.text = currentItem.SetOptionSelected();
        }
    }
}