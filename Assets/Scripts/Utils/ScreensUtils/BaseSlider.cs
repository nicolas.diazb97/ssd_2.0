﻿using UnityEngine;
using UnityEngine.UI;

namespace Utils.ScreensUtils
{
    public abstract class BaseSlider<T> : MonoBehaviour
    {
        [SerializeField] private T[] sliderData;
        [SerializeField] private Button btnCycleRight;
        [SerializeField] private Button btnCycleLeft;

        private int _slideIndex;
        public T CurrentSlideItem => sliderData[_slideIndex];

        protected virtual void OnEnable()
        {
            btnCycleLeft.onClick.AddListener(OnCycleLeft);
            btnCycleRight.onClick.AddListener(OnCycleRight);
            
            OnCurrentSelected(sliderData[_slideIndex = 0]);
        }

        protected virtual void OnDisable()
        {
            btnCycleLeft.onClick.RemoveListener(OnCycleLeft);
            btnCycleRight.onClick.RemoveListener(OnCycleRight);
        }

        private void OnCycleRight()
        {
            if (++_slideIndex >= sliderData.Length) 
                _slideIndex = 0;
            
            OnCurrentSelected(CurrentSlideItem);
        }

        private void OnCycleLeft()
        {
            if (--_slideIndex < 0) 
                _slideIndex = sliderData.Length - 1;

            OnCurrentSelected(CurrentSlideItem);
        }

        protected virtual void OnCurrentSelected(T currentItem) { }
    }
}