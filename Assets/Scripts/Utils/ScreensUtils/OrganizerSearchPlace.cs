﻿using System;
using TMPro;
using UnityEngine;

namespace Utils.ScreensUtils
{
    [RequireComponent(typeof(FilterButton))]
    public class OrganizerSearchPlace : MonoBehaviour
    {
        [SerializeField] private TMP_Text txtPlaceName;
        [SerializeField] private Color filterEnabledFontColor = Color.black;
        private FilterButton _categoryButton;

        private Color _filterDisabledFontColor;

        private void Awake()
        {
            _categoryButton = GetComponent<FilterButton>();
            _filterDisabledFontColor = txtPlaceName.color;
        }

        private void OnEnable()
        {
            _categoryButton.OnFilteringUpdated += OnFiltered;
            OnFiltered(false);
        }

        private void OnDisable()
        {
            _categoryButton.OnFilteringUpdated -= OnFiltered;
        }

        public void SetClueCategory(string category)
        {
            txtPlaceName.text = category;
            _categoryButton.SetSearchFilter(new SearchFilter(category));
        }

        public void SetCategoryText(string text)
        {
            txtPlaceName.text = text;
            GetComponent<FilterVisual>().filtername = text;
        }

        public void OnFiltered(bool isActive)
        {
            GetComponent<FilterVisual>().Selected(true);
            CluesFilterHolder.main.OnFilterSelected(GetComponent<FilterVisual>().filtername);
            txtPlaceName.color = isActive ? filterEnabledFontColor : _filterDisabledFontColor;

        }
    }
}