﻿using UnityEngine;

namespace Utils.ScreensUtils
{
    [RequireComponent(typeof(Collider2D))]
    public class WorldSpaceClickeable : ClickeableClue
    {
        private Collider2D _collider;
        private bool _isClickEnabled;

        public override bool IsClickEnabled
        {
            get => _isClickEnabled;
            set
            {
                _collider = GetComponent<Collider2D>();
                Debug.Log($"Clickeable search place toggled = {value}");
                if (_collider != null) _collider.enabled = _isClickEnabled = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _collider = GetComponent<Collider2D>();
        }

        protected override void ToggleMapClick(bool value)
        {
            // Debug.Log("Toggle map click");
            _collider.enabled = value && IsClickEnabled;
        }
    }
}