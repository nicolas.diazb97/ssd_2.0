﻿using System;
using Data.Scripts;
using Data.ScriptsData;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Utils.ScreensUtils
{
    public class ScriptPropertiesHandler : MonoBehaviour
    {
        [SerializeField] private Image imgThumbnail; 
        [FormerlySerializedAs("scriptProperties")] [SerializeField] private FormattedProperty[] singleProperties;
        [SerializeField] private ScriptCharacter characterPrefab;
        [SerializeField] private Transform charactersParent;
        
        public const string TitleTemplate = "${Title}"; 
        public const string PlayersTemplate = "${Players}";
        public const string DurationTemplate = "${Duration}";
        public const string WordsTemplate = "${Words}";
        public const string ThemeTemplate = "${Theme}";
        public const string DifficultyTemplate = "${Difficulty}";
        public const string BriefTemplate = "${Brief}";
        public const string AuthorTemplate = "${Author}";
        public const string PriceTemplate = "${Price}";
        public const string DurationRangeTemplate = "${DRange}";

        public void Setup(GameScript script)
        {
            imgThumbnail.sprite = script.Thumbnail;

            foreach (var property in singleProperties)
            {
                property.SetProperties(script);
            }

            if (!charactersParent) return;
            
            foreach (Transform child in charactersParent)
            {
                Destroy(child.gameObject);
            }
            
            foreach (var character in script.Characters)
            {
                var characterInstance = Instantiate(characterPrefab, charactersParent);
                characterInstance.Setup(character);
            }
            
            // foreach (var filterTag in GetComponentsInChildren<FilterTag>())
            // {
            //     filterTag.Init();
            // }
        }
        
        [Serializable]
        public class FormattedProperty
        {
            [SerializeField] private string format;
            [SerializeField] private TMP_Text txtProperty;
            
            public void SetProperties(GameScript script)
            {
                var result = format.Replace(TitleTemplate, script.Title);
                result = result.Replace(PlayersTemplate, script.MinPlayers.ToString());
                result = result.Replace(DurationTemplate, script.Duration.ToString());
                result = result.Replace(WordsTemplate, script.WordCount.ToString());
                result = result.Replace(ThemeTemplate, script.Theme);
                result = result.Replace(DifficultyTemplate, script.ScriptDifficulty.ToString());
                result = result.Replace(BriefTemplate, script.Brief);
                result = result.Replace(AuthorTemplate, script.Author);
                result = result.Replace(PriceTemplate, script.PriceType.ToString());
                result = result.Replace(DurationRangeTemplate, script.DurationRange);
                txtProperty.text = result;
            }
        }
    }

    //[CustomEditor(typeof(ScriptPropertiesHandler))]
    //public class PropertiesHandlerEditor : Editor
    //{
    //    public override void OnInspectorGUI()
    //    {
    //        EditorGUILayout.HelpBox("Set the script properties by using the following templates:\n" +
    //                                $"Title = {ScriptPropertiesHandler.TitleTemplate}\n" +
    //                                $"Players = {ScriptPropertiesHandler.PlayersTemplate}\n" +
    //                                $"Duration = {ScriptPropertiesHandler.DurationTemplate}\n" +
    //                                $"Word count = {ScriptPropertiesHandler.WordsTemplate}\n" +
    //                                $"Theme = {ScriptPropertiesHandler.ThemeTemplate}\n" +
    //                                $"Difficulty = {ScriptPropertiesHandler.DifficultyTemplate}\n" +
    //                                $"Brief = {ScriptPropertiesHandler.BriefTemplate}\n" +
    //                                $"Author = {ScriptPropertiesHandler.AuthorTemplate}", MessageType.Info);
    //        DrawDefaultInspector();
    //    }
    //}
}