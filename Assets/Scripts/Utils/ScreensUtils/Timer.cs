using UnityEngine;
using System;

[Serializable]
public class Timer : MonoBehaviour
{
    private const byte ZERO = byte.MinValue;
    private const byte ONE = 1;

    public static Timer Instance { get; private set; }
    private float currentTime;

    public event Action<float> OnTimeUpdated;
    public event Action OnTimeFinish;

    private bool IsActive { get; set; }

    public void StartTimer(float duration)
    {
        Debug.Log("Timer start");
        currentTime = duration;
    }

    public void SetTimerActive(bool value)
    {
        IsActive = value;
    }
    
    public void StopTimer()
    {
        IsActive = false;
        OnFinish();
    }

    public void PauseTimer()
    {
        IsActive = false;
    }

    public void OnFinish() => OnTimeFinish?.Invoke();

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    // private void Start()
    // {
    //     IsActive = true;
    // }

    private void Update()
    {
        if (!IsActive)  return; 

        if (currentTime <= ZERO)
        {
            currentTime = ZERO;
            StopTimer();    
        }

        currentTime -= ONE * Time.deltaTime;
        OnTimeUpdated?.Invoke(currentTime);
    }
}
