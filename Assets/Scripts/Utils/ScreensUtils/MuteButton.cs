﻿using UnityEngine;
using UnityEngine.UI;

namespace Utils.ScreensUtils
{
    [RequireComponent(typeof(StateButton))]
    public class MuteButton : MonoBehaviour
    {
        [SerializeField] private Sprite audioOnSprite;
        [SerializeField] private Sprite audioOffSprite;
        [SerializeField] private Image audioImage;
        [SerializeField] private AudioSource musicSource;

        private StateButton _toggleButton;

        private void Awake()
        {
            _toggleButton = GetComponent<StateButton>();
        }

        private void OnEnable()
        {
            _toggleButton.OnStateToggled += HandleToggleMute;
            SetVisuals(!musicSource.mute);
        }

        private void OnDisable()
        {
            _toggleButton.OnStateToggled -= HandleToggleMute;
        }

        private void HandleToggleMute(bool value)
        {
            musicSource.mute = !value;
            SetVisuals(value);
        }

        private void SetVisuals(bool value)
        {
            audioImage.sprite = value ? audioOnSprite : audioOffSprite;
        }
    }
}