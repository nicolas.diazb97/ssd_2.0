﻿using System.ComponentModel;
using System.Linq;
using Data.Scripts;
using Managers;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;
using VivoxUnity;

namespace Utils.ScreensUtils
{
    [RequireComponent(typeof(ClickeableClue), typeof(SearchPlace))]
    public class CharacterReadyHandler : MonoBehaviour
    {
        [FormerlySerializedAs("imgPlayerPfp")] [SerializeField]
        private Image imgCharacterThumbnail;

        [FormerlySerializedAs("txtPlayerName")] [SerializeField]
        private TMP_Text txtCharacterName;

        [SerializeField] private UnityEvent<bool> onStatusUpdated;
        [SerializeField] private Animator speechAnimator;
        [SerializeField] private GameObject lockIcon;
        [SerializeField] private UnityEvent onPlayerDisconnected;
        [SerializeField] private UnityEvent onPlayerReconnected;
        [SerializeField] private GameObject DisconnectIcon;

        private string _characterName;
        private ClickeableClue _clickeable;
        private SearchPlace _searchableCharacter;
        private GameScript.Character _assignedCharacter;
        private static readonly int BoolVoice = Animator.StringToHash("BoolVoice");
        private IParticipant _vcParticipant;

        private bool AssignedIsLocal => _assignedCharacter.name == DataManager.main.CharacterAssigned.name;
        private string _thisPlayer;


        private void Awake()
        {
            _clickeable = GetComponent<ClickeableClue>();
            _searchableCharacter = GetComponent<SearchPlace>();
        }

        public void Setup(GameScript.Character thisCharacter)
        {
            _assignedCharacter = thisCharacter;
            _thisPlayer = DataManager.main.GetPlayerAssignedToCharacter(_assignedCharacter);

            imgCharacterThumbnail.sprite = thisCharacter.picture;
            txtCharacterName.text = _characterName = thisCharacter.name;
            _searchableCharacter.SetInfo(thisCharacter.name, thisCharacter.picture, thisCharacter.setupsPerRound, thisCharacter);
            DataManager.main.charactersOnScript.Add(thisCharacter.name);

            if (AssignedIsLocal) _clickeable.SetClickNotDrivenByCamera();
            DisconnectIcon.gameObject.SetActive(false);
            Debug.Log("this player " +_thisPlayer);
            // se puede usar punmanager.instance.reconectroom bool para determinar la asignacion despues
            if(VivoxVoiceManager.Instance.LoginState != LoginState.LoggedIn ) return;
            foreach (var participant in VivoxVoiceManager.Instance.CurrentJoinedChannel.Participants)
           {
               if (participant.Account.DisplayName == _thisPlayer)
               {
                   _vcParticipant = participant;
                   participant.PropertyChanged += HandleVoiceParticipantPropertyChanged;
               }
           }
            DisconnectIcon.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            PunManager.Instance.OnPlayerReadyOnInterphase += HandlePlayerReady;
            PunManager.Instance.OnPlayerLeft += HandlePlayerLeft;
            PunManager.Instance.OnPlayerJoined += HandlePlayerJoined;
            GameManager.main.OnNewRoundStarted += HandleRoundStarted;
            VivoxVoiceManager.Instance.OnParticipantAddedEvent += HandleParticipantJoined;
            VivoxVoiceManager.Instance.OnParticipantRemovedEvent += HandleParticipantLeft;
        }

        private void HandlePlayerJoined(Player playerJoined)
        {
          
            if (playerJoined.NickName != _thisPlayer) return;
            
            onPlayerReconnected?.Invoke();
            onPlayerReconnected?.Invoke();
        }

        private void OnDisable()
        {
            PunManager.Instance.OnPlayerReadyOnInterphase -= HandlePlayerReady;
            GameManager.main.OnNewRoundStarted -= HandleRoundStarted;
            PunManager.Instance.OnPlayerLeft -= HandlePlayerLeft;
            VivoxVoiceManager.Instance.OnParticipantAddedEvent -= HandleParticipantJoined;
            VivoxVoiceManager.Instance.OnParticipantRemovedEvent -= HandleParticipantLeft;

            onStatusUpdated.Invoke(false);
        }

        private void HandlePlayerLeft(Player leftPlayer)
        {
            if (leftPlayer.NickName != _thisPlayer) return;
            onPlayerDisconnected?.Invoke();
        }

        private void HandleParticipantJoined(string username, ChannelId channel, IParticipant participant1)
        {
            speechAnimator.SetBool(BoolVoice, false);
            if (participant1.Account.Name == _vcParticipant.Account.Name)
            {
                lockIcon.SetActive(channel.Name.Contains("_"));
                speechAnimator.SetBool(BoolVoice, false);
            }
            
            if (!channel.Name.Contains("_"))
            {
                lockIcon.SetActive(false);
            }

            Debug.Log("se ejecuta login for reconnect");
            var thisPlayer = DataManager.main.GetPlayerAssignedToCharacter(_assignedCharacter);
            foreach (var participant in VivoxVoiceManager.Instance.CurrentJoinedChannel.Participants)
            {
                if (participant.Account.DisplayName == thisPlayer)
                {
                    Debug.Log("este jugador " + participant.Account.DisplayName + "no es this player " + thisPlayer);
                    _vcParticipant = participant;
                    participant.PropertyChanged += HandleVoiceParticipantPropertyChanged;
                }
            }
        }

        private void HandleParticipantLeft(string username, ChannelId channel, IParticipant participant)
        {
            if (participant.Account.Name == _vcParticipant.Account.Name)
            {
                speechAnimator.SetBool(BoolVoice, false);
                lockIcon.SetActive(false);
            }
        }

        private void HandleRoundStarted(RoundConfig roundConfig)
        {
            var roundName = roundConfig.RoundName;
            Debug.Log(" se llega hasta round name " + roundName + "con valor assigned local " + AssignedIsLocal);
            onStatusUpdated?.Invoke(false);
            _clickeable.IsClickEnabled = !AssignedIsLocal && roundConfig.IsSearchRound;
        }

        private void HandleVoiceParticipantPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "SpeechDetected":
                    HandleSpeechDetected(_vcParticipant.SpeechDetected);
                    break;
            }
        }

        private void HandleSpeechDetected(bool isSpeaking)
        {
            speechAnimator.SetBool(BoolVoice, isSpeaking);
        }

        private void HandlePlayerReady(string characterName, bool isReady)
        {
            if (characterName == _characterName)
            {
                onStatusUpdated.Invoke(isReady);
            }
        }
    }
}