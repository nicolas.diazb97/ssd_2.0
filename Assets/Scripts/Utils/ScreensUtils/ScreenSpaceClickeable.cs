﻿using UnityEngine.EventSystems;

namespace Utils.ScreensUtils
{
    public class ScreenSpaceClickeable : ClickeableClue, IPointerClickHandler
    {
        private bool _isMapClickEnabled;

        protected override void ToggleMapClick(bool value)
        {
            _isMapClickEnabled = value;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_isMapClickEnabled && IsClickEnabled)
            {
                SearchPlace.EnableUI();
            }
        }
    }
}