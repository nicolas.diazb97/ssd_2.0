﻿using System;
using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Screens.Utils
{
    [RequireComponent(typeof(Button))]
    public class LeavePrivateRoomButton : MonoBehaviour
    {
        [SerializeField] private string leaveMsg;
        private Button _btnLeave;

        private void Awake()
        {
            _btnLeave = GetComponent<Button>();
        }

        private void OnEnable()
        {
            _btnLeave.onClick.AddListener(OnClickLeave);
        }

        private void OnDisable()
        {
            _btnLeave.onClick.RemoveListener(OnClickLeave);
        }

        public async void OnClickLeave()
        {
            ScreenManager.main.ResetAllScreens();
            
            var confirmPopup = ScreenManager.main.OpenScreen<ConfirmPopup>(false);
            await confirmPopup.WaitUntilConfirmedAsync(leaveMsg);
            
            PrivateRoomManager.main.LeavePrivateRoom();
        }
    }
}