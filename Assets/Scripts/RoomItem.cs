﻿using Managers;
using Photon.Realtime;
using Screens;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils.ScreensUtils;

[RequireComponent(typeof(ScriptPropertiesHandler))]
public class RoomItem : MonoBehaviour
{
    [SerializeField] private Button btnJoinRoom;
    
    [Header("Room Info")]
    [SerializeField] private TMP_Text txtRoomName;
    [SerializeField] private TMP_Text txtRoomMaster;
    [SerializeField] private TMP_Text txtScriptTitle;
    private RoomInfo _roomInfo;
    private ScriptPropertiesHandler _propertiesHandler;

    private void Awake()
    {
        _propertiesHandler = GetComponent<ScriptPropertiesHandler>();
    }

    private void OnEnable()
    {
        btnJoinRoom.onClick.AddListener(OnClickJoin);
    }

    private void OnDisable()
    {
        btnJoinRoom.onClick.RemoveListener(OnClickJoin);
    }

    public void Setup(RoomInfo roomInfo)
    {
        _roomInfo = roomInfo;
        Debug.Log("contraseña:<<<"+ roomInfo.CustomProperties["Password"]);
        var roomProperties = roomInfo.CustomProperties;
        var scriptId = roomProperties[PunManager.ScriptKey].ToString();
        var roomScript = DataManager.main.GetScriptById(int.Parse(scriptId));
        _propertiesHandler.Setup(roomScript);
        txtRoomName.text = (_roomInfo = roomInfo).Name;

        txtRoomMaster.text = roomProperties[PunManager.CreatorKey].ToString();
        txtScriptTitle.text = $"{_roomInfo.PlayerCount} / {_roomInfo.MaxPlayers} {roomScript.Title}";
    }

    private void OnClickJoin()
    {
        var roomPasswd = _roomInfo.CustomProperties["Password"].ToString();
                    
       if (roomPasswd.Length > 0)
        {
            var passwordPopup = ScreenManager.main.OpenScreen<PasswordPopup>(false);
            passwordPopup.SetupInfo(roomPasswd, HandleJoinRoom);
        }
        else
        {
            HandleJoinRoom();
        }
       
      /*if (roomPasswd !=null && roomPasswd.ToString().Length >= 3 )
      {
          var passwordPopup = ScreenManager.main.OpenScreen<PasswordPopup>(false);
          passwordPopup.SetupInfo(roomPasswd.ToString(), HandleJoinRoom);
      }
      else
      {
          HandleJoinRoom();
      } 
      */
    }

    private void HandleJoinRoom()
    {
        PunManager.Instance.JoinRoom(_roomInfo.Name);
        ScreenManager.main.CloseScreen();
    }
}