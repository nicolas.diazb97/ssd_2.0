using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class SearchFilter
{
    [SerializeField] private SearchTag.Type filterType;
    [SerializeField] private string filterValue;
    [SerializeField] private float minFloatValue;
    [SerializeField] private float maxFloatValue;
    [SerializeField] private string numericUnit = "hrs";

    // BEGINING OF DEV AREA
    //[SerializeField] private string CategoryID = "# of players ";


    // END OF DEV AREA

    public SearchFilter(string filterValue)
    {
        filterType = SearchTag.Type.Alphabetic;
        this.filterValue = filterValue;
    }

    public SearchFilter(float minValue, float maxValue)
    {
        filterType = SearchTag.Type.Ranged;
        minFloatValue = minValue;
        maxFloatValue = maxValue;
    }

    public SearchTag.Type FilterType => filterType;

    public string FilterValue => filterValue;

    public float MinFloatValue => minFloatValue;

    public float MaxFloatValue => maxFloatValue;

    public override string ToString()
    {
        return filterType switch
        {
            SearchTag.Type.Ranged => minFloatValue > 0
                ? $"{minFloatValue} - {maxFloatValue} {numericUnit}"
                : $"< {maxFloatValue} {numericUnit}",
            SearchTag.Type.Alphabetic => filterValue,
            _ => throw new ArgumentOutOfRangeException()
        };
    }
}

public class FilterButton : MonoBehaviour
{
    [SerializeField] private SearchFilter filter;
    [SerializeField] private Button btnEnableFilter;
    [SerializeField] private Image imgIsActive;
    [SerializeField] private Color filterEnabledColor = Color.green;
    [SerializeField] private TMP_Text txtFilterText;

    // BEGINING OF DEV AREA
    [SerializeField] private string filterCategory = "# of players ";


    // END OF DEV AREA



    private Color _filterDisabledColor;
    private bool _isFilterActive;
    private FilterManager _filterManagerRef;

    public event Action<bool> OnFilteringUpdated; 

    private void Awake()
    {
        var filterManager = GetComponentInParent<FilterManager>();
        if (!filterManager)
        {
            Debug.LogError($"There must exist a parent filter manager for {name}.");
            return;
        }

        _filterDisabledColor = imgIsActive.color;
        _filterManagerRef = filterManager;
    }

    private void OnEnable()
    {
        btnEnableFilter.onClick.AddListener(ToggleFilterEnabled);
        _filterManagerRef.OnActiveFiltersUpdated += UpdateFilterVisuals;
    }

    private void OnDisable()
    {
        btnEnableFilter.onClick.RemoveListener(ToggleFilterEnabled);
        _filterManagerRef.OnActiveFiltersUpdated -= UpdateFilterVisuals;
    }

    private void UpdateFilterVisuals(SearchFilter[] activeFilters)
    {
        _isFilterActive = activeFilters.Any(searchFilter => searchFilter == filter);
        imgIsActive.color = _isFilterActive ? filterEnabledColor : _filterDisabledColor;
        OnFilteringUpdated?.Invoke(_isFilterActive);
    }

    public void SetSearchFilter(SearchFilter searchFilter)
    {
        filter = searchFilter;
        SetFilterText();
    }

    private void SetFilterText()
    {
        txtFilterText.text = filter.ToString();
    }

    private void OnValidate()
    {
        if (!txtFilterText) return;
        SetFilterText();
    }

    public void ToggleFilterEnabled()
    {
        if (!_isFilterActive)
        {
            _filterManagerRef.AddActiveFilter(filter,filterCategory); //Mod
        }
        else
        {
            _filterManagerRef.RemoveActiveFilter(filter);
        }
    }
}
