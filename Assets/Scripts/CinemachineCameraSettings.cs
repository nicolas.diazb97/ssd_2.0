using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemachineCameraSettings : MonoBehaviour
{
    public bool allowVerticalMovement = false;
    private void OnEnable()
    {
        //CameraManager.main.virtualCamera.Follow = gameObject.transform;
        CameraManager.main.confiner.m_BoundingShape2D = GetComponent<Collider2D>();
        if (allowVerticalMovement)
        {
            CameraManager.main.virtualCamera.GetCinemachineComponent<CinemachineFramingTransposer>().m_DeadZoneHeight = 0;
        }
        else
        {
            CameraManager.main.virtualCamera.GetCinemachineComponent<CinemachineFramingTransposer>().m_DeadZoneHeight = 2;
        }
    }
}
