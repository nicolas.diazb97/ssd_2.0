using Data.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Data.Scripts.GameScript;

[System.Serializable]
public class ScoringResult
{
    [SerializeField]
#if UNITY_EDITOR
    [ListToPopUp(typeof(GameScript), "myList")]
#endif
    public string characterName;
    [SerializeField]
    public int score;

}

