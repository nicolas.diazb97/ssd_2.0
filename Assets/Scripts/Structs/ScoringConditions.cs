using Data.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Data.Scripts.GameScript;
using UnityEngine.UI;
using MyBox;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public enum ConditionType
{
    MajorityVotesCorrect,
    MajorityVotesIncorrect,
    MostVotedCharacter
}

[System.Serializable]
public class ScoringConditions
{
    [SerializeField]
    public ConditionType conditionType;
    [ConditionalField(nameof(conditionType), false, ConditionType.MajorityVotesCorrect)] public int minVotesForCorrect;

    [ConditionalField(nameof(conditionType), false, ConditionType.MajorityVotesIncorrect)] public float minVotesForIncorrect;

    [ConditionalField(nameof(conditionType), false, ConditionType.MostVotedCharacter)] public string mostVotedCharacter;

    [SerializeField]
    public ScoringResult[] results;



    //[SerializeField]
    //[ListToPopUp(typeof(GameScript), "myList")]
    //public string characterName;
    //[SerializeField]
    //public int score;

}

#if UNITY_EDITOR
public static class ExtensionMethods
{
    public static string AsStringValue(this SerializedProperty property)
    {
        switch (property.propertyType)
        {
            case SerializedPropertyType.String:
                return property.stringValue;
            case SerializedPropertyType.Character:
            case SerializedPropertyType.Integer:
                if (property.type == "char") return System.Convert.ToChar(property.intValue).ToString();
                return property.intValue.ToString();
            case SerializedPropertyType.ObjectReference:
                return property.objectReferenceValue != null ? property.objectReferenceValue.ToString() : "null";
            case SerializedPropertyType.Boolean:
                return property.boolValue.ToString();
            case SerializedPropertyType.Enum:
                return property.enumNames[property.enumValueIndex];
            default:
                return string.Empty;
        }
    }
}
#endif
