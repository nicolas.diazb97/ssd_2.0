using System.Collections.Generic;

public class LoginData
{
    public string userId;
    public string userName;
    public string userEmail;
    public string userPic;

    public LoginData(string userId, string userName, string userEmail, string userPic)
    {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPic = userPic;
    }
}
