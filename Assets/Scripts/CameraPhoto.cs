using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class CameraPhoto : MonoBehaviour
{

    int currentCamIndex = 0;
    WebCamTexture tex;
    public RawImage display;
    public Text startStopText;
    public CanvasScaler canvasS;
    public RealtimeDatabaseManager dataBase;
    private string andPath;
    private void Start()
    {
        //RequestPermission();
    }
    public void SwapCam_Clicked()
    {
        if (WebCamTexture.devices.Length > 0)
        {
            currentCamIndex += 1;
            currentCamIndex %= WebCamTexture.devices.Length;
            // if tex is not null:
            // stop the web cam
            // start the web cam

            if (tex != null)
            {
                StopWebCam();
                StartStopCam_Clicked();
            }

        }
    }

    public void RequestPermission()
    {
        if (tex != null) // Stop the camera
        {
            StopWebCam();
            //startStopText.text = "Start Camera";
        }
        else // Start the camera
        {
            WebCamDevice device = WebCamTexture.devices[currentCamIndex];
            tex = new WebCamTexture(device.name);
        }
    }

    public void StartStopCam_Clicked()
    {
        if (tex != null) // Stop the camera
        {
            StopWebCam();
            //startStopText.text = "Start Camera";
        }
        else // Start the camera
        {
            WebCamDevice device = WebCamTexture.devices[currentCamIndex];
            tex = new WebCamTexture(device.name);

            tex.requestedHeight = 1920;
            tex.requestedWidth = 1080;
            startStopText.text = "Width:" + tex.width + " Height: " + tex.height;


            display.texture = tex;

            //RotateFrame(270);

            if (currentCamIndex == 0)
            {
                RotateFrame(270);

            }
            else
            {
                RotateFrame(90);

            }

            tex.Play();

        }
    }

    private void StopWebCam()
    {
        display.texture = null;
        tex.Stop();
        tex = null;
    }

    public void RotateFrame(int angle)
    {
        Debug.Log(display.transform.rotation.eulerAngles);
        display.transform.rotation = Quaternion.Euler(0, 0, angle);
    }
    public void RotateFrameOp()
    {
        Debug.Log(display.transform.rotation.eulerAngles);
        startStopText.text = ("Angle: " + display.transform.rotation.eulerAngles.z + 90).ToString();
        display.transform.rotation = Quaternion.Euler(0, 0, display.transform.rotation.eulerAngles.z + 90);

    }
    public IEnumerator TakeTempPhotoDep()
    {
        yield return new WaitForEndOfFrame();
        //  tempPhoto.gameObject.SetActive(true);

        Debug.Log("SW: " + Screen.width + " SH: " + canvasS.referenceResolution.y);
        float picSize = (1080 * Screen.width) / canvasS.referenceResolution.y;
        Debug.Log("picsize: " + picSize);
        float xOffset = (Screen.width - picSize) / 2; //+ tempPhoto.rectTransform.rect.x;
        float yOffset = (Screen.height - picSize) / 2; //+ tempPhoto.rectTransform.rect.y;

        Texture2D ss = new Texture2D((int)picSize, (int)picSize, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(xOffset, yOffset, picSize, picSize), 0, 0);
        ss.Apply();
        // tempPhoto.texture = ss;

        NativeGallery.Permission permission = NativeGallery.SaveImageToGallery(ss, "GalleryTest", "Image.png", (success, path) =>/* startStopText.text = ("Media save result: " + success + " " + path)*/ andPath = path);
        Debug.Log("Permission result: " + permission);

        // dataBase.UpdateProfilePic(andPath);
        // To avoid memory leaks
        Destroy(ss);
    }

    public void SelectPic()
    {
        // Debug.Log("CW: " + canvasS.referenceResolution.x + " CH: " + canvasS.referenceResolution.y);
        StartCoroutine(TakeTempPhotoDep());
    }

    public void DiscardPhoto()
    {
        tex.Play();
    }

    public void TakeTempPhoto()
    {
        tex.Pause();
    }

}