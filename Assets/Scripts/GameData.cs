namespace SSD.GameData 
{
    public enum Gender
    {
        Female,
        Male
    }


    public enum Difficulty
    {
        Easy = 0,
        Medium = 1,
        Hard = 2
    }

    public enum Price
    {
        Free, Paid
    }

    public enum GameScriptType
    {
        Vanilla = 0, Seasonal = 1
    }
}
