using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ConsoleItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI myText;
    [SerializeField] private TextMeshProUGUI detailsText;

    private void Awake()
    {
        detailsText = GameObject.FindGameObjectWithTag("DetailsText").GetComponent<TextMeshProUGUI>();
    }

    public void SetDetailsText()
    {
        detailsText.text = myText.text;
    }
}
