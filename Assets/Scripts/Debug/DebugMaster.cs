using UnityEngine;
using TMPro;
using System;

public class DebugMaster : MonoBehaviour
{
    public static DebugMaster debugInstance;

    [Header("Config")]
    [SerializeField] GameObject debugButton;
    [SerializeField] GameObject testUserButton;

    [Header("Console")]
    [SerializeField] private Transform consoleContent;
    [SerializeField] private GameObject consoleItem;
    [SerializeField] private GameObject unityConsole;
    [SerializeField] private GameObject appleLogin;

    private void Awake()
    {
        if (debugInstance == null)
        {
            DontDestroyOnLoad(gameObject);
            debugInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public void AddMesaggeToConsole(string message)
    {
        var currentDebug = Instantiate(consoleItem, consoleContent);
        currentDebug.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text =
            $"[{FixTimeFormat(DateTime.Now.Hour.ToString())}:" +
            $"{FixTimeFormat(DateTime.Now.Minute.ToString())}:" +
            $"{FixTimeFormat(DateTime.Now.Second.ToString())}] {message}";

    }
    private string FixTimeFormat(string time)
    {
        if (time.Length > 1)
            return time;
        else
            return $"0{time}";
    }

    public void FixOrder()
    {
        transform.SetAsLastSibling();
    }

    public void TurnDebugButton()
    {
        debugButton.SetActive(!debugButton.activeSelf);

    }
    public void TurnUnityConsole()
    {
        unityConsole.SetActive(!unityConsole.activeSelf);
        Debug.Log("UnityConsole");

    }
    public void TurnTestUserButton()
    {
        testUserButton.SetActive(!testUserButton.activeSelf);
    }
    public void TurnAppleButton()
    {
        appleLogin.SetActive(!appleLogin.activeSelf);
    }
    public void TurnLogin()
    {
        ScreenManager.main.TurnScreen(SerializableScreen.Type.Login);
    }

    public void TurnMenu()
    {
        ScreenManager.main.TurnScreen(SerializableScreen.Type.Menu);
    }
    public void TurnHome()
    {
        ScreenManager.main.TurnScreen(SerializableScreen.Type.Home);
    }

}
