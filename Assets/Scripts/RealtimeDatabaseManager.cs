using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Storage;
using UnityEngine.Events;
using System;
using Managers;
using Photon.Pun;
using Screens;
using UnityEngine.Android;
using UnityEngine.Networking;
using VivoxUnity;
using System.Linq;


public class RealtimeDatabaseManager : MonoBehaviour
{
    //Ref to database
    public DatabaseReference dataRef;

    public FirebaseUser User;
    [HideInInspector] public RegEventFirebase OnUserVerificationCompleted;
    [HideInInspector] public UnityEvent OnFirebaseDataCheckingCompleted;
    public static RealtimeDatabaseManager rtdbmReference;
    [SerializeField] ScriptItemTable scriptItemTable;
    [SerializeField] private Util util;

    //2.0: Variables de los datos del usuario, propiedades accesibles desde la instancia de la clase.
    private User currentUser = new User();
    public string UserId { get { return FormattedId(currentUser.Id); } }
    public string UserName { get { return currentUser.Name; } }
    public string UserEmail { get { return currentUser.Email; } }
    public string UserPhotoUrl { get { return currentUser.PhotoUrl; } }
    public string UserScripts { get { return currentUser.Scripts; } }
    public string UserOnline { get { return currentUser.Online; } }
    public string UserRoomId { get { return currentUser.RoomId; } }
    public Sprite UserSpritePhoto { get { return spritePhoto; } }
    public uint UserPlayedScripts => (uint)currentUser.playedScripts.Count;

    //Aux user variables
    private List<string> UserScriptList = new List<string>();
    private Sprite spritePhoto;
    private Texture2D texturePhoto;

    [HideInInspector] public bool isInSession;

    // private readonly PermissionCallbacks _permissionCallbacks = new PermissionCallbacks();

    private void Awake()
    {
        if (rtdbmReference == null)
        {
            rtdbmReference = this;
        }
        else { Destroy(gameObject); }

        // _permissionCallbacks.PermissionGranted += HandlePermissionGranted;
    }

    private void OnDestroy()
    {
        // _permissionCallbacks.PermissionGranted -= HandlePermissionGranted;
    }

    void Start()
    {
        dataRef = FirebaseDatabase.DefaultInstance.RootReference;
        isInSession = false; 
        Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        UnityEngine.Debug.Log("Received a new message from: " + e.Message.From);
    }

    //2.1: Este método recibe los datos del usuario para hacer el debido tratamiento desde la base de datos de firebase.
    public void GetUserDataInfo(LoginData loginData)
    {
        currentUser.Id = loginData.userId;
        currentUser.Name = FormattedName(loginData.userName);
        currentUser.Email = loginData.userEmail;
        currentUser.PhotoUrl = loginData.userPic;
        //dataRef.Child("User").Child(currentUser.Id).Child("playedScripts").GetValueAsync().ContinueWith
        //(
        //    task => 
        //    {
        //        if (task.IsFaulted)
        //        {
        //            print(task.Result + " Cant get user scripts");
        //        }
        //        else if (task.IsCompleted) 
        //        {
        //            string json = task.Result.GetValue(false).ToString();
        //            currentUser.playedScripts = JsonUtility.FromJson<List<string>>(json);
        //            print("user scripts getted");
        //        }

        //    }    
        //);

        DebugMaster.debugInstance.AddMesaggeToConsole("Social network information get successfully");
        Debug.Log("Social network information get successfully");

        StartCoroutine(ValidateUser(currentUser.Id));
    }
    //2.2: Esta función usa el campo Id del usuario para validad si ya existe en la base de datos.
    private IEnumerator ValidateUser(string userId)
    {
        //Get the currently logged in user data
        var DBTask = dataRef.Child("User").Child(userId).GetValueAsync();

        DebugMaster.debugInstance.AddMesaggeToConsole("Validating user existence");
        Debug.Log("Validating user existence");

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            DebugMaster.debugInstance.AddMesaggeToConsole(message: $"Failed to register task with {DBTask.Exception}");
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else if (DBTask.Result.Value == null)
        {
            //2.2.1: En caso de que no exista, procede a crear el nuevo registro del usuario.
            DebugMaster.debugInstance.AddMesaggeToConsole($"User '{UserName}' does not exist");
            Debug.Log($"User '{UserName}' does not exist");
            OnUserVerificationCompleted.Invoke(false);
            //1: Primero descarga la foto de perfil ya que es el proceso mas demorado. 
            yield return StartCoroutine(DownloadPhotoFromSN(UserPhotoUrl));
            //2: Luego se dirige a la pantalla de Home del juego, en este momento aun no se ha creado el usuario en base de datos
            //  ya que el objetivo es ir a esta pantalla lo mas rápido posible con la información mínima necesaria.
            BeginSession();
            //3: La foto de perfil se sube a Storage de firebase.
            yield return StartCoroutine(UploadPhotoToStorage(texturePhoto, Guid.NewGuid().ToString()));
            //4: Por último se crea el usuario en Realtime Database de firebase.
            CreateNewUserOnFirebase(currentUser.Id, currentUser.Name, currentUser.Email, currentUser.PhotoUrl);

            PunManager.Instance.SetPlayerCustomProperties(UserName, UserId, UserPhotoUrl);
        }
        else
        {
            //2.2.2: Si el usuario ya existe, carga la información 
            DataSnapshot snapshot = DBTask.Result;
            DebugMaster.debugInstance.AddMesaggeToConsole($"User '{UserName}' exists");
            Debug.Log($"User '{UserName}' exists");

            //1: Carga de los campos de la tabla del usuario
            currentUser.Id = snapshot.Child("Id").Value.ToString();
            currentUser.Name = snapshot.Child("Name").Value.ToString();
            currentUser.Email = snapshot.Child("Email").Value.ToString();
            currentUser.PhotoUrl = snapshot.Child("PhotoUrl").Value.ToString();
            currentUser.Scripts = snapshot.Child("Scripts").Value.ToString();
            currentUser.Online = snapshot.Child("Online").Value.ToString();
            currentUser.RoomId = snapshot.Child("RoomId").Value.ToString();
            DataSnapshot playedScripts = snapshot.Child("playedScripts");

            if (playedScripts.Value != null)
            {
                List<object> list = playedScripts.Value as List<object>;
                currentUser.playedScripts = list.Cast<string>().ToList();
            }
            else 
            {
                currentUser.playedScripts = new List<string>();
            }


            PunManager.Instance.SetPlayerCustomProperties(UserName, UserId, UserPhotoUrl);

            //Verifica si el usuario ya esta logeado (en desarrollo)
            if (currentUser.Online.Equals("1"))
            {
                DebugMaster.debugInstance.AddMesaggeToConsole("User already logged in");
                Debug.Log("User already logged in");
                util.ActiveButton("Facebook");
                util.ActiveButton("Google");
                UpdateOnlineStatus(false);
            }
            else if (currentUser.Online.Equals("0"))
            {

                DebugMaster.debugInstance.AddMesaggeToConsole("Data loaded successfully");
                Debug.Log("Data loaded successfully");

                //2: Descarga la foto desde Storage
                yield return StartCoroutine(DownloadPhotoFromStorage(currentUser.PhotoUrl));

                //3: Inicia sesion a penas descarga la foto y el nombre de usuario
                BeginSession();
                DebugMaster.debugInstance.AddMesaggeToConsole($"Purchased scripts string: {UserScripts}");
                Debug.Log($"Purchased scripts string: {UserScripts}");
                CreateScriptList(UserScripts);

            }

            OnUserVerificationCompleted.Invoke(true);
        }
        OnFirebaseDataCheckingCompleted.Invoke();
    }
    //3: Función encargada de registrar un nuevo usuario en firebase, solo con los datos básicos.
    public void CreateNewUserOnFirebase(string id, string name, string email, string picUrl)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole($"Creating new user '{name}'");
        Debug.Log($"Creating new user '{name}'");

        currentUser.Id = id;
        currentUser.Name = name;
        currentUser.Email = email;
        currentUser.PhotoUrl = picUrl;
        currentUser.Scripts = "";
        currentUser.Online = "1";
        currentUser.RoomId = "0";

        string json = JsonUtility.ToJson(currentUser);
        dataRef.Child("User").Child(currentUser.Id).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                DebugMaster.debugInstance.AddMesaggeToConsole($"User '{UserName}' created successfully");
                Debug.Log($"User '{UserName}' created successfully");
            }
            else
            {
                DebugMaster.debugInstance.AddMesaggeToConsole($"Failed create new user '{UserName}'");
                Debug.Log($"Failed create new user '{UserName}'");
            }
        });
    }
    //4: Función auxiliar que puede cargar todos los datos del usuario desde firebase.
    private IEnumerator LoadFullUserInfo(string userId)
    {
        //Get the currently logged in user data
        var DBTask = dataRef.Child("User").Child(userId).GetValueAsync();

        DebugMaster.debugInstance.AddMesaggeToConsole("Load Full User Info");
        Debug.Log("Load Full User Info");

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            DebugMaster.debugInstance.AddMesaggeToConsole(message: $"Failed to register task with {DBTask.Exception}");
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else if (DBTask.Result.Value == null)
        {
            DebugMaster.debugInstance.AddMesaggeToConsole($"User '{UserName}' does not exist");
            Debug.Log($"User '{UserName}' does not exist");
            OnUserVerificationCompleted.Invoke(false);
        }
        else
        {
            //PROCEEDS TO START SESSION
            DataSnapshot snapshot = DBTask.Result;

            //1: Load data from firebase of registered user
            currentUser.Id = snapshot.Child("Id").Value.ToString();
            currentUser.Name = snapshot.Child("Name").Value.ToString();
            currentUser.Email = snapshot.Child("Email").Value.ToString();
            currentUser.PhotoUrl = snapshot.Child("PhotoUrl").Value.ToString();
            currentUser.Scripts = snapshot.Child("Scripts").Value.ToString();
            currentUser.Online = snapshot.Child("Online").Value.ToString();
            currentUser.RoomId = snapshot.Child("RoomId").Value.ToString();

            DebugMaster.debugInstance.AddMesaggeToConsole("Data loaded successfully");
            Debug.Log("Data loaded successfully");

            OnUserVerificationCompleted.Invoke(true);
        }
        OnFirebaseDataCheckingCompleted.Invoke();
    }
    //5: Funciones para actualizar cada campo de los datos del usuario.
    public void UpdateName(string newName)
    {
        currentUser.Name = newName;
        UpdateFullUserDataOnFirebase(currentUser.Id, currentUser.Name, currentUser.Email, currentUser.PhotoUrl, currentUser.Scripts, currentUser.Online, currentUser.RoomId);
    }
    public void UpdatePicUrl(string newPicUrl)
    {
        currentUser.PhotoUrl = newPicUrl;
        UpdateFullUserDataOnFirebase(currentUser.Id, currentUser.Name, currentUser.Email, currentUser.PhotoUrl, currentUser.Scripts, currentUser.Online, currentUser.RoomId);
    }
    public void UpdateScripts(string newScripts)
    {
        currentUser.Scripts += $"{newScripts};";
        UpdateFullUserDataOnFirebase(currentUser.Id, currentUser.Name, currentUser.Email, currentUser.PhotoUrl, currentUser.Scripts, currentUser.Online, currentUser.RoomId);
        scriptItemTable.PurchasedState();
        CreateScriptList(UserScripts);
    }
    public void UpdateOnlineStatus(bool newOnlineStatus)
    {
        if (newOnlineStatus) currentUser.Online = "1";
        else currentUser.Online = "0";

        UpdateFullUserDataOnFirebase(currentUser.Id, currentUser.Name, currentUser.Email, currentUser.PhotoUrl, currentUser.Scripts, currentUser.Online, currentUser.RoomId);
    }
    public void UpdateRoomId(string newRoomId)
    {
        currentUser.RoomId = newRoomId;
        UpdateFullUserDataOnFirebase(currentUser.Id, currentUser.Name, currentUser.Email, currentUser.PhotoUrl, currentUser.Scripts, currentUser.Online, currentUser.RoomId);
    }
    [SerializeField] private int scriptTest;
    [ContextMenu("Set new script")]
    public void SaveTestScripts()
    {
        SavePlayedScript("Test" + scriptTest);
    }
    public void SavePlayedScript(string playedScript)
    {
        if (currentUser.playedScripts.Contains(playedScript)) { return; }

        currentUser.playedScripts.Add(playedScript);
        dataRef.Child("User").Child(currentUser.Id).Child("playedScripts").SetValueAsync(currentUser.playedScripts);
    }

    //6: Función que hace la actualización de datos del usuario.
    public void UpdateFullUserDataOnFirebase(string id, string name = "", string email = "", string picUrl = "", string scripts = "", string online = "1", string roomId = "0")
    {
        User user = new User();

        user.Id = id;
        user.Name = name;
        user.Email = email;
        user.PhotoUrl = picUrl;
        user.Scripts = scripts;
        user.Online = online;
        user.RoomId = roomId;
        user.playedScripts = currentUser.playedScripts;

        string json = JsonUtility.ToJson(user);

        dataRef.Child("User").Child(user.Id).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                //Debug.Log("successfully added data to firebase");
            }
            else
            {
                Debug.Log("not successfull added data to firebase");
            }
        });
    }

    //6: Función que descarga la foto del usuario nuevo desde la red social, o cualquier link de imagen.
    IEnumerator DownloadPhotoFromSN(string userpPicUrl)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("Downloading profile photo from the user's social network");
        Debug.Log("Downloading profile photo from the user's social network");

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(userpPicUrl);
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D texture = new Texture2D(1, 1);
            texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            Texture2D squarePhoto = new Texture2D(1, 1);
            squarePhoto = SquarePhotoGenerator(texture);
            //la foto se convierte a textura2D y sprite para diversos fines.
            texturePhoto = squarePhoto;
            spritePhoto = Texture2DtoSprite(squarePhoto);

            DebugMaster.debugInstance.AddMesaggeToConsole("Profile photo downloaded successfully");
            Debug.Log("Profile photo downloaded successfully");
        }
    }

    //7: Función que recibe la imagen descargada y la sube a Storage firebase.
    private IEnumerator UploadPhotoToStorage(Texture2D profilePhotoUp, string photoGuid)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("Uploading profile photo to storage");
        Debug.Log("Uploading profile photo to storage");

        var storage = FirebaseStorage.DefaultInstance;
        string path = $"/profilePics/{currentUser.Id}/{photoGuid}.png";
        var profilePicRef = storage.GetReference(path);
        currentUser.PhotoUrl = path;
        var bytes = profilePhotoUp.EncodeToPNG();
        var uploadTask = profilePicRef.PutBytesAsync(bytes);
        yield return new WaitUntil(predicate: () => uploadTask.IsCompleted);
        if (uploadTask.Exception != null)
        {
            Debug.LogError($"Failed to upload because {uploadTask.Exception}");
            yield break;
        }
        var getUrlTask = profilePicRef.GetDownloadUrlAsync();
        yield return new WaitUntil(predicate: () => getUrlTask.IsCompleted);

        if (getUrlTask.Exception != null)
        {
            Debug.LogError($"Failed to get download url with {getUrlTask.Exception}");
            yield break;
        }
        DebugMaster.debugInstance.AddMesaggeToConsole("Uploaded photo successfully");
        Debug.Log("Uploaded photo successfully");
    }
    //8: Inicia la actulización de la foto de perfil desde la galería.
    public void UploadProfilePhoto(Texture2D photoGallery, string photoGuid)
    {
        StartCoroutine(UploadPhotoFromGalleryToStorage(photoGallery, photoGuid));
    }
    //8.1: Función que recibe la foto seleccionada desde la galería y la sube a Storage firebase.
    private IEnumerator UploadPhotoFromGalleryToStorage(Texture2D photoGallery, string photoGuid)
    {
        Util.utilInstance.DisableButton("LogOut");
        //1: Assing photo from gallery to photo sprite
        Texture2D squarePhoto = new Texture2D(1, 1);
        squarePhoto = SquarePhotoGenerator(photoGallery);
        spritePhoto = Texture2DtoSprite(squarePhoto);
        texturePhoto = squarePhoto;
        Debug.Log("Assing photo from gallery to photo sprite");
        yield return new WaitForEndOfFrame();

        //2: Reload profile screen to load new profile photo
        StartCoroutine(RefreshProfile());
        Debug.Log("Reload profile screen to load new profile photo");
        yield return new WaitForEndOfFrame();

        //3: Upload new profile picture to storage
        yield return UploadPhotoToStorage(squarePhoto, photoGuid);
        Debug.Log("Upload new profile picture to storage");

        //4: Update Firebase user register
        UpdatePicUrl($"/profilePics/{currentUser.Id}/{photoGuid}.png");
        Debug.Log("Update Firebase user register");
        Util.utilInstance.ActiveButton("LogOut");
    }
    //Debug para saber si un usuario esta en sesión (en desarrollo).
    [ContextMenu("KnowSession")]
    public void KnowSession()
    {
        Debug.Log($"Session state: {isInSession}");
    }
    //9: Función encargada de llevar al usuario a la pantalla de home, desde la de login.
    public void BeginSession()
    {
        // VivoxVoiceManager.Instance.OnUserLoggedInEvent += OpenHomeScreen;

        if (VivoxVoiceManager.Instance.LoginState != LoginState.LoggedIn)
        {
            if (Application.isEditor)
            {
                VivoxVoiceManager.Instance.Login(PhotonNetwork.LocalPlayer.NickName);
            }
            else
            {
                switch (Application.platform)
                {
                    case RuntimePlatform.Android:
                        if (Permission.HasUserAuthorizedPermission(Permission.Microphone))
                        {
                            VivoxVoiceManager.Instance.Login(PhotonNetwork.LocalPlayer.NickName);
                        }
                        else
                        {
                            Permission.RequestUserPermission(Permission.Microphone);
                        }

                        break;
                    case RuntimePlatform.IPhonePlayer:
                        if (Application.HasUserAuthorization(UserAuthorization.Microphone))
                        {
                            VivoxVoiceManager.Instance.Login(PhotonNetwork.LocalPlayer.NickName);
                        }
                        else
                        {
                            Application.RequestUserAuthorization(UserAuthorization.Microphone);
                        }

                        break;
                }
            }
        }

        ScreenManager.main.OpenScreen(SerializableScreen.Type.Menu);
        ScreenManager.main.OpenScreen(SerializableScreen.Type.Home, false);
        //PunManager.Instance.SetPlayerCustomProperties(UserName, UserId, UserPhotoUrl);
        DebugMaster.debugInstance.AddMesaggeToConsole($"Welcome '{UserName}'");
        Debug.Log($"Welcome '{UserName}'");

        UpdateOnlineStatus(true);
        isInSession = true;

    }

    private void OpenHomeScreen()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.Menu);
        ScreenManager.main.OpenScreen(SerializableScreen.Type.Home, false);
        VivoxVoiceManager.Instance.OnUserLoggedInEvent -= OpenHomeScreen;
    }
    //AUX1: Convierte una imagen de texture2D a sprite.
    public static Sprite Texture2DtoSprite(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
    //AUX2: Desactiva y vuelve a activar la pantalla de profile para ver los cambios actualizados.
    private IEnumerator RefreshProfile()
    {
        ScreenManager.main.TurnScreen(SerializableScreen.Type.Profile);
        yield return new WaitForEndOfFrame();
        ScreenManager.main.TurnScreen(SerializableScreen.Type.Profile);
    }

    public void DownloadPhoto(string url, SavedPlayer image)
    {
        StartCoroutine(DownloadPhotoFromPUN(url, image));
    }

    public IEnumerator DownloadPhotoFromPUN(string userpPhotocUrl, SavedPlayer image)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("Downloading photo PUN");
        Debug.Log("Downloading photo PUN");
        Debug.Log($"LINK {userpPhotocUrl}");
        var storage = FirebaseStorage.DefaultInstance;
        var screenshotReference = storage.GetReference(userpPhotocUrl);

        var downloadTask = screenshotReference.GetBytesAsync(long.MaxValue);
        yield return new WaitUntil(predicate: () => downloadTask.IsCompleted);

        var texture = new Texture2D(width: 2, height: 2);
        texture.LoadImage(downloadTask.Result);
        image.llenado(Texture2DtoSprite(texture));
    }
    //10: Descarga la foto del usuario desde Storage firebase.
    public IEnumerator DownloadPhotoFromStorage(string userpPhotocUrl)
    {
        DebugMaster.debugInstance.AddMesaggeToConsole("Downloading profile photo");
        Debug.Log("Downloading profile photo");

        var storage = FirebaseStorage.DefaultInstance;
        var screenshotReference = storage.GetReference(userpPhotocUrl);

        var downloadTask = screenshotReference.GetBytesAsync(long.MaxValue);
        yield return new WaitUntil(predicate: () => downloadTask.IsCompleted);

        var texture = new Texture2D(width: 2, height: 2);
        texture.LoadImage(downloadTask.Result);
        spritePhoto = Texture2DtoSprite(texture);


        Util.utilInstance.ActiveButton("LogOut");
        DebugMaster.debugInstance.AddMesaggeToConsole("Profile photo downloaded successfully");
        Debug.Log("Profile photo downloaded successfully");
    }
    //TOOL1: Crea una lista de los scripts a partir del string de todos los scripts comprados por el usuario.
    private void CreateScriptList(string userScriptList)
    {
        UserScriptList.Clear();
        char[] b = userScriptList.ToCharArray();
        string scriptItem = "";

        foreach (char item in b)
        {
            if (item != ';')
            {
                scriptItem += item;
            }
            else
            {
                UserScriptList.Add(scriptItem);
                scriptItem = "";
            }
        }

        var scriptList = "";
        foreach (var debugItem in UserScriptList)
        {
            scriptList += $"{debugItem}\n";
        }

        DebugMaster.debugInstance.AddMesaggeToConsole($"Formatted Script List:\n{scriptList}");
        Debug.Log($"Formatted Script List:\n{scriptList}");
    }
    //TOOL1.2: Borra la lista de scripts.
    public void ClearScriptList()
    {
        UserScriptList.Clear();
    }
    //11: Retorna la lista de los scripts comprados.
    public List<string> LoadScriptList()
    {
        return UserScriptList;
    }
    //TOOL2: Formatea el Id del usuario para solo mostrar los ultimos 7 caracteres.
    private string FormattedId(string id)
    {
        char[] b = id.ToCharArray();
        string shortUserId = "";

        for (int i = (b.Length - 7); i < b.Length; i++)
        {
            shortUserId += b[i];
        }
        return "ID: " + shortUserId;
    }
    //TOOL3: Formatea el nombre del usuario para solo mostrar el primer nombre.
    private string FormattedName(string name)
    {
        char[] b = name.ToCharArray();

        string shortUserName = "";

        foreach (char item in b)
        {
            if (item != ' ')
            {
                shortUserName += item;
            }
            else
            {
                return shortUserName;
            }
        }
        return shortUserName;
    }
    //TOOL4: Genera una Texture2D cuadrada del tamaño mas grande posible según la imagen proporcionada.
    private Texture2D SquarePhotoGenerator(Texture2D texture)
    {
        if (texture.width == texture.height)
        {
            return texture;
        }
        else if (texture.width > texture.height)
        {
            int xOffset = (texture.width - texture.height) / 2;
            Debug.Log($"Resize Width to: {texture.height} with offset of {xOffset}");
            Color[] pix = texture.GetPixels(xOffset, 0, texture.height, texture.height);
            Texture2D resizeTex = new Texture2D(texture.height, texture.height);
            resizeTex.SetPixels(pix);
            resizeTex.Apply();
            return Resize(resizeTex);
        }
        else
        {
            int yOffset = (texture.height - texture.width) / 2;
            Color[] pix = texture.GetPixels(0, yOffset, texture.width, texture.width);
            Texture2D resizeTex = new Texture2D(texture.width, texture.width);
            resizeTex.SetPixels(pix);
            resizeTex.Apply();
            return Resize(resizeTex);
        }
    }
    //TOOL5: Redimensiona una Texture2D en una mas pequeña, según targetSize.
    Texture2D Resize(Texture2D texture2D)
    {
        int targetSize = 128;
        RenderTexture rt = new RenderTexture(targetSize, targetSize, 24);
        RenderTexture.active = rt;
        Graphics.Blit(texture2D, rt);
        Texture2D result = new Texture2D(targetSize, targetSize);
        result.ReadPixels(new Rect(0, 0, targetSize, targetSize), 0, 0);
        result.Apply();
        return result;
    }
    //TOOL6: Verifica si la aplicación esta en foco (en desarrollo)
    void OnApplicationFocus(bool pauseStatus)
    {
        if (isInSession)
        {
            if (pauseStatus) { UpdateOnlineStatus(true); }
            else { UpdateOnlineStatus(false); }
        }
    }
    private void OnApplicationQuit()
    {

        if (isInSession)
        {
            UpdateOnlineStatus(false);
            isInSession = false;
        }
    }
    public void IsInSesion(bool session)
    {
        isInSession = session;
    }


}
