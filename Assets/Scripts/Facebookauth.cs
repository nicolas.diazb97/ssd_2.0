﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using Facebook.Unity;
//using Firebase.Auth;
//using System;
//using TMPro;
//using UnityEngine.Networking;
//using UnityEngine.UI;


//public class Facebookauth : MonoBehaviour
//{

//    [Header("Config")]
//    [SerializeField] private Text infoText;
//    [SerializeField] private Util util;
//    [SerializeField] private RealtimeDatabaseManager dataBase;
//    private FirebaseAuth auth;

//    [Header("DataSet")]
//    private string displayName;
//    private string userEmail;
//    private string userId;
//    private string picUrl;
//    private Uri profilepic;
//    //AuxVariables
//    private string loginScreen = "Login";
//    private string homeScreen = "Home";
//    private string menuScreen = "Menu";
//    private string databaseObj = "DataBase";

//    /*
//    [Header("Screens")] [SerializeField] private GameObject Home;
//    [SerializeField] private GameObject Menu;
//    [SerializeField] private GameObject Login;
//    [SerializeField] private TextMeshProUGUI DisplayName;
//    [SerializeField] private GameObject Picture;
//    [SerializeField] private TextMeshProUGUI DisplayName2;
//    [SerializeField] private GameObject Picture2;
//    */

//    /*
//    [SerializeField] private Image Picture;
//    [SerializeField] private TextMeshProUGUI DisplayName;
//    [SerializeField] private GameObject Screen;
//    [SerializeField] private GameObject Login;
//    [SerializeField] private GameObject Menu;
//    */

//    private void Awake()
//    {
//        if (!FB.IsInitialized)
//        {
//            FB.Init(InitCallBack, OnHideUnity);
//        }
//        else
//        {
//            FB.ActivateApp();
//        }
//    }
//    private void InitCallBack()
//    {
//        if (!FB.IsInitialized)
//        {
//            FB.ActivateApp();
//        }
//        else
//        {
//            Debug.Log("Failed to initialize");
//        }
//    }
//    private void OnHideUnity(bool isgameshown)
//    {
//        if (!isgameshown)
//        {
//            Time.timeScale = 0;
//        }
//        else
//        {
//            Time.timeScale = 1;
//        }
//    }

//    public void Facebook_Login()
//    {
//        var permission = new List<string>() { "public_profile", "email" };
//        FB.LogInWithReadPermissions(permission, AuthCallBack);
//    }

//    private void AuthCallBack(ILoginResult result)
//    {
//        if (FB.IsLoggedIn)
//        {
//            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
//            //  debug.text = (aToken.UserId);

//            string accesstoken;
//            string[] data;
//            string acc;
//            string[] some;
//#if UNITY_EDITOR
//            Debug.Log("this is raw access " + result.RawResult);
//            data = result.RawResult.Split(',');
//            Debug.Log("this is access" + data[3]);
//            acc = data[3];
//            some = acc.Split('"');
//            Debug.Log("this is access " + some[3]);
//            infoText.text = infoText.text +"Facebook Works"+ some[3].ToString();
//            accesstoken = some[3];
            
//#elif UNITY_ANDROID || UNITY_IOS
//            Debug.Log("this is raw access "+result.RawResult);
//            data = result.RawResult.Split(',');
//            Debug.Log("this is access"+data[0]);
//             acc = data[0];
//             some = acc.Split('"');
//            Debug.Log("this is access " + some[3]);
           

//            infoText.text = infoText.text +"Facebook Works"+ some[3].ToString();
//             accesstoken = some[3];
//#endif
//            authwithfirebase(accesstoken);
//        }
//        else
//        {
//            Debug.Log("User Cancelled login");
//        }
//    }
//    public void authwithfirebase(string accesstoken)
//    {
//        auth = FirebaseAuth.DefaultInstance;
//        Firebase.Auth.Credential credential = Firebase.Auth.FacebookAuthProvider.GetCredential(accesstoken);
//        auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
//        {
//            if (task.IsFaulted)
//            {
//                Debug.Log("singin encountered error" + task.Exception);
//            }
//            Firebase.Auth.FirebaseUser newuser = task.Result;

//            //GZACK IMPLEMENTATION

//            AddToInformation("Sign In FB");
//            AddToInformation("Name: " + newuser.DisplayName);
//            AddToInformation("Email: " + newuser.Email);
//            AddToInformation("PicUrl: " + newuser.PhotoUrl);
//            AddToInformation("UserId: " + newuser.UserId);

//            displayName = newuser.DisplayName;
//            userEmail = newuser.Email;
//            userId = newuser.UserId;
//            picUrl = newuser.PhotoUrl.ToString() + "?width=1080&height=1080";

//            //

//            StartCoroutine(ChangeGameState());
//            /*
//            DisplayName.text = newuser.DisplayName;
//            DisplayName2.text = newuser.DisplayName;
//            StartCoroutine(DownloadImage(newuser.PhotoUrl.AbsoluteUri));
//            */
//        });
//    }
//    IEnumerator ChangeGameState()
//    {
//        //yield return new WaitForEndOfFrame();
//        //util.ActiveScreen(databaseObj);

//        //yield return new WaitForEndOfFrame();
//        //dataBase.StartCoroutine("ValidateUserExist", userId);

//        //yield return new WaitForSeconds(2f);
//        //dataBase.SaveData(userId, userEmail, displayName, picUrl);
//        //AddToInformation("SaveData: " + userId);

//        //yield return new WaitForEndOfFrame();
//        //dataBase.StartCoroutine("LoadUserData", userId);

//        //yield return new WaitForSeconds(0.5f);
//        //util.ActiveScreen(homeScreen);
//        //util.ActiveScreen(menuScreen);
//        //util.DisableScreen(loginScreen);
//    }

//    /*  public void authwithfirebase(string accesstoken)
//      {
//          Debug.Log("fire base access");
//          auth = FirebaseAuth.DefaultInstance;
//          Firebase.Auth.Credential credential = Firebase.Auth.FacebookAuthProvider.GetCredential(accesstoken);
//          auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
//          {
//              if (task.IsFaulted)
//              {
//                  Debug.Log("singin encountered error" + task.Exception);
//              }

//              AddToInformation("SignIn With Facebook");
//              Firebase.Auth.FirebaseUser newuser = task.Result;

//              AddToInformation("Name: " + newuser.DisplayName);
//              AddToInformation("UserId: " + newuser.UserId);
//              AddToInformation("Email: " + newuser.Email);
//              AddToInformation("PicUrl: " + newuser.PhotoUrl.AbsoluteUri + "?width=1080&height=1080");

//              picUrl = newuser.PhotoUrl.AbsoluteUri + "?width=1080&height=1080";
//              displayName = newuser.DisplayName;
//              userId = newuser.UserId;
//              userEmail = newuser.Email;
//            // StartCoroutine("ChangeGameState");
//           Login.SetActive(false);
//           Home.SetActive(true);
//           Menu.SetActive(true);

//          });
//          Debug.Log("end firebase acess");
//      } */
//    /*
//    public IEnumerator DownloadImage(string MediaUrl)
//    {
//        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl + "?width=1080&height=1080");
//        yield return request.SendWebRequest();
//        if (request.isNetworkError || request.isHttpError)
//            Debug.Log(request.error);
//        else
//        {

//            Texture2D tex = ((DownloadHandlerTexture)request.downloadHandler).texture;
//            Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(tex.width / 2, tex.height / 2));
//            Picture.GetComponent<Image>().overrideSprite = sprite;
//            Picture2.GetComponent<Image>().overrideSprite = sprite;
//        }
//        Login.SetActive(false);
//        Home.SetActive(true);
//        Menu.SetActive(true);
//    }
//    */
//    private void AddToInformation(string str) { infoText.text += "\n" + str; }
//}