using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ResolutionEvent : MonoBehaviour
{
    public Vector2 minRes;
    public UnityEvent OnResolutionReached;
    // Start is called before the first frame update
    void Start()
    {
        if(Screen.width<= minRes.x && Screen.height <= minRes.y)
        {
            OnResolutionReached.Invoke();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Screen.width);
    }
}
