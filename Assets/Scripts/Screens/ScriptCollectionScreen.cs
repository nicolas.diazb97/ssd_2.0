using Data.Scripts;
using Data.ScriptsData;
using Managers;
using UnityEngine;

namespace Screens
{
    public class ScriptCollectionScreen : ScreenTemplate
    {
        [SerializeField] private ScriptItem itemPrefab;
        [SerializeField] private Transform contentParent;

        private void OnEnable()
        {
            foreach (var script in DataManager.main.AvailableScripts)
            {
                var itemInstance = Instantiate(itemPrefab, contentParent);
                itemInstance.OpenButton.interactable = !script.isUnderDev;
                itemInstance.Setup(script);
                CheckForBadges(script,itemInstance.gameObject);
            }
            LoadScriptButtonsState();
        }

        public override void Init()
        {
            base.Init();
            ScreenManager.main.OpenScreen(SerializableScreen.Type.Menu);
            LoadScriptButtonsState();
        }

        public void CheckForBadges(GameScript script, GameObject item)
        {
            if (script.isUnderDev)
            {
                item.GetComponentInChildren<ComingSoonBadge>().SetBadge();
            }
        }
        public override void Close()
        {
            base.Close();
            ResetStateButtons();
        }

        private void ResetStateButtons()
        {

            foreach (var button in contentParent.GetComponentsInChildren<ScriptItem>())
            {
                button.MarkAsNotPurchased();
            }
        }
        private void LoadScriptButtonsState()
        {
            var scriptList = RealtimeDatabaseManager.rtdbmReference.LoadScriptList();
            var debugList = "";
            foreach (var debugItem in scriptList)
            {
                debugList += $"{debugItem}\n";
            }

            DebugMaster.debugInstance.AddMesaggeToConsole($"Update buttons by:\n{debugList}");
            Debug.Log($"Update buttons by:\n{debugList}");

            foreach (var button in contentParent.GetComponentsInChildren<ScriptItem>())
            {
                foreach (var scriptName in scriptList)
                {
                    if (button.GetComponent<ScriptItem>().GetTitle() == scriptName)
                    {
                        button.MarkAsPurchased();
                    }
                }

            }
        }
    }
}