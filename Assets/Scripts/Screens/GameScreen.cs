﻿using System;
using Managers;
using Photon.Pun;
using Screens.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Utils.ScreensUtils;

namespace Screens
{
    public class GameScreen : ScreenTemplate
    {
        [SerializeField] private UnityEvent onGameStarted;
        [SerializeField] private UnityEvent onGameFinished;
        [SerializeField] private TMP_Text txtRoomId;
        [SerializeField] private TMP_Text txtScriptTitle;
        [SerializeField] private TMP_Text txtRoundName;
        [SerializeField] private TMP_Text txtNext;
        [SerializeField] private Transform playersContainer;
        [SerializeField] private StateButton isReadyButton;
        [SerializeField] private Button btnOpenCluesOrganizer;
        [SerializeField] private Button evaluationButton;
        [FormerlySerializedAs("btnLeave")] [SerializeField] private LeaveRoomButton btnLeave;
        [SerializeField] private Button btnOpenScriptsOrganizer;
        [SerializeField] private Button btnChangeRound;
        [SerializeField] private EvaluationScreen evaluationScreen;
        [SerializeField] private AudioSource backgroundMusicPlayer;
        [SerializeField] private GameObject EvaluationGameobject;
        [SerializeField] private GameObject ResultsGameobject;

        private int _playersReady;

        public override void Init()
        {
            base.Init();
            DataManager.main.OnReconnectionDone.AddListener(SetResultsPhase);
            evaluationButton.gameObject.SetActive(false);
            DataManager.main.OnAllPlayersVote += () => evaluationButton.gameObject.SetActive(true);
            PunManager.Instance.Ingame = true;
            PunManager.Instance.OnPlayerReadyOnInterphase += OnPlayerReady;
            isReadyButton.OnStateToggled += HandleStateToggled;
            PunManager.Instance.OnLeaveRoom += onGameFinished.Invoke;
            GameManager.main.OnNewRoundStarted += HandleNewRound;
            btnOpenCluesOrganizer.onClick.AddListener(OpenCluesOrganizer);
            btnOpenScriptsOrganizer.onClick.AddListener(OpenScriptsOrganizer);
            DataManager.main.SetQuestionsData();
            /*if (PunManager.Instance.ReconectRoom)
            {
                var scriptId = PhotonNetwork.CurrentRoom.CustomProperties[PunManager.ScriptKey].ToString();
                Debug.Log("el script id de la reconect es  " + scriptId);
                var CurrentRoomScript = DataManager.main.GetScriptById(int.Parse(scriptId));
                CameraManager.main.MapRenderer = CurrentRoomScript.SearchMap;
            }
            else
            {
                CameraManager.main.MapRenderer = DataManager.main.CurrentRoomScript.SearchMap;
            } */

            CameraManager.main.MapRenderer = DataManager.main.CurrentRoomScript.SearchMap;

            txtRoomId.text = $"Room ID: {PunManager.Instance.CurrentRoomId}";
            txtScriptTitle.text = DataManager.main.CurrentRoomScript.Title;
            if (!PunManager.Instance.ReconectRoom)
            {
                txtRoundName.text = string.Empty;
            }
            else
            {
                txtRoundName.text = RoundConfig.RoundEnumToString(GameManager.main.CurrentRound);
            }

            RealtimeDatabaseManager.rtdbmReference.UpdateRoomId(PhotonNetwork.CurrentRoom.Name);
            Debug.Log(" players container");
            for (var i = 0; i < playersContainer.childCount; i++)
            {
                var child = playersContainer.GetChild(i);
                var characters = DataManager.main.CurrentRoomScript.Characters;
                var inRange = i < characters.Length;
                Debug.Log(" inrange en index" + i);
                child.gameObject.SetActive(inRange);
                if (inRange)
                {
                    if (child.TryGetComponent(out CharacterReadyHandler readyHandler))
                    {
                        readyHandler.Setup(characters[i]);
                    }
                }
            }
            MusicManager.main.PlayMusic(false);
        }

        public void SetupPlayersHandler()
        {
            for (var i = 0; i < playersContainer.childCount; i++)
            {
                var child = playersContainer.GetChild(i);
                var characters = DataManager.main.CurrentRoomScript.Characters;
                var inRange = i < characters.Length;
                Debug.Log(" inrange en index" + i);
                child.gameObject.SetActive(inRange);
                if (inRange)
                {
                    if (child.TryGetComponent(out CharacterReadyHandler readyHandler))
                    {
                        readyHandler.Setup(characters[i]);
                    }
                }
            }
            MusicManager.main.PlayMusic(false);
        }

        public override void ResetIt()
        {
            base.ResetIt();
            evaluationButton.gameObject.SetActive(false);
        }

        public override void Close()
        {
            base.Close();
            PunManager.Instance.OnPlayerReadyOnInterphase -= OnPlayerReady;
            isReadyButton.OnStateToggled -= HandleStateToggled;
            _playersReady = 0;
            PunManager.Instance.OnLeaveRoom -= onGameFinished.Invoke;
            GameManager.main.OnNewRoundStarted -= HandleNewRound;
            btnOpenCluesOrganizer.onClick.RemoveListener(OpenCluesOrganizer);
            btnOpenScriptsOrganizer.onClick.RemoveListener(OpenScriptsOrganizer);
            backgroundMusicPlayer.Stop();
            // close screen eva
            EvaluationGameobject.gameObject.SetActive(false);
            ResultsGameobject.gameObject.SetActive(false);
        }

        private void HandleNewRound(RoundConfig roundConfig)
        {
            var popup = ScreenManager.main.OpenScreen<RoundStartedPopup>(false);
            popup.SetInfo(roundConfig);
            switch (roundConfig.RoundName)
            {
                case Round.ScriptReading:
                    onGameStarted?.Invoke();
                    backgroundMusicPlayer.PlayOneShot(DataManager.main.CurrentRoomScript.BackgroundMusic);
                    txtNext.text = "Self Intro";
                    btnLeave.gameObject.SetActive(false);
                    break;
                case Round.SelfIntroduction:
                    txtNext.text = "Search for clues";
                    break;
                case Round.SearchForClues1:
                    txtNext.text = "Next round";
                    break;
                case Round.SearchForCluesU:
                    txtNext.text = "Discussion";
                    break;
                case Round.SearchForClues2:
                    txtNext.text = "Discussion";
                    break;
                case Round.Discussion:
                    txtNext.text = "Voting";
                    break;
                case Round.Voting:
                    DataManager.main.OnAllPlayersVote += SetResultsPhase;
                    Timer.Instance.SetTimerActive(true);
                    var screen = ScreenManager.main.OpenScreen<InGameQuestions>();
                    screen.SetQuestions();
                    screen.SetUp();
                    txtNext.text = "Questions";
                    break;
                case Round.Results:
                    if(!PhotonNetwork.IsMasterClient) PunManager.Instance.LoadDataFromjson();
                    btnLeave.gameObject.SetActive(true);
                    DataManager.main.InvokeAllPlayersVoted();
                    ScreenManager.main.CloseScreen(SerializableScreen.Type.InGameQuestions);
                    var votingResults = ScreenManager.main.OpenScreen<VotingResults>(false);
                    votingResults.UpdateVisuals();
                    break;
            }

            isReadyButton.SetState(false);
            _playersReady = 0;
            txtRoundName.text = RoundConfig.RoundEnumToString(roundConfig.RoundName);
            btnChangeRound.interactable = true;
        }
        private void SetResultsPhase()
        {
            if(GameManager.main.CurrentRound == Round.Results) txtNext.text = "Truth";
            Timer.Instance.StopTimer();
        }
        private void HandleStateToggled(bool isReady)
        {
            switch (GameManager.main.CurrentRound)
            {
                // if (GameManager.main.CurrentRound == Round.Voting)
                // {
                //     if (GameManager.main.HasVoted)
                //     {
                //         var scriptsOrganizer = ScreenManager.main.OpenScreen<ScriptsOrganizer>(false);
                //         scriptsOrganizer.JumpToInfoOnLoad = InfoType.Truth;
                //     }
                //     else
                //     {
                //         var scriptsOrganizer = ScreenManager.main.OpenScreen<InGameQuestions>(false);
                //     }
                //
                //     return;
                // }
                case Round.Voting:
                    ScreenManager.main.OpenScreen<InGameQuestions>(false);
                    return;
                case Round.Results:
                    var scriptsOrganizer = ScreenManager.main.OpenScreen<ScriptsOrganizer>(false);
                    scriptsOrganizer.JumpToInfoOnLoad = InfoType.Truth;
                    
                    return;
            }

            btnChangeRound.interactable = false;
            PunManager.Instance.NotifyPlayerReadyOnInterphase(isReady);
        }

        private void OpenCluesOrganizer()
        {
            ScreenManager.main.OpenScreen(SerializableScreen.Type.CluesOrganizer, false);
        }

        private void OpenScriptsOrganizer()
        {
            CameraManager.main.ToggleMovementActive(false);
            ScreenManager.main.OpenScreen(SerializableScreen.Type.ScriptsOrganizer, false);
        }

        private void OnPlayerReady(string playerReady, bool status)
        {
            Debug.Log($"On player ready. Player: <color=yellow>{playerReady}.</color>. Status: <color=blue>{status}</color>");
            if (status)
            {
                if (++_playersReady >= PunManager.Instance.MaxRoomPlayers)
                {
                    PunManager.Instance.NotifyNewRoundStarted();
                }
            }
            else
            {
                --_playersReady;
            }
        }

        public void OpenVotingResults()
        {
            var scriptsOrganizer = ScreenManager.main.OpenScreen<VotingResults>(false);
            //scriptsOrganizer.JumpToInfoOnLoad = InfoType.vori;
        }

        public void OpenInGameQuestions()
        {
            ScreenManager.main.OpenScreen(SerializableScreen.Type.InGameQuestions, false);
        }

        public void OpenEvaluation()
        {
            ScreenManager.main.OpenScreen(SerializableScreen.Type.Evaluation, false);
        }
    }

}