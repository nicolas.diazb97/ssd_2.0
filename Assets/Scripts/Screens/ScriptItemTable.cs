﻿using System.Collections.Generic;
using Data.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Utils.ScreensUtils;

namespace Screens
{
    [RequireComponent(typeof(ScriptPropertiesHandler))]
    public class ScriptItemTable : ScreenTemplate
    {
        [SerializeField] private Button btnCreateRoom;

        [Header("Data Objects")]
        [SerializeField] private TextMeshProUGUI itemPrice;

        [FormerlySerializedAs("isPurshasedLabel")] [SerializeField] private GameObject isPurchasedLabel;
        [SerializeField] private GameObject purchaseButton;
        [SerializeField] private GameObject createButton;
        [SerializeField] private List<GameObject> purchaseList = new List<GameObject>();

        private string formatName;
        private string sPrice;
        private GameScript _scriptOnDisplay;
        private ScriptPropertiesHandler _propertiesHandler;

        private void Awake()
        {
            _propertiesHandler = GetComponent<ScriptPropertiesHandler>();
        }

        public override void Init()
        {
            base.Init();
            btnCreateRoom.onClick.AddListener(OnClickCreateRoom);
            Debug.Log("Script item table init");

        }

        public override void Close()
        {
            base.Close();
            btnCreateRoom.onClick.RemoveListener(OnClickCreateRoom);
            Debug.Log("Script item table close");
        }

        public void SetupInfo(GameScript data)
        {
            _propertiesHandler.Setup(_scriptOnDisplay = data);
            //LoadStateScript(data.Title);
            //LoadPurchaseScriptButton(data);
            

        }

        public void LoadPurchaseScriptButton(GameScript data)
        {
            Debug.Log($"ScripName: {data.Title}");

            switch (data.Title)
            {
                case "Murder at the Amusement Park5":
                    ActiveAndConfigButton(0, data.Title);
                    Debug.Log(" se activa el murder 5");
                    break;
                case "Assassination of JFK":
                    ActiveAndConfigButton(1, data.Title);
                    break;
                case "Millionaire Match":
                    ActiveAndConfigButton(2, data.Title);
                    break;
                case "Demon Realm":
                    ActiveAndConfigButton(3, data.Title);
                    break;
                case "Murder at the Amusement Park":
                    ActiveAndConfigButton(4, data.Title);
                    break;
                case "Millionaire Match Test":
                    ActiveAndConfigButton(5, data.Title);
                    break;
                case "Demon Realm Test":
                    ActiveAndConfigButton(6, data.Title);
                    break;
                case "Cuckoo's Nest":
                    ActiveAndConfigButton(7, data.Title);
                    Debug.Log(" se activa cuckos");
                    break;
                case "Twilight":
                    ActiveAndConfigButton(8, data.Title);
                    Debug.Log(" se activa cuckos");
                    break;
                
            }
        }

        private void ActiveAndConfigButton(int index, string scriptName)
        {
            Debug.Log($"Activating: {scriptName}");
            formatName = Billing.FormatSName(scriptName);
           // purchaseList[index].SetActive(true);
            purchaseButton = purchaseList[index];
            purchaseList[index].SetActive(true);
            if (purchaseButton.GetComponent<Button>().interactable == false)
            {
                RealtimeDatabaseManager.rtdbmReference.UpdateScripts(formatName);
            }
            purchaseList[index].GetComponentInChildren<TextMeshProUGUI>().text = Billing.sharedInstence.GetScriptPrice(formatName);
        }

        private void LoadStateScript(string scriptName)
        {
            Debug.Log("state script");
            formatName = Billing.FormatSName(scriptName);

            itemPrice.text = Billing.sharedInstence.GetScriptPrice(formatName);

            var purchasedScripts = RealtimeDatabaseManager.rtdbmReference.LoadScriptList();

            NotPurchasedState();
            foreach (string item in purchasedScripts)
            {
                if (item == formatName)
                {
                    PurchasedState();
                    return;
                }
            }
        }
        public void NotPurchasedState()
        {
            purchaseButton.SetActive(true);
            isPurchasedLabel.SetActive(false);
            createButton.SetActive(false);
        }

        public void PurchasedState()
        {
            createButton.SetActive(true);
            purchaseButton.SetActive(false);
            isPurchasedLabel.SetActive(true);
        }



        public void OpenScriptCollection()
        {
            ScreenManager.main.OpenScreen(SerializableScreen.Type.ScriptCollection);
            purchaseButton.SetActive(false);
        }

        private void OnClickCreateRoom()
        {
            var createRoomScreen = ScreenManager.main.OpenScreen<CreateRoomScreen>();
            createRoomScreen.Setup(_scriptOnDisplay);
        }
    }
}
