using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ProfileScreen : ScreenTemplate
{
    [Header("References")]
    [SerializeField] private RealtimeDatabaseManager dataBase;


    [Header("DataSet")]
    [SerializeField] private TextMeshProUGUI userName;
    [SerializeField] private TextMeshProUGUI scriptsStats;

    [SerializeField] private TextMeshProUGUI id;

    [SerializeField] private TMP_InputField inputtmpro;
    [SerializeField] private GameObject okButton;
    [SerializeField] private Image userImg;


    [SerializeField] private TextMeshProUGUI ownedScrips;

    public override void Init()
    {
        base.Init();
        LoadProfileInfo();
    }
    public override void Close()
    {
        base.Close();
        inputtmpro.text = "";
        inputtmpro.gameObject.SetActive(false);
        okButton.SetActive(false);
    }

    public void LoadProfileInfo()
    {
        id.text = dataBase.UserId;
        userName.text = dataBase.UserName;

        uint playedScripts = RealtimeDatabaseManager.rtdbmReference.UserPlayedScripts;

        scriptsStats.text = $"{playedScripts}/{40}";
        userImg.sprite = dataBase.UserSpritePhoto;
        ownedScrips.text = $"0/{RealtimeDatabaseManager.rtdbmReference.LoadScriptList().Count}";
    }

    public void ChangeName()
    {
        if (inputtmpro.text != "")
        {
            dataBase.UpdateName(inputtmpro.text);
            LoadProfileInfo();
            inputtmpro.text = "";
        }
        else
        {
            inputtmpro.text = "";
        }
    }

    public void LockSpaces()
    {
        char[] b = inputtmpro.text.ToCharArray();

        string shortuserName = "";

        foreach (char item in b)
        {
            if (item != ' ')
            {
                shortuserName += item;
            }
        }
        inputtmpro.text = shortuserName;
        shortuserName = "";
    }

    public void OpenHelpPage()
    {
        ScreenManager.main.CloseScreen();
        ScreenManager.main.OpenScreen(SerializableScreen.Type.HelpPage);
    }

    public void OpenLogin()
    {
        ScreenManager.main.CloseScreen();
        ScreenManager.main.OpenScreen(SerializableScreen.Type.Login);
    }


}
