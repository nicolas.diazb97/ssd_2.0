﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Managers;

public class QuestionDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private AcusedDisplay acusedDisplayPrefab;
    [SerializeField] private VoteDisplay voteDisplayPrefab;
    private int index;

    private List<AcusedDisplay> accuseds = new List<AcusedDisplay>();
    private VoteDisplay currentVoteDisplay;
    public void SetTitle(string value) => title.text = value;
    public void SetIndex(int value) => index = value;

    public void SetCorrectAnswerStatus() 
    {
        if (index != DataManager.main.CurrentRoomScript.AccusedQuestionIndex) return;
        for (int i = 0; i < accuseds.Count; i++)
        {
            accuseds[i].SetAnswerStatus();
        }
    }

    public void AddAcused(AnswerResutl accused)
    {
        AcusedDisplay currentAcussed= Instantiate(acusedDisplayPrefab, transform);
        currentAcussed.SetName(accused.Answer);
        currentAcussed.SetPhoto(accused.AnswerPic);
        currentAcussed.SetIndex(index);
        currentAcussed.Votes = accused.Votes;
        accuseds.Add(currentAcussed);
    }

    private void SetCorrectAnswerAtTop()
    {
        for (int i = 0; i < accuseds.Count; i++)
        {
            if (accuseds[i].GetName == QuestionManager.main.Questions[index].CorrectAnswer.text)
            {
                accuseds[i].transform.SetSiblingIndex(1);
            }
        }
    }

    public void ShowVotes()
    {
        SetCorrectAnswerAtTop();
        foreach (var accused in accuseds)
        {
            foreach (Vote vote in accused.Votes)
            {
                currentVoteDisplay = Instantiate(voteDisplayPrefab, accused.VotersParent);
                currentVoteDisplay.SetPhoto(DataManager.main.GetCharacterPic(vote.UserName));
            }
        }
    }

    private void OnDestroy()
    {
        DataManager.main.OnAllPlayersVote -= SetCorrectAnswerStatus;
        for (int i = 0; i < accuseds.Count; i++)
        {
            Destroy(accuseds[i].gameObject);
        }
        accuseds.Clear();
    }

    private void Start()
    {
        DataManager.main.OnAllPlayersVote += SetCorrectAnswerStatus;
    }
    private void OnEnable()
    {
        DataManager.main.OnAllPlayersVote += SetCorrectAnswerStatus;
    }
}
