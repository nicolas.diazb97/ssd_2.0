﻿using Data.Scripts;
using Data.ScriptsData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils.ScreensUtils;

namespace Screens
{
    [RequireComponent(typeof(ScriptPropertiesHandler))]
    public class CreateRoomScreen : ScreenTemplate
    {
        [SerializeField] private Button btnCreateRoom;
        [SerializeField] private TMP_InputField iptPasswd;
        [SerializeField] private int minPasswdChars = 4;
        [SerializeField] private int maxPasswdChars = 12;

        private GameScript _scriptOnDisplay;
        private ScriptPropertiesHandler _propertiesHandler;
        private string _roomPasswd = string.Empty;

        private void Awake()
        {
            _propertiesHandler = GetComponent<ScriptPropertiesHandler>();
            iptPasswd.characterLimit = maxPasswdChars;
        }

        public override void Init()
        {
            base.Init();
            btnCreateRoom.onClick.AddListener(OnClickCreateRoom);
            iptPasswd.onValueChanged.AddListener(SetRoomPassword);
            iptPasswd.text = string.Empty;
        }

        public override void Close()
        {
            base.Close();
            btnCreateRoom.onClick.RemoveListener(OnClickCreateRoom);
            iptPasswd.onValueChanged.RemoveListener(SetRoomPassword);
            _roomPasswd = string.Empty;
        }

        public void Setup(GameScript script)
        {
            _propertiesHandler.Setup(_scriptOnDisplay = script);
        }

        private void SetRoomPassword(string password)
        {
            var isPasswdValid = IsValid(password);
            btnCreateRoom.interactable = isPasswdValid;
            if (isPasswdValid) _roomPasswd = password;
        }

        private void OnClickCreateRoom()
        {
            if (!IsValid(_roomPasswd)) _roomPasswd = string.Empty;
            Debug.Log($"Room password: {_roomPasswd}");
            PunManager.Instance.CreateRoom(_scriptOnDisplay.MinPlayers.ToString(), _scriptOnDisplay.ScriptId, _roomPasswd);
        }

        public void SetUsePasswordForRoom(bool isUsed)
        {
            btnCreateRoom.interactable =
                !isUsed || IsValid(iptPasswd.text);

            if (!isUsed)
            {
                iptPasswd.SetTextWithoutNotify(_roomPasswd = string.Empty);
            }
        }

        public void OnClickBack()
        {
            ScreenManager.main.OpenScreen(SerializableScreen.Type.ScriptDetail);
        }

        private bool IsValid(string input) => input.Length >= minPasswdChars && input.Length <= maxPasswdChars;
    }
}