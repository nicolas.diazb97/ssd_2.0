
public class HelpPageScreen : ScreenTemplate
{
    public override void Init()
    {
        base.Init();
    }

    public void OpenProfile()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.Menu);
        ScreenManager.main.OpenScreen(SerializableScreen.Type.Profile, false);
    }

    public void WhatIs()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.WhatIs);
    }
    public void UiTutorial()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.UiTutorial);
    }
}
