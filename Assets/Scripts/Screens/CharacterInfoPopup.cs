﻿using Data.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Screens
{
    public class CharacterInfoPopup : ScreenTemplate
    {
        [SerializeField] private TMP_Text txtCharacterDesc;
        [SerializeField] private Image imgCharacterThumbnail;

        public void Setup(GameScript.Character displayCharacter)
        {
            imgCharacterThumbnail.sprite = displayCharacter.picture;
            txtCharacterDesc.text =
                $"{displayCharacter.name}\n{displayCharacter.gender}, {displayCharacter.age}\n{displayCharacter.occupation}";
        }
    }
}