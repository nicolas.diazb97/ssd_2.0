using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class HomeScreen : ScreenTemplate
{
    [Header("References")]
    [SerializeField] private RealtimeDatabaseManager dataBase;

    [Header("DataSet")]
    [SerializeField] private TextMeshProUGUI userName;
    [SerializeField] private Image userImg;

    public override void Init()
    {
        base.Init();
        LoadProfileInfo();
        ScreenManager.main.CloseScreen(SerializableScreen.Type.ResetScreen);
        if (RealtimeDatabaseManager.rtdbmReference.UserRoomId != "")
        {
            Debug.Log("se intenta conectar");
            PunManager.Instance.ReconectRoom = true;
            PhotonNetwork.JoinRoom(RealtimeDatabaseManager.rtdbmReference.UserRoomId);
        }
        else
        {
            Debug.Log("no hay donde reconectar");
            PunManager.Instance.ReconectRoom = false;
        }
        MusicManager.main.PlayMusic(true);
    }
    public void LoadProfileInfo()
    {
        userName.text = dataBase.UserName;
        userImg.sprite = dataBase.UserSpritePhoto;
    }

    public void OpenGameLobby()
    {
        ScreenManager.main.CloseScreen();
        ScreenManager.main.OpenScreen(SerializableScreen.Type.GameLobby);
    }


}
