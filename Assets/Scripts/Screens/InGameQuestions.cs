using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Managers;

public class InGameQuestions : ScreenTemplate
{
    [SerializeField] private float screenTime = 120f;
    [SerializeField] private QuestionAnswerProperties questionPrefab;
    [SerializeField] private Transform questionContent;
    [SerializeField] private TMP_Text questionText;
    [SerializeField] private TMP_Text timeText;
    [SerializeField] private Button questionButton;
    [SerializeField] private TMP_Text questionButtonText;
    [SerializeField] private Button exitButton;
    private QuestionAnswerProperties selectedAnswer;
    private bool screenSetted = false;

    private void OnTimeEnds()
    {
        // Timer.Instance.OnTimeFinish -= OnTimeEnds;
        // Timer.Instance.OnTimeFinish -= NextScreen;
        Timer.Instance.OnTimeUpdated -= UpdateTime;
        DataManager.main.OnAllPlayersVote -= NextScreen;
        questionButton.onClick.RemoveAllListeners();
        // NextScreen();
      if(GameManager.main.CurrentRoundConfig.RoundName != Round.Results)  PunManager.Instance.NotifyNewRoundStarted();
    }

    public void SetQuestions()
    {
        // init setup
        Timer.Instance.OnTimeFinish += OnTimeEnds;
        Timer.Instance.OnTimeUpdated += UpdateTime;
        // DataManager.main.OnAllPlayersVote += NextScreen;
        questionButton.onClick.AddListener(() => CheckAnswer());
        Timer.Instance.StartTimer(screenTime);
        UpdateTime(screenTime);

    }

    public void UpdateTime(float time) => timeText.text = "("+time.ToString("0")+" s"+ ")";

    public override void Init()
    {
        base.Init();
        
        // if (!screenSetted)
        // {
        //     Timer.Instance.OnTimeFinish += OnTimeEnds;
        //     Timer.Instance.OnTimeUpdated += UpdateTime;
        //     DataManager.main.OnAllPlayersVote += NextScreen;
        //     questionButton.onClick.AddListener(() => CheckAnswer());
        //     Timer.Instance.StartTimer(screenTime);
        //     UpdateTime(screenTime);
        //     screenSetted = !screenSetted;
        // }
    }

    public override void ResetIt()
    {
        base.ResetIt();
        DataManager.main.TotalVotes = byte.MinValue;
        questionButton.interactable = true;
        questionButton.GetComponent<Animator>().SetBool("onReady",true);
        screenSetted = false;
    }

    public override void Close()
    {
        Debug.Log("Game questions close");
        base.Close();
    }

    public void OnClose()
    {
        ScreenManager.main.CloseScreen();
    }

    public void SetUp()
    {
        Question currentQuestion = QuestionManager.main.CurrentQuestion;
        Debug.Log(currentQuestion.QuestionName);
        questionText.text = string.Concat("<b>", currentQuestion.QuestionName, "</b>");
        questionButtonText.text = !QuestionManager.main.IsLastQuestion ? "Next Question" : "Submit";

        foreach (Transform child in questionContent)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < currentQuestion.Answers.Length; i++)
        {
            char letter = (char)(65 + i);
            QuestionAnswerProperties question = Instantiate(questionPrefab, questionContent);
            question.GetComponent<Button>().onClick.AddListener(() => SelectOption(question));
            question.Index = i;
            question.AccusedName = currentQuestion.Answers[i].text;
            question.transform.Find("Name").GetComponent<TMP_Text>().text = question.AccusedName;
            question.transform.Find("YellowImage").Find("Letter").GetComponent<TMP_Text>().text = $"{letter}.";
        }
    }

    public void SelectOption(QuestionAnswerProperties obj)
    {
        questionButton.GetComponent<Animator>().SetBool("onReady", true);
        selectedAnswer = obj;
        foreach (Transform child in questionContent)
        {
            child.Find("YellowImage").GetComponent<Image>().enabled = false;
            child.GetComponent<QuestionAnswerProperties>().IsSelected = false;
        }
        obj.IsSelected = true;
        obj.transform.Find("YellowImage").GetComponent<Image>().enabled = true;
    }

    private void NextScreen()
    {
        ScreenManager.main.CloseScreen(SerializableScreen.Type.InGameQuestions);
        var screen = ScreenManager.main.OpenScreen<VotingResults>(false);
        screen.UpdateVisuals();
    }

    public void OnLastQuestion()
    {
        questionButton.onClick.RemoveListener(OnLastQuestion);
        questionButton.interactable = false;
        questionButton.GetComponent<Animator>().SetBool("onReady", false);
        questionButton.onClick.RemoveAllListeners();
        ScreenManager.main.CloseScreen(SerializableScreen.Type.InGameQuestions);
        PunManager.Instance.QuestionsClearConirmation();
    }

    public void CheckAnswer()
    {
        if (selectedAnswer == null) return;

        ResultsManager.main.AddAcused(selectedAnswer.AccusedName);
        ResultsManager.main.AddVoteToAccused(selectedAnswer.AccusedName);
        QuestionManager.main.CheckAnswer(selectedAnswer.Index);
        selectedAnswer = null;
        SetUp();
    }
}
