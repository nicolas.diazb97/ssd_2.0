using System;
using System.Collections.Generic;
using UnityEngine;
using Managers;
using UnityEngine.UI;
using TMPro;
using WebSocketSharp;
using System.Collections;
using System.Threading.Tasks;
using Data.Scripts;

public enum InfoType
{
    Background, Task, Truth, Story, Today
}

public class ScriptsOrganizer : ScreenTemplate
{
    [Header("Buttons")]
    [SerializeField] private Button btnBackground;
    [SerializeField] private Button btnYourHistory;
    [SerializeField] private Button btnToday;
    [SerializeField] private Button btnTask;
    [SerializeField] private Button btnTruth;
    [SerializeField] private Button btnOptions;
    [SerializeField] private Button btnPanel;
    [SerializeField] private RectTransform readingContent;

    [Header("Text")]
    [SerializeField] private TextMeshProUGUI descriptionTemplate;

    [Header("Input")]
    [SerializeField] private TMP_InputField InputField;
    [SerializeField] private TextSetup PrefabText;
    [SerializeField] private Transform ContainerTextSearch;
    [SerializeField] private Button cleanSearchBtn;
    [TextArea]
    public string disclaimerText;

    //AUX
    private float backgroundSize, storySize, todaySize, taskSize, truthSize;

    public static ScriptsOrganizer main;

    public InfoType JumpToInfoOnLoad { get; set; }

    private void Awake()
    {
        if (main == null)
        {
            main = this;
        }
        else
        {
            Destroy(gameObject);
        }

        JumpToInfoOnLoad = InfoType.Background;
    }


    public void ToLoad()
    {
        LoadInfo();
    }


    private bool FirstOpen = false;
    public override void Init()
    {
        base.Init();
        // StartCoroutine(LoadDefInfo());

        /*  if (!FirstOpen && GameManager.main.CurrentRoundConfig.RoundName == Round.Results )
          {
              FirstOpen = true;
              LoadInfo();
          }*/
        LoadInfo();

        btnBackground.onClick.AddListener(LoadBackgroundInfo);
        btnYourHistory.onClick.AddListener(LoadYourStory);
        btnToday.onClick.AddListener(LoadToday);
        btnTask.onClick.AddListener(LoadTask);
        btnTruth.onClick.AddListener(LoadTruth);

        cleanSearchBtn.onClick.AddListener(CleanSearch);

        btnOptions.onClick.AddListener(LoadOptions);
        btnPanel.onClick.AddListener(OnClose);

        InputField.onValueChanged.AddListener(SearchInScript);
    }

    public override void Close()
    {
        base.Close();
        btnBackground.onClick.RemoveAllListeners();
        btnYourHistory.onClick.RemoveAllListeners();
        btnToday.onClick.RemoveAllListeners();
        btnTask.onClick.RemoveAllListeners();
        btnTruth.onClick.RemoveAllListeners();

        btnOptions.onClick.RemoveAllListeners();
        btnPanel.onClick.RemoveAllListeners();

        InputField.onValueChanged.RemoveAllListeners();
        CameraManager.main.ToggleMovementActive(true);
    }

    private void LoadOptions()
    {

        btnOptions.onClick.RemoveAllListeners();
        btnPanel.onClick.RemoveAllListeners();

        btnOptions.onClick.AddListener(CloseOptions);
        btnPanel.onClick.AddListener(CloseOptions);

        ScreenManager.main.OpenScreen(SerializableScreen.Type.MenuScriptsOrganizer, false);
    }
    // IEnumerator LoadDefInfo()
    // {
    //     descriptionTemplate.text = "";
    //
    //     SetTitle("Background");
    //     foreach (var backgroundText in DataManager.main.BackgroundText())
    //     {
    //         descriptionTemplate.text += backgroundText.Text;
    //     }
    //     yield return new WaitForEndOfFrame();
    //     storySize = readingContent.rect.height;
    //
    //     SetTitle("Story");
    //     foreach (var story in DataManager.main.PersonalStoryText())
    //     {
    //         descriptionTemplate.text += story.Text;
    //     }
    //     yield return new WaitForEndOfFrame();
    //     todaySize = readingContent.rect.height;
    //
    //
    //     SetTitle("Today");
    //     foreach (var todayText in DataManager.main.PersonaltodayText())
    //     {
    //         descriptionTemplate.text += todayText.Text;
    //     }
    //     yield return new WaitForEndOfFrame();
    //     taskSize = readingContent.rect.height;
    //
    //     SetTitle("Task");
    //     descriptionTemplate.text += DataManager.main.CharacterAssigned.task;
    //     yield return new WaitForEndOfFrame();
    //     truthSize = readingContent.rect.height;
    //
    //     if (GameManager.main.CurrentRound == Round.Voting)
    //     {
    //         truthSize = readingContent.rect.height;
    //         SetTitle("Truth");
    //         foreach (var truthText in DataManager.main.Truth())
    //         {
    //             descriptionTemplate.text += truthText.Text;
    //         }
    //         yield return new WaitForEndOfFrame();
    //     }
    //
    //     for (int i = 0; i < 30; i++)
    //     {
    //         descriptionTemplate.text += "\n";
    //     }
    //     
    //     switch (JumpToInfoOnLoad)
    //     {
    //         case InfoType.Background:
    //             break;
    //         case InfoType.Task:
    //             LoadTask();
    //             break;
    //         case InfoType.Truth:
    //             LoadTruth();
    //             break;
    //         case InfoType.Story:
    //             LoadYourStory();
    //             break;
    //         case InfoType.Today:
    //             LoadToday();
    //             break;
    //         default:
    //             throw new ArgumentOutOfRangeException();
    //     }
    //
    //     JumpToInfoOnLoad = InfoType.Background;
    // }
    public void SetBackgroundText()
    {
        descriptionTemplate.text = "";
        SetTitle("Background");
        foreach (var backgroundText in DataManager.main.BackgroundText())
        {
            descriptionTemplate.text += backgroundText.Text;
        }
        descriptionTemplate.text += disclaimerText;
    }
    public void SetTodayText()
    {
        descriptionTemplate.text = "";
        SetTitle(DataManager.main.CurrentRoomScript.TimelineTitle.ToString());
        foreach (var backgroundText in DataManager.main.PersonaltodayText())
        {
            descriptionTemplate.text += backgroundText.Text;
        }
        //descriptionTemplate.text += disclaimerText;
    }
    public void SetYourStoryText()
    {
        descriptionTemplate.text = "";
        SetTitle("Your story");
        foreach (var story in DataManager.main.PersonalStoryText())
        {
            descriptionTemplate.text += story.Text;
        }
        //descriptionTemplate.text += disclaimerText;
    }
    public void SetGoalsText()
    {
        descriptionTemplate.text = "";
        SetTitle("Goals");
        descriptionTemplate.text += DataManager.main.CharacterAssigned.task;
        //descriptionTemplate.text += disclaimerText;
    }
    public void SetTruthText()
    {
        descriptionTemplate.text = "";
        SetTitle("Truth");
        foreach (var truthText in DataManager.main.Truth())
        {
            descriptionTemplate.text += truthText.Text;
        }
        //descriptionTemplate.text += disclaimerText;
    }
    private async void LoadInfo()
    {

        descriptionTemplate.text = " ";
        todaySize = 0;
        storySize = 0;

        readingContent.sizeDelta = Vector2.one;
        readingContent.anchoredPosition = Vector2.zero;
        SetTitle("Background");
        foreach (var backgroundText in DataManager.main.BackgroundText())
        {
            descriptionTemplate.text += backgroundText.Text;
        }

        descriptionTemplate.text += disclaimerText;
        //await Task.Yield();

        //storySize = readingContent.rect.height;

        //SetTitle("Your story");
        //foreach (var story in DataManager.main.PersonalStoryText())
        //{
        //    descriptionTemplate.text += story.Text;
        //}
        //await Task.Yield();
        //todaySize = readingContent.rect.height;


        //SetTitle(DataManager.main.CurrentRoomScript.TimelineTitle.ToString());
        //foreach (var todayText in DataManager.main.PersonaltodayText())
        //{
        //    descriptionTemplate.text += todayText.Text;
        //}
        //await Task.Yield();
        //taskSize = readingContent.rect.height;

        //SetTitle("Goals");
        //descriptionTemplate.text += DataManager.main.CharacterAssigned.task;
        //await Task.Yield();

        //truthSize = readingContent.rect.height;
        //btnTruth.gameObject.SetActive(false);

        //if (GameManager.main.CurrentRound == Round.Results)
        //{

        //    SetTitle("Truth");
        //    foreach (var truthText in DataManager.main.Truth())
        //    {
        //        descriptionTemplate.text += truthText.Text;
        //    }
        //    btnTruth.gameObject.SetActive(true);

        //}
        await Task.Yield();
        /* await Task.Yield();
               for (int i = 0; i < 30; i++)
               {
                   descriptionTemplate.text += "\n";
               }
       */
        Debug.Log("load " + JumpToInfoOnLoad.ToString());
        switch (JumpToInfoOnLoad)
        {
            case InfoType.Background:
                break;
            case InfoType.Task:
                LoadTask();
                break;
            case InfoType.Truth:
                TruthBtn();
                break;
            case InfoType.Story:
                LoadYourStory();
                break;
            case InfoType.Today:
                LoadToday();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            btnPanel.onClick.RemoveAllListeners();
            btnPanel.onClick.AddListener(OnClose);
        }
    }

    private void SetTitle(string title)
    {
        //  Debug.Log($"Setting title {title}");
        descriptionTemplate.text += $"<b>{title}:</b>";
    }

    public void LoadBackgroundInfo()
    {
        CloseOptions();
        readingContent.localPosition = new Vector2(readingContent.localPosition.x, 0 + 942.2317f);

    }
    public void LoadYourStory()
    {
        CloseOptions();
        readingContent.localPosition = new Vector2(readingContent.localPosition.x, storySize + 942.2317f);

    }

    public void LoadToday()
    {
        CloseOptions();
        readingContent.localPosition = new Vector2(readingContent.localPosition.x, todaySize + 942.2317f);

    }
    public void LoadTask()
    {
        CloseOptions();
        // readingContent.localPosition = new Vector2(readingContent.localPosition.x, taskSize + 942.2317f);
        readingContent.localPosition = new Vector2(readingContent.localPosition.x, taskSize + 942.2317f);

    }
    public void LoadTruth()
    {
        //  JumpToInfoOnLoad = InfoType.Truth;

        readingContent.localPosition = new Vector2(readingContent.localPosition.x, truthSize + 942.2317f);
        CloseOptions();


    }

    public void TruthBtn()
    {
        JumpToInfoOnLoad = InfoType.Truth;
        readingContent.localPosition = new Vector2(readingContent.localPosition.x, truthSize + 942.2317f);

    }

    private void CloseOptions()
    {
        btnOptions.onClick.RemoveAllListeners();
        btnPanel.onClick.RemoveAllListeners();

        btnOptions.onClick.AddListener(LoadOptions);
        btnPanel.onClick.AddListener(OnClose);
        //  ScreenManager.main.CloseScreen(SerializableScreen.Type.MenuScriptsOrganizer);
        ScreenManager.main.CloseScreen();
    }


    public void OnClose()
    {
        ScreenManager.main.CloseScreen();
        CameraManager.main.ToggleMovementActive(true);
    }

    private void CleanSearch()
    {
        InputField.text = "";
    }
    public void SearchInScript(string ChangeValue)
    {
        foreach (Transform var in ContainerTextSearch)
        {
            Destroy(var.gameObject);
        }

        if (ChangeValue.IsNullOrEmpty())
        {
            return;
        }
        //  var Text = DataManager.main.ScriptTexts();
        //  List<string> ToInstance = new List<string>();
        Dictionary<GameScript.ParagraphStruct, string> ToInstance = new Dictionary<GameScript.ParagraphStruct, string>();

        ToInstance.Clear();

        // llenado

        foreach (var String in DataManager.main.BackgroundText())
        {
            //   if (String.Text.IsNullOrEmpty()) return;

            //  var temp = String.Split('\n', '.');
            var temp = String.Text.Split('\n');
            Debug.Log(temp + "temp variable separada por saltos de pagina");

            string longtext = "";

            foreach (var VARIABLE in DataManager.main.BackgroundText())
            {
                longtext = longtext + VARIABLE.Text;
            }
            Debug.Log("este es el long text + " + longtext);
            foreach (var parrafo in temp)
            {
                var temp123 = ChangeValue.Substring(0, 1).ToUpper() + ChangeValue.Substring(1).ToLower();
                Debug.Log("palabra a buscar con la primera mayuscula" + temp123);

                if (parrafo.Contains(ChangeValue) || parrafo.Contains(temp123))
                {
                    if (!ToInstance.ContainsKey(String))
                    {
                        ToInstance.Add(String, longtext);
                    }

                }
            }
        }

        foreach (var String in DataManager.main.PersonalStoryText())
        {
            if (String.Text.IsNullOrEmpty()) return;

            //  var temp = String.Split('\n', '.');
            var temp = String.Text.Split('\n');
            Debug.Log(temp + "temp variable separada por saltos de pagina");
            string longtext = "";

            foreach (var VARIABLE in DataManager.main.PersonaltodayText())
            {
                //  longtext = longtext + VARIABLE + "\n\r" + "\n\r";
                longtext = longtext + VARIABLE.Text;
            }
            Debug.Log("este es el long text + " + longtext);
            foreach (var parrafo in temp)
            {
                var temp123 = ChangeValue.Substring(0, 1).ToUpper() + ChangeValue.Substring(1).ToLower();
                Debug.Log("palabra a buscar con la primera mayuscula" + temp123);
                if (parrafo.Contains(ChangeValue) || parrafo.Contains(temp123))
                {
                    if (!ToInstance.ContainsKey(String))
                    {
                        ToInstance.Add(String, longtext);
                    }

                }
            }
        }

        foreach (var String in DataManager.main.PersonaltodayText())
        {
            if (String.Text.IsNullOrEmpty()) return;

            //  var temp = String.Split('\n', '.');
            var temp = String.Text.Split('\n');
            Debug.Log(temp + "temp variable separada por saltos de pagina");
            string longtext = "";

            foreach (var VARIABLE in DataManager.main.PersonaltodayText())
            {
                //  longtext = longtext + VARIABLE + "\n\r" + "\n\r";
                longtext = longtext + VARIABLE.Text;
            }
            Debug.Log("este es el long text + " + longtext);
            foreach (var parrafo in temp)
            {
                var temp123 = ChangeValue.Substring(0, 1).ToUpper() + ChangeValue.Substring(1).ToLower();
                Debug.Log("palabra a buscar con la primera mayuscula" + temp123);
                if (parrafo.Contains(ChangeValue) || parrafo.Contains(temp123))
                {
                    if (!ToInstance.ContainsKey(String))
                    {
                        ToInstance.Add(String, longtext);
                    }

                }
            }
        }




        if (GameManager.main.CurrentRound == Round.Results)
        {
            foreach (var String in DataManager.main.Truth())
            {
                if (String.Text.IsNullOrEmpty()) return;

                //  var temp = String.Split('\n', '.');
                var temp = String.Text.Split('\n');
                Debug.Log(temp + "temp variable separada por saltos de pagina");
                string longtext = "";

                foreach (var VARIABLE in DataManager.main.Truth())
                {
                    //  longtext = longtext + VARIABLE + "\n\r" + "\n\r";
                    longtext = longtext + VARIABLE.Text;
                }
                Debug.Log("este es el long text + " + longtext);
                foreach (var parrafo in temp)
                {
                    var temp123 = ChangeValue.Substring(0, 1).ToUpper() + ChangeValue.Substring(1).ToLower();
                    Debug.Log("palabra a buscar con la primera mayuscula" + temp123);
                    if (parrafo.Contains(ChangeValue) || parrafo.Contains(temp123))
                    {
                        if (!ToInstance.ContainsKey(String))
                        {
                            ToInstance.Add(String, longtext);
                        }

                    }
                }
            }
        }

        // intance
        if (ToInstance.Count <= 0) return;
        Debug.LogWarning("to instance " + ToInstance.Count);
        foreach (var TextToInstance in ToInstance)
        {
            var clone = Instantiate(PrefabText);
            clone.transform.localScale = Vector3.one;
            clone.transform.SetParent(ContainerTextSearch);
            clone.SetupText(TextToInstance.Key, ChangeValue, TextToInstance.Value);
        }
    }

    public string DescriptionTemplate()
    {
        return descriptionTemplate.text;
    }

    private void Start()
    {
        GameManager.main.OnNewRoundStarted += (r) =>
        {
            print("ROUND NAME" + r.RoundName);
            btnTruth.gameObject.SetActive(r.RoundName == Round.Results);
        };

        btnToday.GetComponentInChildren<TextMeshProUGUI>().text = DataManager.main.CurrentRoomScript.TimelineTitle.ToString();
    }
}
