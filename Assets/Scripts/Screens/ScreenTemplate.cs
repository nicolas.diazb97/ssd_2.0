using UnityEngine;

public abstract class ScreenTemplate : MonoBehaviour
{
    public bool IsOpen { get; private set; }
    
    public virtual void Init()
    {
        LoadingVisualsHandler.main.SetState(false);
        gameObject.SetActive(true);
        IsOpen = true;
    }
    public virtual void Close()
    {
        gameObject.SetActive(false);
        IsOpen = false;
    }

    public virtual void ResetIt()  {}
}
