﻿using System;
using System.Collections.Generic;
using System.Linq;
using Managers;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using Utils.ScreensUtils;
using WebSocketSharp;

namespace Screens
{
    public class CluesOrganizer : ScreenTemplate
    {
        [FormerlySerializedAs("clueInfoPrefab")] [SerializeField] private OrganizerClue organizerCluePrefab;
        [SerializeField] private OrganizerSearchPlace filterButtonPrefab;
        
        [Header("Containers")]
        [SerializeField] private Transform cluesContainer;
        [SerializeField] private Transform filtersContainer;
        
        
        public override void Init()
        {
            base.Init();
            
            var children = new List<GameObject>();
            foreach (Transform child in filtersContainer.transform) children.Add(child.gameObject);
          // children.ForEach(child =>  Destroy(child));
          foreach (var VAR in children)
          {
              if (VAR.name != "All Clues Button")
              {
                  Destroy(VAR);
              }
              
          }  
          
          
            var clues = DataManager.main.CluesFound;
            var listedPlaces = new List<string>();
           // SpawnClueCategory("Clue", "All Clues");
            var searchPlace = SpawnClueCategory(PhotonNetwork.LocalPlayer.NickName, "My Clues");
            foreach (var clue in clues)
            {
                var clueInstance = Instantiate(organizerCluePrefab, cluesContainer);
                clueInstance.Setup(clue);

                string clueFoundIn;
                if (String.IsNullOrEmpty(clue.TagFound))
                {
                    clueFoundIn = clue.FoundIn.PlaceName;
                }
                else
                {
                    clueFoundIn = clue.TagFound;
                }


                if (listedPlaces.Contains(clueFoundIn)) continue;

                SpawnClueCategory(clueFoundIn);
                listedPlaces.Add(clueFoundIn);
            }

            CameraManager.main.ToggleMovementActive(false);
            searchPlace.GetComponent<FilterButton>().ToggleFilterEnabled();
        }

        private OrganizerSearchPlace SpawnClueCategory(string category, string categoryText = "")
        {
            var placeFilter = Instantiate(filterButtonPrefab, filtersContainer);
            placeFilter.SetClueCategory(category);
            if (categoryText != string.Empty)
            {
                placeFilter.SetCategoryText(categoryText);
            }

            return placeFilter;
        }

        public void SpawnClueCategory(string category, Transform parent, string categoryText = "")
        {
            var placeFilter = Instantiate(filterButtonPrefab, parent);
            placeFilter.SetClueCategory(category);
            if (categoryText != string.Empty)
            {
                placeFilter.SetCategoryText(categoryText);
            }
        }

        public override void Close()
        {
            base.Close();
            
            foreach (var container in new [] {cluesContainer, filtersContainer})
            {
                foreach (Transform child in container)
                {
                    if (child.GetComponent<OrganizerClue>())
                    {
                        Destroy(child.gameObject);
                    }
                }
            }
            
            CameraManager.main.ToggleMovementActive(true);
        }

        public void OnClose()
        {
            ScreenManager.main.CloseScreen();
            CameraManager.main.ToggleMovementActive(true);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnClose();
            }
        }
    }
}