using UnityEngine;
using Managers;
using System.Collections.Generic;
using Data.Scripts;

public class VotingResults : ScreenTemplate
{
    [SerializeField] private Transform questionResultContent;
    [SerializeField] private QuestionDisplay questionDisplay;
    [SerializeField] private GameObject resultsButton;
    [SerializeField] private GameObject evaluationScreen;

    List<QuestionDisplay> questionDisplays = new List<QuestionDisplay>();
    public override void Init()
    {
        base.Init();
    }

    public override void ResetIt()
    {
        base.ResetIt();
        resultsButton.gameObject.SetActive(false);
    }

    public override void Close()
    {
        if (!resultsButton.activeSelf || !evaluationScreen.activeSelf)
        {
            resultsButton.SetActive(true);
            evaluationScreen.SetActive(true);
        }
        base.Close();
    }

    public void OnClose()
    {

        ScreenManager.main.CloseScreen();
    }

    private void ClearVisuals()
    {
        foreach (Transform child in questionResultContent)
            Destroy(child.gameObject);
    }

    private void OnDisable()
    {
        ClearVisuals();
    }

    public void OnEnable()
    {
        UpdateVisuals();
    }

    public void UpdateVisuals()
    {

        ClearVisuals();
        for (int i = 0; i < DataManager.main.Results.Count; i++)
        {
            Result result = DataManager.main.Results[i];

            QuestionDisplay currentDisplay = Instantiate(questionDisplay, questionResultContent);
            currentDisplay.SetTitle(result.QuestionName);
            currentDisplay.SetIndex(i);

            GameScript roomScript = DataManager.main.CurrentRoomScript;

            string correctAnswer = QuestionManager.main.Questions[i].CorrectAnswer.text;

            bool killerInAccuseds = false;

            foreach (AnswerResutl accused in result.Answers)
            {
                if (accused.Answer == correctAnswer)
                {
                    killerInAccuseds = true;
                    break;
                }
            }

            if (!killerInAccuseds)
            {
                AnswerResutl accused = new AnswerResutl
                {
                    Answer = correctAnswer,
                    AnswerPic = DataManager.main.GetCharacterPic(correctAnswer),
                    AnswerId = ++DataManager.main.CurrentAcusedId
                };

                result.AddAccused(accused);
            }
            questionDisplays.Add(currentDisplay);

            foreach (AnswerResutl accused in result.Answers)
            {
                currentDisplay.AddAcused(accused);
            }
            currentDisplay.ShowVotes();
        }


        for (int i = 0; i < questionDisplays.Count; i++)
        {
            questionDisplays[i].SetCorrectAnswerStatus();
        }
    }
}
