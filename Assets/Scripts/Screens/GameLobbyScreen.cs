using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon;
using Photon.Realtime;
using UnityEngine;
using Managers;
using Photon.Pun;

namespace Screens
{
    public class GameLobbyScreen : ScreenTemplate
    {
        [SerializeField] private Transform roomsContainer;
        [SerializeField] private RoomItem roomItemPrefab;
       
        public override void Init()
        {
            base.Init();
            PunManager.Instance.OnRoomCreated += OnRoomListUpdated;
            OnRoomListUpdated(PunManager.Instance.cachedRoomList.Values.ToList());
            MusicManager.main.PlayMusic(true);
        }

        public override void Close()
        {
            base.Close();
            PunManager.Instance.OnRoomCreated -= OnRoomListUpdated;
            ClearContainer();
        }

        private void OnRoomListUpdated(List<RoomInfo> rooms)
        {
            
            
            
            // ordenar lista nueva segun parametros
           // rooms.OrderBy(rooms => DataManager.main.GetScriptById(int.Parse(rooms.CustomProperties[PunManager.ScriptKey].ToString())).PriceType).ThenByDescending(rooms => rooms.PlayerCount).ThenBy(rooms=> DataManager.main.GetScriptById(int.Parse(rooms.CustomProperties[PunManager.ScriptKey].ToString())).Duration).ThenByDescending( rooms=> DataManager.main.GetScriptById(int.Parse(rooms.CustomProperties[PunManager.ScriptKey].ToString())).ScriptDifficulty);
         var x = rooms
                 .OrderByDescending(rooms => DataManager.main.GetScriptById(int.Parse(rooms.CustomProperties[PunManager.ScriptKey].ToString())).PriceType)
                 .ThenByDescending(rooms => rooms.PlayerCount-rooms.MaxPlayers)
                 .ThenBy(rooms=> rooms.MaxPlayers)
                 .ThenBy(rooms=> PunManager.Instance.Indice(rooms) )
                 .ThenBy((rooms=> DataManager.main.GetScriptById(int.Parse(rooms.CustomProperties[PunManager.ScriptKey].ToString())).Duration))
                 .ThenBy(rooms => (int)(DataManager.main.GetScriptById(int.Parse(rooms.CustomProperties[PunManager.ScriptKey].ToString())).ScriptDifficulty))
             ;
            ClearContainer();
            foreach (var roomCache in x)
            { 
                var roomItem = Instantiate(roomItemPrefab, roomsContainer); 
                roomItem.Setup(roomCache);
                Debug.Log("password on room list updated"+roomCache.CustomProperties["Password"]);
               
            }

            if (RealtimeDatabaseManager.rtdbmReference.UserRoomId != "")
            {
              //  var roomItem = Instantiate(roomItemPrefab, roomsContainer); 
             /*   RoomInfo roomcache = new Room(RealtimeDatabaseManager.rtdbmReference.UserRoomId,null);
                roomItem.Setup(roomcache);
                */ 
             Debug.LogWarning("se intenta reconectar a la sala " + RealtimeDatabaseManager.rtdbmReference.UserRoomId );
             //  PunManager.Instance.JoinRoom(RealtimeDatabaseManager.rtdbmReference.UserRoomId);
            }
        }

        private void ClearContainer()
        {
            foreach (Transform child in roomsContainer)
            {
                Destroy(child.gameObject);
            }
        }

        public void OpenHome()
        {
            ScreenManager.main.OpenScreen(SerializableScreen.Type.Menu);
            ScreenManager.main.OpenScreen(SerializableScreen.Type.Home, false);
        }

        public void OpenScriptCollection()
        {
            ScreenManager.main.OpenScreen(SerializableScreen.Type.Menu);
            ScreenManager.main.OpenScreen(SerializableScreen.Type.ScriptCollection, false);
        }
    }
}
