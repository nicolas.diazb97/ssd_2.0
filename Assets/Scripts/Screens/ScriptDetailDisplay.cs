using Data.Scripts;
using UnityEngine;

namespace Screens
{
    public class ScriptDetailDisplay : ScreenTemplate
    {
        [SerializeField] private ScriptProperties propertiesHandler;

        public void Setup(GameScript data)
        {
            propertiesHandler.SetProperties(data);
        }
    }
}