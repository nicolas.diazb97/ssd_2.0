
public class PictureMenuScreen : ScreenTemplate
{
    public override void Init()
    {
        base.Init();
    }

    public void OpenProfile()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.Profile);
    }
}
