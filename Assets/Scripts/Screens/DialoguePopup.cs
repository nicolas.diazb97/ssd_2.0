﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Screens
{
    public class DialoguePopup : ScreenTemplate
    {
        [SerializeField] private TMP_Text txtDialogue;
        [SerializeField] private Button btnAccept;

        public void Setup(string message)
        {
            txtDialogue.text = message;
        }

        public override void Init()
        {
            base.Init();
            btnAccept.onClick.AddListener(OnClickAccept);
        }

        public override void Close()
        {
            base.Close();
            btnAccept.onClick.RemoveListener(OnClickAccept);
        }

        private void OnClickAccept()
        {
            ScreenManager.main.CloseScreen();
        }
    }
}