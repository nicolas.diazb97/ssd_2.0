using Data.Scripts;
using Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EvaluationScreen : ScreenTemplate
{
    [SerializeField] private bool hasVotedInThisSection = false;

    [SerializeField] private Transform reusltsParent;
    [SerializeField] private CharacterEvaluationDisplay evaluationDisplayPrefab;
    private List<CharacterEvaluationDisplay> displays = new List<CharacterEvaluationDisplay>();
    private List<CharacterEvaluationDisplay> displaysOrder = new List<CharacterEvaluationDisplay>();
    private List<string> tags = new List<string>();
    public Sprite[] valorationSprites;
    [TextArea]
    public string DisclaimerText;
    public override void Init()
    {
        base.Init();
        DataManager.main.charactersOnScript.Clear();
        DataManager.main.GameAssignations.ToList().ForEach(ga =>
        {
            DataManager.main.charactersOnScript.Add(ga.AssignedCharacter.name);
        });

    }
    public override void ResetIt()
    {
        base.ResetIt();
        hasVotedInThisSection = false;
    }
    public void ClearPreviusData()
    {
        for (int i = 0; i < displays.Count; i++) Destroy(displays[i].gameObject);
        displays.Clear();
    }
    public void CalculateEvaluation()
    {
        ClearPreviusData();

        for (int i = 0; i < DataManager.main.charactersOnScript.Count; i++)
        {
            float currentPotins = 0;
            string character = DataManager.main.charactersOnScript[i];
            Sprite pic = DataManager.main.GetCharacterPic(character);

            for (int j = 0; j < QuestionManager.main.Questions.Length; j++)
            {
                tags.Clear();
                Question currenQuestion = QuestionManager.main.Questions[j];
                string answer = DataManager.main.GetAnswerByCharacter(character, j);

                tags.Add(answer == currenQuestion.CorrectAnswer.text ? "Users Right" : "Users Wrong");
                tags.Add(character);



                currentPotins += currenQuestion.GetScoreForCharacter(tags);
            }
            print(tags.Count);
            foreach (string item in tags)
            {
                print(item);
            }

            CharacterEvaluationDisplay current = Instantiate(evaluationDisplayPrefab, reusltsParent);
            current.SetUp(pic, character, $"Points: {currentPotins}\n\nExp: 100", "", this);
            current.score = Mathf.RoundToInt(currentPotins);
            current.UpdateMVP();
            displays.Add(current);
            SortCharactersByScore();
            if (hasVotedInThisSection) DisableAllLikes(true);
            SortCharactersByScore();
        }
    }
    public void SortCharactersByScore()
    {
        Debug.LogError("sorting");
        displays = displays.OrderBy(go => go.score).ToList();
        Transform parentTransform = reusltsParent.transform;
        int numberChildren = parentTransform.childCount;



        for (int i = 0; i < displays.Count; i++)
        {
            for (int j = 0; j < numberChildren; j++)
            {
                if (parentTransform.GetChild(j).GetComponent<CharacterEvaluationDisplay>().characterNameString.Equals(displays[i].characterNameString))
                {
                    Debug.LogError("i: " + i + "j: " + j);
                        //parentTransform.GetChild(j).GetComponent<CharacterEvaluationDisplay>().AssignMVPState(false);
                                        //if (j == 0)
                    //{
                    //    parentTransform.GetChild(j).GetComponent<CharacterEvaluationDisplay>().AssignMVPState(true);
                    //}
                    //else
                    //{
                    //}
                    parentTransform.GetChild(j).SetSiblingIndex(i);
                }
            }
        }
        int maxScore = 0;
        foreach (var item in QuestionManager.main.Questions)
        {
            maxScore += item.Reward;
        }
        for (int i = 0; i < displays.Count; i++)
        {
            parentTransform.GetChild(i).GetComponent<CharacterEvaluationDisplay>().AssignMVPState(false);
            Debug.LogError(displays[i].characterNameString + "-" + displays[i].score + "-");
            if ((displays[i].score*100)/maxScore >= 80)
                parentTransform.GetChild(i).GetComponentInChildren<RankText>().GetComponent<Image>().sprite = valorationSprites[0];
            else if ((displays[i].score * 100) / maxScore >=50)
                parentTransform.GetChild(i).GetComponentInChildren<RankText>().GetComponent<Image>().sprite = valorationSprites[1];
            else if ((displays[i].score * 100) / maxScore < 50)
                parentTransform.GetChild(i).GetComponentInChildren<RankText>().GetComponent<Image>().sprite = valorationSprites[2];
        }
        Debug.LogError("sorted, max points possible: "+maxScore);
        List<CharacterEvaluationDisplay> matchedData = displays.Where(x => x.score == displays.Max(result => result.score)).ToList();
        foreach (var item in displays)
        {
            item.GetComponent<CharacterEvaluationDisplay>().AssignMVPState(false);
        }
        if (matchedData.Count > 0)
        {
            matchedData[0].GetComponent<CharacterEvaluationDisplay>().AssignMVPState(true);
        }
        Debug.LogError("MVP assigned to: "+ matchedData[0].GetComponent<CharacterEvaluationDisplay>().characterNameString);
    }
    public AnswerResutl GetAccusedByName(string name)
    {
        for (int i = 0; i < DataManager.main.Results[ushort.MinValue].Answers.Length; i++)
        {
            if (DataManager.main.Results[ushort.MinValue].Answers[i].Answer == name)
            {
                return DataManager.main.Results[ushort.MinValue].Answers[i];
            }
        }

        return null;
    }
    public void DisableAllLikes(bool calculateMVP = false)
    {
        hasVotedInThisSection = true;
        for (int i = 0; i < displays.Count; i++)
        {
            displays[i].VoteButton.interactable = false;
            if (calculateMVP) displays[i].UpdateMVP();
        }
    }
}
