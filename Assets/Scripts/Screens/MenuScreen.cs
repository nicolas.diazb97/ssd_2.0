public class MenuScreen : ScreenTemplate
{
    public override void Init()
    {
        base.Init();

    }

    public void OpenHome()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.Home);
    }
    public void OpenScriptCollection()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.ScriptCollection);
    }
    public void OpenProfile()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.Profile);
    }
}