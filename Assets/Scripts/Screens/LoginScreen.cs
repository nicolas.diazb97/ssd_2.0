using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginScreen : ScreenTemplate
{
    //AuxVariables

    bool FirstTime;

    [SerializeField] private Text versionText;
    public override void Init()
    {
        base.Init();
        SetVersionName();

        if (FirstTime)
        {
            Util.utilInstance.ActiveButton("Facebook"); // Por qué el util se encarga de activar y desactivar objetos propios de esta pantalla?
            Util.utilInstance.ActiveButton("Google");
        }
        FirstTime = true;


    }

    private void SetVersionName()
    {
        versionText.text = $"v{Application.version}";
    }

    public void OpenTermsConditions()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.TermsConditions);
    }
    public void OpenPolicyPrivacy()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.PolicyPrivacy);
    }

}
