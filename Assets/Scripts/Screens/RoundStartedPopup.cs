﻿using Managers;
using TMPro;
using UnityEngine;

namespace Screens
{
    public class RoundStartedPopup : ScreenTemplate
    {
        [SerializeField] private TMP_Text txtRoundName;
        [SerializeField] private TMP_Text txtRoundDesc;

        public void SetInfo(RoundConfig config)
        {
            txtRoundName.text = RoundConfig.RoundEnumToString(config.RoundName);
            txtRoundDesc.text = config.RoundDescription;
        }
    }
}