﻿using System.Collections.Generic;
using System.Linq;
using Data.Scripts;
using Managers;
using Photon.Pun;
using Photon.Realtime;
using Screens.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Utils.ScreensUtils;

namespace Screens
{
    public class RoomScreen : ScreenTemplate
    {
        [SerializeField] private TMP_Text txtRoomId;
        [SerializeField] private TMP_Text txtScriptTitle;
        [SerializeField] private TMP_Text txtPlayers;
        [SerializeField] private Transform charactersAssigningContainer;
        [SerializeField] private AssignableCharacter assignableCharacterPrefab;
        [SerializeField] private StateButton btnSetReadyState;
        [SerializeField] private Color readyButtonColor = Color.white;
        [SerializeField] private Color notReadyButtonColor = Color.gray;
        [SerializeField] private Image imgIsReady;
        [FormerlySerializedAs("btnLeave")] [SerializeField] private LeavePrivateRoomButton btnLeavePrivate;

        private GameScript _currentRoomScript;
        private AssignableCharacter[] _assignableCharacters;

        public override void Init()
        {
            base.Init();
            PunManager.Instance.OnEnterRoom += OnEnterRoom;
            btnSetReadyState.OnStateToggled += SetReadyState;
            ToggleReadyVisuals(false);

            var roomId = PunManager.Instance.CurrentRoomId;
            txtRoomId.text = $"Room ID: {roomId}";
            
            txtScriptTitle.text = (_currentRoomScript = PunManager.Instance.CurrentRoomScript).Title;
            txtPlayers.text = $"Choose a character (0/{PunManager.Instance.MaxRoomPlayers})";
            SetAssignableCharacters();
            MusicManager.main.PlayMusic(false);
        }

        public override void Close()
        {
            base.Close();
            PunManager.Instance.OnEnterRoom -= OnEnterRoom;
            btnSetReadyState.OnStateToggled -= SetReadyState;
            _assignableCharacters = null;

            foreach (Transform child in charactersAssigningContainer)
            {
                Destroy(child.gameObject);
            }
        }

        public void OnBackButtonPressed()
        {
            btnLeavePrivate.OnClickLeave();
        }

        private void SetAssignableCharacters()
        {
            var spawnedCharacters = new List<AssignableCharacter>();

            foreach (var character in _currentRoomScript.Characters)
            {
                var assignableCharacter = Instantiate(assignableCharacterPrefab, charactersAssigningContainer);
                assignableCharacter.Init(OnPlayerAssigned, character);
                spawnedCharacters.Add(assignableCharacter);
            }

            _assignableCharacters = spawnedCharacters.ToArray();
        }

        private void OnPlayerAssigned()
        {
            if (_assignableCharacters == null) return;

            txtPlayers.text = $"Choose a character ({_assignableCharacters.Count(character => character.IsAssigned)}/{PunManager.Instance.MaxRoomPlayers})";

            // if (bool.Parse(PhotonNetwork.LocalPlayer.CustomProperties["Ready"].ToString()))
            // {
            //     foreach (var assignableCharacter in _assignableCharacters)
            //     {
            //         assignableCharacter.SetInteractable(false);
            //     }
            // }

            var playerList = PhotonNetwork.PlayerList;
            if (playerList.Length >= PhotonNetwork.CurrentRoom.MaxPlayers && playerList.All(player => bool.Parse(player.CustomProperties["Ready"].ToString())))
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    PhotonNetwork.CurrentRoom.IsVisible = false;
                    PhotonNetwork.CurrentRoom.IsOpen = false;
                }
                ScreenManager.main.OpenScreen(SerializableScreen.Type.GameScreen);
                PunManager.Instance.NotifyNewRoundStarted();
              
            }
        }

        private void SetReadyState(bool isReady)
        {
            PunManager.Instance.ChangeReadyStatus(isReady);

            // if (_assignableCharacters == null) return;
            // foreach (var character in _assignableCharacters)
            // {
            //     character.SetInteractable(!readyState);
            // }
        }

        private void OnEnterRoom()
        {
            var assigned = _assignableCharacters.FirstOrDefault(character => !character.IsAssigned);
            if (!assigned) return;

            assigned.AssignPlayerToCharacter();
        }

        public void ToggleReadyVisuals(bool isReady)
        {
            btnSetReadyState.GetComponentInChildren<TMP_Text>().text = isReady ? "Not Ready" : "Ready";
            imgIsReady.color = isReady ? readyButtonColor : notReadyButtonColor;
        }
    }
}