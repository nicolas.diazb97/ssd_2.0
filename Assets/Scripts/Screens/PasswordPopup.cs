﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Screens
{
    public class PasswordPopup : ScreenTemplate
    {
        [SerializeField] private Button btnJoinRoom;
        [SerializeField] private UnityEvent onPasswordWrong;
        [SerializeField] private TMP_InputField passwdInputField;

        private Action _onPasswordMatchEvt;
        private string _targetPassword;
        private string _passwordInput;

        public string PasswordInput
        {
            get => _passwordInput;
            set
            {
                btnJoinRoom.interactable = value.Length == passwdInputField.characterLimit;
                _passwordInput = value;
            }
        }

        public void SetupInfo(string password, Action cbOnPasswordMatch)
        {
            _onPasswordMatchEvt = cbOnPasswordMatch;
            _targetPassword = password;
            passwdInputField.text = string.Empty;
            btnJoinRoom.interactable = false;
        }

        public override void Init()
        {
            base.Init();
            btnJoinRoom.onClick.AddListener(OnClickJoin);
        }

        public override void Close()
        {
            base.Close();
            btnJoinRoom.onClick.RemoveListener(OnClickJoin);
            passwdInputField.text = string.Empty;
        }

        private void OnClickJoin()
        {
            if (PasswordInput == _targetPassword)
            {
                ScreenManager.main.CloseScreen();
                _onPasswordMatchEvt?.Invoke();
            }
            else
            {
                onPasswordWrong?.Invoke();
            }
        }
    }
}