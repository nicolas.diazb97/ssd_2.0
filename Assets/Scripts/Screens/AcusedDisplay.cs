﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Managers;

public class AcusedDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI nameDisplay;
    [SerializeField] private Image photo;
    [SerializeField] private Image killerState;
    [SerializeField] private Transform voters;
    private string answer;
    private int index;

    public List<Vote> Votes { get; set; } = new List<Vote>();
    public string GetName => nameDisplay.text;
    public void SetName(string value) => answer = nameDisplay.text = value;
    public void SetPhoto(Sprite value)
    {
        if (value == null)
            photo.GetComponentInParent<Mask>().transform.parent.gameObject.SetActive(false);
        photo.sprite = value;
    }
    public Transform VotersParent => voters;


    public void SetIndex(int value) => index = value;

    public void SetAnswerStatus()
    {
        Question question = QuestionManager.main.Questions[index];
        Answer correctAnswer = question.CorrectAnswer;

        Answer currentAnswer = question.GetAnserByName(answer);
        if (currentAnswer.HasTags)
        {
            bool wellAnswered = DataManager.main.GetMostVotedAnswer(QuestionManager.main.Questions[index].QuestionName).Answer == correctAnswer.text;
            print(answer);
            killerState.gameObject.SetActive(true);
            killerState.sprite = wellAnswered ? currentAnswer.CorrectSprite : currentAnswer.IncorrectSprite;
        }
    }
}
