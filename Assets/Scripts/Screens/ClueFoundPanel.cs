﻿using Data;
using Managers;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Screens
{
    public class ClueFoundPanel : ScreenTemplate
    {
        [SerializeField] private TMP_Text txtClueTitle;
        [SerializeField] private TMP_Text txtClueDesc;
        [SerializeField] private Image imgClueThumbnail;
        [FormerlySerializedAs("btnBackToSearchPanel")] [SerializeField] private Button btnFurtherResearch;

        private Clue _nestedClue;

        public override void Init()
        {
            base.Init();
            btnFurtherResearch.onClick.AddListener(FurtherResearch);
        }

        public override void Close()
        {
            base.Close();
            btnFurtherResearch.onClick.RemoveListener(FurtherResearch);
            CameraManager.main.ToggleMovementActive(true);
        }

        private void FurtherResearch()
        {
            Debug.LogError("further research pressed");
            Setup(_nestedClue);
            PunManager.Instance.NotifyClueFoundByPlayer(_nestedClue.Title, _nestedClue.ClueID);
        }

        public void Setup(Clue foundClue, bool showFurtherButton = true)
        {
            Debug.LogError("setup bool " + showFurtherButton + foundClue.Title);
            btnFurtherResearch.interactable = foundClue;
            if (!foundClue)
            {
                txtClueTitle.text = "No clues left";
                txtClueDesc.text = "There are no more clues left in this place";
                return;
            }

            btnFurtherResearch.gameObject.SetActive(foundClue.NestedClue.Count > 0);
            btnFurtherResearch.interactable = showFurtherButton;
            Debug.LogError(foundClue.Title + " numero de nested clues in here: " + foundClue.NestedClue.Count);
            if (foundClue.NestedClue.Count > 0)
            {
                int currNestedClueIndex = 0;
                Clue tempNestedClue = null;
                for (int i = 0; i < foundClue.NestedClue.Count; i++)
                {
                    //Debug.LogError("found in: " + fclue.FoundIn.PlaceName);
                    List<Clue> matchedClues = DataManager.main._foundClues.Where(f => f.Title.Equals(foundClue.NestedClue[i].Title)).ToList();
                    if (matchedClues.Count <= 0 && tempNestedClue == null)
                    {
                        currNestedClueIndex = i;
                        break;
                    }
                }
                Debug.LogError("current nested clue ID: " + currNestedClueIndex);
                _nestedClue = foundClue.NestedClue[currNestedClueIndex];
                Debug.Log($"Nested clue found: {foundClue.NestedClue[0].Title}");
                Debug.Log($"Nested clue found: {_nestedClue.Title}");
                _nestedClue.FoundBy = foundClue.FoundBy;
                _nestedClue.FoundIn = foundClue.FoundIn;
            }

            txtClueTitle.text = foundClue.Title;
            txtClueDesc.text = foundClue.Description;
            imgClueThumbnail.sprite = foundClue.Thumbnail;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ScreenManager.main.CloseScreen();
            }
        }
    }
}