﻿using Photon.Realtime;
using TMPro;
using UnityEngine;

namespace Screens
{
    public class GameChat : ScreenTemplate
    {
        [SerializeField] private Transform messagesContainer;
        [SerializeField] private TMP_Text messagePrefab;

        private void Awake()
        {
            PunManager.Instance.OnPlayerJoined += OnPlayerJoinedRoom;
            PunManager.Instance.OnPlayerLeft += OnPlayerLeftRoom;
            PunManager.Instance.OnLeaveRoom += ResetChat;
            PunManager.Instance.OnEnterRoom += OnEnterRoom;
        }

        private void OnDestroy()
        {
            PunManager.Instance.OnPlayerJoined -= OnPlayerJoinedRoom;
            PunManager.Instance.OnPlayerLeft -= OnPlayerLeftRoom;
            PunManager.Instance.OnLeaveRoom -= ResetChat;
            PunManager.Instance.OnEnterRoom -= OnEnterRoom;
        }

        private void OnEnterRoom()
        {
            LogMessage($"<color=yellow>{PunManager.Instance.LocalPlayerNickname}</color> joined the room.");
        }

        private void OnPlayerLeftRoom(Player leftPlayer)
        {
            LogMessage($"<color=yellow>{leftPlayer.CustomProperties["Nick"]}</color> left the room.");
        }

        private void OnPlayerJoinedRoom(Player joinedPlayer)
        {
            LogMessage($"<color=yellow>{joinedPlayer.CustomProperties["Nick"]}</color> joined the room.");
        }

        private void LogMessage(string message)
        {
            var messageInstance = Instantiate(messagePrefab, messagesContainer);
            messageInstance.text = message;
        }
        public void ResetChat()
        {
            foreach (Transform transform in messagesContainer)
            {
                Destroy(transform.gameObject);
            }
        }
    }
}