﻿using UnityEngine;
using UnityEngine.UI;

public class VoteDisplay : MonoBehaviour
{
    [SerializeField] private Image photo;
    public void SetPhoto(Sprite value) => photo.sprite = value;
}
