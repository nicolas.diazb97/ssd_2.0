public class WhatIsScreen : ScreenTemplate
{
    public override void Init()
    {
        base.Init();

    }

    public void OpenHelpPage()
    {
        ScreenManager.main.OpenScreen(SerializableScreen.Type.HelpPage);
    }
}
