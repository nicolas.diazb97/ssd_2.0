using Newtonsoft.Json.Linq;
using TMPro;
using UnityEngine;

namespace Languages
{
    [RequireComponent(typeof(TMP_Text)), DisallowMultipleComponent, ExecuteAlways]
    public class Translator : MonoBehaviour
    {
        [SerializeField] private string key;
        private Color _notNullColor;
    
        private void Start()
        {
            if (Application.isPlaying) OnLanguageChange();
        }

        public void OnLanguageChange(JObject languageDic = null)
        {
            if (string.IsNullOrEmpty(key)) return;

            if (languageDic != null)
            {
                ProcessTokenValue(languageDic.TryGetValue(key, out var value) ? value.Value<string>() : string.Empty);
            }
            else
            {
                ProcessTokenValue(LanguageController.main.GetValue(key));
            }
        }

        private void ProcessTokenValue(string value = null)
        {
            var textMesh = GetComponent<TMP_Text>();
            var isNullOrEmpty = string.IsNullOrEmpty(value);
            if (isNullOrEmpty && Application.isPlaying) Debug.LogError($"Missing translator value for TMP {name}");
            if (textMesh.color != Color.red) _notNullColor = textMesh.color;

            textMesh.color = !isNullOrEmpty ? (_notNullColor != new Color() ? _notNullColor : textMesh.color) : Color.red;
            textMesh.text = !isNullOrEmpty ? value : "Missing value";
        }

        private void OnValidate()
        {
            var languageController = FindObjectOfType<LanguageController>();
            if (languageController) languageController.RefreshTranslator(this);
        }
    }
}