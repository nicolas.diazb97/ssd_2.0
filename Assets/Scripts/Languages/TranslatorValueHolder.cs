﻿using UnityEngine;

namespace Languages
{
    public class TranslatorValueHolder : MonoBehaviour
    {
        [SerializeField] private string key;

        public string Value
        {
            get
            {
                var languageController = LanguageController.main;
                if (languageController)
                {
                    return languageController.GetValue(key);
                }
                
                Debug.LogError($"Translator {name} is unable to get its value. A Language Controller instance is required in the current scene.");
                return string.Empty;
            }
        }
    }
}