using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Languages
{
    public class LanguageController : Singleton<LanguageController>
    {
        [SerializeField] private TextAsset languagesAsset;
        [SerializeField] private SystemLanguage currentLang = SystemLanguage.English;

        private JObject _langToken;
        private const string LangKey = "Lang";

        public override void Awake()
        {
            base.Awake();
            _langToken = JObject.Parse(languagesAsset.text);
            
            // if (Application.isEditor) return;
            // if (PlayerPrefs.HasKey(LangKey))
            // {
            //     if (Enum.TryParse(PlayerPrefs.GetString(LangKey), out SystemLanguage value))
            //     {
            //         currentLang = value;
            //     }
            // }
            // else
            // {
            //     currentLang = Application.systemLanguage;
            // }
        }

        private void OnValidate()
        {
            foreach (var translator in FindObjectsOfType<Translator>())
            {
                RefreshTranslator(translator);
            }
        }

        public void RefreshTranslator(Translator translator)
        {
            translator.OnLanguageChange(JObject.Parse(languagesAsset.text)[currentLang.ToString()]?.Value<JObject>());
        }

        public string GetValue(string key)
        {
            if (_langToken == null)
            {
                Debug.LogError($"Unable to read values from {name} since the language token has not been initialized.");
                return null;
            }
            
            var currentLangToken = _langToken[currentLang.ToString()]?.Value<JObject>();
            if (currentLangToken == null) return null;
            
            if (currentLangToken.TryGetValue(key, out var value))
            {
                return value.Value<string>();
            }
            
            Debug.LogError($"No value was found in {languagesAsset.name} for key {key}");
            return null;
        }

        public void SetLanguage(SystemLanguage newLang)
        {
            var stringNewLang = newLang.ToString();
            if (_langToken.ContainsKey(stringNewLang))
            {
                currentLang = newLang;
                PlayerPrefs.SetString(LangKey, stringNewLang);
            }
            else
            {
                Debug.LogError($"Attempted to set the game language to {newLang}, but it has no key assigned in {languagesAsset.name}");
            }
        }
    }
}