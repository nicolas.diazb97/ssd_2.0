using System;
using System.Collections.Generic;
using UnityEngine;

public class FilterManager : MonoBehaviour
{
    [SerializeField] private bool allowMultipleActiveFilters = true;
    
    private readonly List<SearchFilter> _activeFilters = new List<SearchFilter>();

    // BEGINNING DEV AREA

    private readonly Dictionary<String,SearchFilter> _activeCategories = new Dictionary<String, SearchFilter>();


    // END DEV AREA


    public event Action<SearchFilter[]> OnActiveFiltersUpdated;

    public void AddActiveFilter(SearchFilter filterValue, string filterCategory) //Mod
    {
        Debug.Log("Here!");


        if (!allowMultipleActiveFilters) _activeFilters.Clear();


        if (!_activeCategories.ContainsKey(filterCategory))
        {
            Debug.Log("value: " + filterValue);
            _activeCategories.Add(filterCategory,filterValue); //Added
            _activeFilters.Add(filterValue);
        }
        else if (_activeCategories.ContainsKey(filterCategory))
        {
            _activeFilters.Remove(_activeCategories[filterCategory]); //Added);
            Debug.Log("value: " + filterValue);
            _activeCategories[filterCategory] = filterValue;    //Added
            _activeFilters.Add(filterValue);
        }





        OnActiveFiltersUpdated?.Invoke(_activeFilters.ToArray());
    }

    public void RemoveActiveFilter(SearchFilter filterValue)
    {
        _activeFilters.Remove(filterValue);
        OnActiveFiltersUpdated?.Invoke(_activeFilters.ToArray());
    }

    public void DisableAllFilters()
    {
        OnActiveFiltersUpdated?.Invoke(Array.Empty<SearchFilter>());
    }

    public void NotifyActiveFilters()
    {
        OnActiveFiltersUpdated?.Invoke(_activeFilters.ToArray());
    }

    private void OnDisable()
    {
        _activeFilters.Clear();
    }
}
