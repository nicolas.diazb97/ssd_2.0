using System;
using UnityEngine;

public class PanelUI : MonoBehaviour
{
    public static event Action<bool> enableMapMovement;
    public static event Action<bool> enableMapClick;
    public void DeactivePanel()
    {
        gameObject.transform.parent.gameObject.SetActive(false);
        enableMapMovement?.Invoke(true);
        enableMapClick.Invoke(true);
    }

    public void ActivePanel()
    {
        gameObject.transform.parent.gameObject.SetActive(true);
        enableMapMovement?.Invoke(false);
        enableMapClick.Invoke(false);
    }
}
