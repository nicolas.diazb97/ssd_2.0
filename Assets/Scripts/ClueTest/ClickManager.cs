using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DigitalRubyShared;
using Cinemachine;


public class ClickManager : MonoBehaviour
{
    /// <summary>
    /// Librería de gestos de Digital Ruby Shared
    /// </summary>
    /// <param name="tapGesture">Se reconoce este gesto al hacer tap en la pantalla</param>
    /// <param name="panGesture">Se reconoce este gesto al arrastrar el dedo por la pantalla</param>
    /// <param name="longPressGesture">Se reconoce al mantener presionado por mas de 0.1s</param>
    private TapGestureRecognizer tapGesture;
    private PanGestureRecognizer panGesture;
    private LongPressGestureRecognizer longPressGesture;

    [Range(0.0f, 1.0f)]
    [Tooltip("The min speed before re-enabling threshold units on the pan gesture")]
    public float minimumSpeedBeforeThresholdUnitsIsReEnabled;

    [Range(0.0f, 1.0f)]
    [Tooltip("The min time before re-enabling threshold units on the pan gesture")]
    public float minimumTimeBeforeThresholdUnitsIsEnabled;

    /// <summary>
    /// En el momento en el que se termina un tap se lanza un rayo al sitio 
    /// donde se tapeo por ultima vez para determinar si hay una pista en ese sitio o no
    /// </summary>
    /// <param name="gesture"></param>
    private void Tap_Updated(GestureRecognizer gesture)
    {
        if (gesture.State == GestureRecognizerState.Ended)
        {
            List<GestureTouch> touches = FingersScript.Instance.CurrentTouches.ToList();
            Vector2 endTouchPosition = new Vector2(touches[0].ScreenX, touches[0].ScreenY);
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(endTouchPosition);
            RaycastHit2D hit = Physics2D.Raycast(worldPosition, Vector2.zero);
            if (hit)
            {
                if (hit.collider.CompareTag("Clue"))
                {
                    hit.transform.GetComponent<ClickeableClue>().EnableUI();
                }
            }
        }
    }
    /// <summary>
    /// Calcula la posicion actual del dedo en la pantalla cada segundo para mover el mapa
    /// de acuerdo a esta posicion
    /// </summary>
    /// <param name="gesture"></param>
    private void Pan_Updated(GestureRecognizer gesture)
    {
        if (gesture.CurrentTrackedTouches.Count != 0)
        {
            GestureTouch touch = gesture.CurrentTrackedTouches[0];
            Vector3 pos = new Vector3(touch.X, touch.Y, -10);
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(pos);
            RaycastHit2D hit = Physics2D.Raycast(worldPosition, Vector2.zero, LayerMask.GetMask("Map"));
            if (hit) 
            {
                if (gesture.State == GestureRecognizerState.Possible)
                {
                    CameraManager.main.SetOrigin(pos);
                }
                if (hit.collider.CompareTag("Map"))
                {
                    CameraManager.main.MoveCamera(pos);
                }
            }         
        }
    }

    /// <summary>
    /// Activa y desactiva el damping de la camara al realizar el movimiento del mapa para evitar shaking
    /// </summary>
    /// <param name="gesture"></param>
    private void LongPressGestureCallback(GestureRecognizer gesture)
    {
        if (gesture.State == GestureRecognizerState.Began)
        {
            CameraManager.main.virtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_XDamping = 0;
        }
        else if (gesture.State == GestureRecognizerState.Ended)
        {
            CameraManager.main.virtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_XDamping = 1;
        }
    }

    /// <summary>
    /// Se inicializa el gesto LongPress
    /// </summary>
    private void CreateLongPressGesture()
    {
        longPressGesture = new LongPressGestureRecognizer();
        longPressGesture.MaximumNumberOfTouchesToTrack = 1;
        longPressGesture.AllowSimultaneousExecutionWithAllGestures();
        longPressGesture.MinimumDurationSeconds = 0.1f;
        longPressGesture.StateUpdated += LongPressGestureCallback;
        FingersScript.Instance.AddGesture(longPressGesture);
    }


    /// <summary>
    /// Se inicializan los gestos de tap y movimiento
    /// </summary>
    private void Start()
    {
        tapGesture = new TapGestureRecognizer { MaximumNumberOfTouchesToTrack = 10 };
        tapGesture.StateUpdated += Tap_Updated;
        FingersScript.Instance.AddGesture(tapGesture);

        panGesture = new PanGestureRecognizer();
        panGesture.StateUpdated += Pan_Updated;
        panGesture.MaximumNumberOfTouchesToTrack = 2;
        FingersScript.Instance.AddGesture(panGesture);

        panGesture.SpeedUnitsToRestartThresholdUnits = minimumSpeedBeforeThresholdUnitsIsReEnabled;
        panGesture.TimeToRestartThresholdUnits = minimumTimeBeforeThresholdUnitsIsEnabled;

        tapGesture.ClearTrackedTouchesOnEndOrFail = true;
        tapGesture.AllowSimultaneousExecution(panGesture);

        CreateLongPressGesture();
    }
}
