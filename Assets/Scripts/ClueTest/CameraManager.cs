using System;
using Managers;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class CameraManager : Singleton<CameraManager>
{
    [Header("Map")]
    [SerializeField]
    private SpriteRenderer mapRenderer;
    [SerializeField]
    private Transform player;
    public CinemachineVirtualCamera virtualCamera;
    public CinemachineConfiner confiner;
    [SerializeField]
    private Vector3 origin;

    [Header("Zoom options")]
    [SerializeField]
    private float zoomStep = 1;
    [SerializeField]
    private float minZoom = 5;
    [SerializeField]
    private float maxZoom = 15;
    [SerializeField] private Image gameBackground;

    private float mapMinX;
    private float mapMaxX;
    private float mapMinY;
    private float mapMaxY;

    private bool enableMovement = true;

    public event Action<bool> OnMovementToggled;

    public SpriteRenderer MapRenderer
    {
        get => mapRenderer;
        set
        {
            //  if (GameManager.main.HasGameStarted) return;

            var cameraPos = player.transform.position;
            if (mapRenderer) Destroy(mapRenderer.gameObject);
            
            mapRenderer = Instantiate(value, new Vector2(cameraPos.x, cameraPos.y), Quaternion.identity);
            mapRenderer.transform.SetParent(transform);
            gameBackground.gameObject.SetActive(false);
            SetBounds();
        }
    }

    /// <summary>
    /// Asigna el valor de origen a la camara de acuerdo a la posicion del tap
    /// </summary>
    /// <param name="pos">Posicion del tap</param>
    public void SetOrigin(Vector3 pos)
    {
        origin = transform.Find("Main Camera").GetComponent<Camera>().ScreenToWorldPoint(pos);
    }

    /// <summary>
    /// Se calcula la distancia del origen a la posicion deseada para el movimiento 
    /// </summary>
    /// <param name="pos">Posicion del cursor</param>
    public void MoveCamera(Vector3 pos)
    {
        if (player.transform.position == transform.Find("Main Camera").position) return;
        Vector3 distance = origin - transform.Find("Main Camera").GetComponent<Camera>().ScreenToWorldPoint(pos);
        player.transform.position = ClampCamera(player.transform.position + distance);
    }

    /// <summary>
    /// Realiza el zoom modificando el tamaño ortografico de la camara
    /// </summary>
    public void ZoomIn()
    {
        float newSize = transform.Find("Main Camera").GetComponent<Camera>().orthographicSize - zoomStep;
        transform.Find("Main Camera").GetComponent<Camera>().orthographicSize = Mathf.Clamp(newSize, minZoom, maxZoom);
        player.transform.position = ClampCamera(player.transform.position);
    }

    /// <summary>
    /// Realiza el zoom modificando el tamaño ortografico de la camara
    /// </summary>
    public void ZoomOut()
    {
        float newSize = transform.Find("Main Camera").GetComponent<Camera>().orthographicSize + zoomStep;
        transform.Find("Main Camera").GetComponent<Camera>().orthographicSize = Mathf.Clamp(newSize, minZoom, maxZoom);
        player.transform.position = ClampCamera(player.transform.position);
    }

    /// <summary>
    /// Mantiene el movimiento de la camara dentro del tamaño del background/mapa
    /// </summary>
    /// <param name="targetPosition">Posicion deseada</param>
    /// <returns></returns>
    private Vector3 ClampCamera(Vector3 targetPosition)
    {
        float camHeight = transform.Find("Main Camera").GetComponent<Camera>().orthographicSize;
        float camWidth = transform.Find("Main Camera").GetComponent<Camera>().orthographicSize * transform.Find("Main Camera").GetComponent<Camera>().aspect;

        float minX = mapMinX + camWidth;
        float maxX = mapMaxX - camWidth;
        float minY = mapMinY + camHeight;
        float maxY = mapMaxY - camHeight;

        float newX = Mathf.Clamp(targetPosition.x, minX, maxX);
        float newY = Mathf.Clamp(targetPosition.y, minY, maxY);

        return new Vector3(newX, newY, -10);
    }

    private void EnableMapMovement(bool value) => enableMovement = value;
    public override void Awake()
    {
        base.Awake();
        PunManager.Instance.OnLeaveRoom += ResetMap;
        //player = Camera.main;
        if (!mapRenderer) return;

        SetBounds();
    }

    private void Start()
    {
        Input.simulateMouseWithTouches = true;
    }

    public override void OnDestroy()
    {
        PunManager.Instance.OnLeaveRoom -= ResetMap;
        base.OnDestroy();
    }

    /// <summary>
    /// Reinicia el mapa al iniciar la partida
    /// </summary>
    private void ResetMap()
    {
        // if (Application.isPlaying) return;
        gameBackground.gameObject.SetActive(true);
        if (!mapRenderer) return;

        Destroy(mapRenderer.gameObject);
    }

    /// <summary>
    /// Asigna los limites mapa de acuerdo a su tamaño
    /// </summary>
    private void SetBounds()
    {
        mapMinX = mapRenderer.transform.position.x - (mapRenderer.bounds.size.x / 2);
        mapMaxX = mapRenderer.transform.position.x + (mapRenderer.bounds.size.x / 2);
        mapMinY = mapRenderer.transform.position.y - mapRenderer.bounds.size.y / 2;
        mapMaxY = mapRenderer.transform.position.y + mapRenderer.bounds.size.y / 2;
    }

    /// <summary>
    /// Activa o desactiva el movimiento del mapa
    /// </summary>
    /// <param name="isEnabled"></param>
    public void ToggleMovementActive(bool isEnabled)
    {
        OnMovementToggled?.Invoke(enableMovement = isEnabled);
    }

    void Update()
    {
        /*if (enableMovement)
        {
            MoveCamera();
        }*/
    }

}
