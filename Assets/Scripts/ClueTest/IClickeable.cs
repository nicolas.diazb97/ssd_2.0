
public interface IClickeable
{
    void OnClick();
}
