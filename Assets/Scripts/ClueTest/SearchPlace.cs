using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Data;
using Data.Scripts;
using Managers;
using Photon.Pun;

[RequireComponent(typeof(ClickeableClue))]
public class SearchPlace : MonoBehaviour
{
    [SerializeField] private RoundSetup[] setupsPerRound;
    [SerializeField] private string placeName;
    [SerializeField] private Sprite image;

    [Header("Private Voice Room Config")]
    [SerializeField] private string vcSuffix;
    [SerializeField] private SpriteRenderer roomMapPrefab;

    private readonly Queue<Clue> _findableClues = new Queue<Clue>();
    private int _currentRoundIndex;
    private ClickeableClue _clickeable;
    private int _cluesRemaining;
    private GameScript.Character _searchCharacter;
    private List<string> currNestedClues = new List<string>();
    private Clue currClue;
    private int remainingNestedClues = 0;

    public string PlaceName => placeName;
    public Sprite Image => image;

    private void Awake()
    {
        _clickeable = GetComponent<ClickeableClue>();
    }

    public void EnableUI()
    {
        Debug.Log("search place clues remaining faltan" + _cluesRemaining);
        if (_searchCharacter == null)
        {
            var searchPanel = ScreenManager.main.OpenScreen<SearchPlacePanel>(false);
            Debug.LogError("call for setup");
            searchPanel.Setup(this, SetCurrentClueFound, _cluesRemaining, vcSuffix, roomMapPrefab);
        }
        else
        {
            if (ScreenManager.main.OpenScreen(SerializableScreen.Type.CharacterSearchPlace, false) is SearchPlacePanel panelInstance)
            {
                panelInstance.Setup(this, SetCurrentClueFound, _cluesRemaining, vcSuffix, roomMapPrefab, _searchCharacter);
            }
        }
    }

    private void HandleClueFound(string clueName, string playerId, string clueId)
    {
        bool isClueFromOtherPlayer = (!PhotonNetwork.LocalPlayer.NickName.Equals(playerId));
        Debug.LogError("handling clue found " + clueName);
        //if()
        var currentRoundSetup = setupsPerRound.FirstOrDefault(setup => GameManager.main.CurrentRound == setup.roundIndex);
        if (currentRoundSetup == null) return;
        var foundClue = currentRoundSetup.assignedClues.FirstOrDefault(clue => clue.Title == clueName);

        if (clueId != "")
        {
            foundClue = currentRoundSetup.assignedClues.FirstOrDefault(clue => clue.ClueID == clueId);
        }
        if (!foundClue)
        {
            var nestedParentClues = currentRoundSetup.assignedClues.Where(clue => clue.NestedClue.Count > 0).ToList();
            nestedParentClues.ForEach(n => Debug.Log(n.Title));
            List<Clue> matchedclues = new List<Clue>();
            for (int i = 0; i < nestedParentClues.Count; i++)
            {
                List<Clue> tempClues = nestedParentClues[i].NestedClue.Where(clue => clue.Title.Equals(clueName)).ToList();
                if (tempClues.Count > 0)
                    foundClue = tempClues[0];
            }
            Debug.LogError("nesteds clue que nmo esta en el mapa: " + matchedclues.Count);
            //++_cluesRemaining;

            if (!foundClue) return;
        }

        Debug.Log($"Clue found: {clueName} by {playerId}");

        //--_cluesRemaining;
        if (foundClue.NestedClue.Count > 0)
        {
            currNestedClues.Clear();
            foreach (var clues in foundClue.NestedClue)
            {
                currNestedClues.Add(clues.Title);
                Debug.Log("nest clue in here" + clues.Title);
            }
            //SetClueFound(foundClue.NestedClue, playerId);
        }
        foreach (var clues in foundClue.NestedClue)
        {
            if (!clues.Title.Equals(clueName))
            {
                //--_cluesRemaining;
                Debug.LogWarning("dicrease on for each");
            }
            else
            {
                //if (isClueFromOtherPlayer)
                //{
                //    _cluesRemaining++;
                //    Debug.LogWarning("incrkease in nestedclue");
                //}
                //else
                //{

                //    Debug.LogWarning("not incrkease in nestedclue");
                //}
            }
        }
        if (!PhotonNetwork.LocalPlayer.NickName.Equals(playerId))
        {
            if (_findableClues.Count != 0)
            {
                List<Clue> matchedNestdClues = currentRoundSetup.assignedClues.Where(ac => ac.Title.Equals(clueName)).ToList();
                if (matchedNestdClues.Count <= 0)
                {
                    Debug.LogWarning("es una nested clue");
                    ++_cluesRemaining;
                    currClue = _findableClues.Peek();
                }
                else
                {
                    currClue = _findableClues.Dequeue();
                }
                --_cluesRemaining;
                Debug.LogWarning("decrease on conditional");
            }
            Debug.LogWarning("dequeue by stranger?");
        }
        if (clueId == "")
            SetClueFound(foundClue, playerId);
    }

    private Clue SetCurrentClueFound()
    {
        Debug.LogWarning("setting current clue found");
        bool cycleBreaker = false;
        _findableClues.ToList().ForEach(fc =>
        {
            Debug.LogWarning(fc.Title);
        });
        if (_findableClues.Count < 1) return null;
        if (currClue != null)
        {
            remainingNestedClues = 0;
            int foundedClues = 0;
            Debug.LogError("nombre de pista: " + currClue.Title + " tiene " + currClue.NestedClue.Count + " pistas anidadas");
            if (currClue.NestedClue.Count <= 0)
            {
                currClue = _findableClues.Dequeue();
                PunManager.Instance.NotifyClueFoundByPlayer(currClue.Title, currClue.ClueID);
                _cluesRemaining = _findableClues.Count;
                Debug.LogError("no hay pistas anidada, pasa a la siguiente");
            }
            foreach (var item in currClue.NestedClue)
            {
                List<Clue> matchedClues = DataManager.main._foundClues.Where(f => f.Title.Equals(item.Title)).ToList();
                if (matchedClues.Count > 0)
                    foundedClues++;
            }
            remainingNestedClues = currClue.NestedClue.Count - foundedClues;
            Debug.LogError("faltan " + remainingNestedClues + "pistas anidadas");
            if (remainingNestedClues <= 0)
            {
                if (currClue.NestedClue.Count > 0)
                {
                    Debug.LogError("siguiente, no hay mas pistas anidadas");
                    foreach (var item in currClue.NestedClue)
                    {
                        //currClue = _findableClues.Dequeue();
                        //currClue = _findableClues.Dequeue();
                    }
                    currClue = _findableClues.Dequeue();
                    PunManager.Instance.NotifyClueFoundByPlayer(currClue.Title, currClue.ClueID);
                    _cluesRemaining = _findableClues.Count;
                    Debug.LogError("intentando regustrar pista entrante" + currClue.Title);
                }
                else
                {
                    Debug.LogError("siguiente, no hay mas pistas anidadas else");
                    _cluesRemaining = _findableClues.Count;
                }
            }
        }
        else
        {
            //currClue = _findableClues.Peek();
            currClue = _findableClues.Dequeue();
            PunManager.Instance.NotifyClueFoundByPlayer(currClue.Title, currClue.ClueID);
            Debug.LogError("es null " + currClue.Title);
            _cluesRemaining = _findableClues.Count;
        }
        Debug.LogError("termina condicionales");
        //if (currClue.Found) return SetCurrentClueFound();

        return currClue;
    }
    public void CheckForEndOfNestedClues(string clueName)
    {
        if (currClue.Title.Equals(clueName))
        {

            if (remainingNestedClues <= 0)
            {
                if (currClue.NestedClue.Count > 0)
                {
                    Debug.LogError("siguiente, no hay mas pistas anidadas");
                    foreach (var item in currClue.NestedClue)
                    {
                        //currClue = _findableClues.Dequeue();
                        //currClue = _findableClues.Dequeue();
                    }
                    currClue = _findableClues.Dequeue();
                }
                else
                {
                    Debug.LogError("siguiente, no hay mas pistas anidadas else");
                    _cluesRemaining = _findableClues.Count;
                }
            }
        }
    }

    private void SetClueFound(Clue clue, string foundBy)
    {
        Debug.LogError("clue found panel");
        clue.FoundIn = this;
        clue.FoundBy = foundBy;
        DataManager.main.AddFoundClue(clue);
        //CheckForEndOfNestedClues(clue.Title);
    }

    private void HandleRoundChanged(RoundConfig roundConfig)
    {
        if (!roundConfig) return;

        _clickeable.IsClickEnabled = roundConfig.IsSearchRound;
        _findableClues.Clear();
        var selectedSetup = setupsPerRound.FirstOrDefault(setup => setup.roundIndex == roundConfig.RoundName);
        if (selectedSetup == null) return;

        foreach (var clue in selectedSetup.assignedClues)
        {
            if (!DataManager.main.CluesFound.Contains(clue))
            {
                _findableClues.Enqueue(clue);
            }
        }

        _cluesRemaining = _findableClues.Count;
    }

    public void SetInfo(string newPlaceName, Sprite image, RoundSetup[] roundSetups, GameScript.Character character)
    {
        placeName = newPlaceName;
        this.image = image;
        setupsPerRound = roundSetups;
        _searchCharacter = character;
    }

    private void OnEnable()
    {
        PunManager.Instance.OnClueFoundByPlayer += HandleClueFound;
        GameManager.main.OnNewRoundStarted += HandleRoundChanged;
        HandleRoundChanged(GameManager.main.CurrentRoundConfig);

        /*  foreach (var setup in setupsPerRound)
        {
            foreach (var clue in setup.assignedClues)
            {
                if (!string.IsNullOrEmpty(clue.FoundBy))
                {
                    SetClueFound(clue, clue.FoundBy);
                }
            }
        }
        */
        var reconnectionInfo = PunManager.Instance.ReconnectionInfo;
        if (reconnectionInfo == null || !PunManager.Instance.ReconectRoom) return;

        CheckFoundClues(reconnectionInfo);
    }

    private void OnDisable()
    {
        PunManager.Instance.OnClueFoundByPlayer -= HandleClueFound;
        GameManager.main.OnNewRoundStarted -= HandleRoundChanged;
    }

    private void CheckFoundClues(ReconnectionInfo reconnectionInfo)
    {
        foreach (var roundSetup in setupsPerRound)
        {
            foreach (var clue in roundSetup.assignedClues)
            {
                var matchingClue =
                    reconnectionInfo.FoundClues.FirstOrDefault(foundClue => foundClue.ClueName == clue.Title);
                if (matchingClue == null) continue;

                SetClueFound(clue, matchingClue.FoundByPlayer);
            }
        }
    }

    [Serializable]
    public class RoundSetup
    {
        public Round roundIndex;
        public Clue[] assignedClues;
    }
}