using UnityEngine;

[RequireComponent(typeof(SearchPlace))]
public abstract class ClickeableClue : MonoBehaviour
{
    protected SearchPlace SearchPlace { get; private set; }
    public virtual bool IsClickEnabled { get; set; }

    protected virtual void Awake()
    {
        SearchPlace = GetComponent<SearchPlace>();
    }

    protected abstract void ToggleMapClick(bool value);
    public void EnableUI() => SearchPlace.EnableUI();

    private void OnEnable()
    {
        CameraManager.main.OnMovementToggled += ToggleMapClick;
    }

    private void OnDisable()
    {
        SetClickNotDrivenByCamera();
    }

    public void SetClickNotDrivenByCamera()
    {
        CameraManager.main.OnMovementToggled -= ToggleMapClick;
    }
}
