using System;
using Data;
using Data.Scripts;
using Managers;
using Screens;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using WebSocketSharp;

public class SearchPlacePanel : ScreenTemplate
{
    [SerializeField] private TMP_Text txtPlaceName;
    [SerializeField] private Image imgPlaceName;
    [SerializeField] private Button btnSearch;
    [SerializeField] private Button btnJoinPrivateRoom;

    private Func<Clue> _cbOnClueDisplayed;
    private int _cluesRemaining;
    private string _privateRoomSuffix;
    private SpriteRenderer _privateRoomMap;

    public override void Init()
    {
        base.Init();
        btnSearch.onClick.AddListener(DisplayClue);
        if (btnJoinPrivateRoom != null)
        {
            btnJoinPrivateRoom.onClick.AddListener(JoinPrivateRoom);
        }
        CameraManager.main.ToggleMovementActive(false);
    }

    public override void Close()
    {
        base.Close();
        btnSearch.onClick.RemoveListener(DisplayClue);
        if (btnJoinPrivateRoom)
        {
            btnJoinPrivateRoom.onClick.RemoveListener(JoinPrivateRoom);
        }
        CameraManager.main.ToggleMovementActive(true);
    }

    public void Setup(SearchPlace place, Func<Clue> cbOnClueDisplayed, int cluesRemaining, string voiceRoomSuffix,
        SpriteRenderer bgPrivateRoom, GameScript.Character characterInfo = null)
    {

        //Debug.Log(" se abrio " + place.PlaceName);
        //Debug.Log(" pistas que quedan por descubrir "+ _cluesRemaining);
        //var x = _cluesRemaining = cluesRemaining;
        //Debug.Log("resultado " + x );
        Debug.LogError("setup for clue displayed");
        btnSearch.interactable = (_cluesRemaining = cluesRemaining) > 0;
        _cbOnClueDisplayed = cbOnClueDisplayed;
        var notCharacterSearchPlace = characterInfo == null;
        txtPlaceName.text = notCharacterSearchPlace ? place.PlaceName : $"{characterInfo.name}\n{characterInfo.gender}, {characterInfo.age}\n{characterInfo.occupation}";
        if (btnJoinPrivateRoom != null && !voiceRoomSuffix.IsNullOrEmpty())
        {
            btnJoinPrivateRoom.gameObject.SetActive(notCharacterSearchPlace);
        }
        if (place.Image)
        {
            imgPlaceName.sprite = place.Image;
        }
        else
        {
            Debug.LogError("no place image");
        }
        if (btnJoinPrivateRoom != null && voiceRoomSuffix.IsNullOrEmpty())
        {
            btnJoinPrivateRoom.gameObject.SetActive(false);
        }

       
        _privateRoomSuffix = voiceRoomSuffix;
        _privateRoomMap = bgPrivateRoom;
    }

    private void JoinPrivateRoom()
    {
        PrivateRoomManager.main.JoinPrivateRoom(_privateRoomSuffix, _privateRoomMap);
        ScreenManager.main.CloseScreen();
    }

    private void DisplayClue()
    {
        var foundPanel = ScreenManager.main.OpenScreen<ClueFoundPanel>();
        Debug.LogError("esto pasa hijo de su ");
        foundPanel.Setup(_cbOnClueDisplayed.Invoke());
        
        if (_cluesRemaining < 1) btnSearch.interactable = false;
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ScreenManager.main.CloseScreen();
        }
    }
}
