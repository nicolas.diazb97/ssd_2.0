using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemsHolder : MonoBehaviour
{
    public List<FilterItem> items;
    // Start is called before the first frame update
    void Awake()
    {
        items = GetComponentsInChildren<FilterItem>().ToList();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
