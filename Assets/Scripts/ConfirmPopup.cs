﻿using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmPopup : ScreenTemplate
{
    [SerializeField] private Button btnConfirm;
    [SerializeField] private TMP_Text txtDialogue;

    private bool _confirmed;

    public override void Init()
    {
        base.Init();
        btnConfirm.onClick.AddListener(OnConfirm);
    }

    public async Task WaitUntilConfirmedAsync(string dialogueMsg)
    {
        txtDialogue.text = dialogueMsg;
        while (!_confirmed)
        {
            await Task.Yield();
        }
        
        Close();
    }

    private void OnConfirm()
    {
        _confirmed = true;
    }

    public override void Close()
    {
        base.Close();
        txtDialogue.text = string.Empty;
        btnConfirm.onClick.RemoveListener(OnConfirm);
        _confirmed = false;
    }
}