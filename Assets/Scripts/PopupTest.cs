using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class PopupTest : MonoBehaviour
{
    public static List<string> myList;
    //[ListToPopUp(typeof(PopupTest), "myList")]
    public string popup;
    [ContextMenu("Create")]
    public void CreateNumberList()
    {
        myList = new List<string> { "1", "2", "3" };
    }
}