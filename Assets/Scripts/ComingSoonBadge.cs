using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComingSoonBadge : MonoBehaviour
{
    // Start is called before the first frame update
    public void SetBadge()
    {
        GetComponent<Image>().enabled = true;
    }
}
