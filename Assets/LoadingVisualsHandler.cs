using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingVisualsHandler : Singleton<LoadingVisualsHandler>
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public void SetState(bool _state)
    {
        GetComponentInChildren<LoadingState>(true).gameObject.SetActive(_state);
    }
}
