﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DebuggingEssentials
{
    [DefaultExecutionOrder(-99999999)]
    public class WindowManager : MonoBehaviour
    {
        public static WindowManager instance;
        public static EventHandling eventHandling = new EventHandling();
        public static Vector2 mousePos;
        static string tooltip = string.Empty;
        static int lastWindowId;

        public SO_WindowManager windowData;
        
        RuntimeInspector runtimeInspector;
        RuntimeConsole runtimeConsole;
        
        Vector2 size;

        void Awake()
        {
            if (windowData.useDontDestroyOnLoadScene) DontDestroyOnLoad(gameObject);

            RuntimeInspector.ResetStatic();
            NavigationCamera.ResetStatic();
            GUIChangeBool.ResetStatic();
            DrawEnum.ResetStatic();
            CullGroup.ResetStatic();
            HtmlDebug.ResetStatic();

            instance = this;
            runtimeInspector = GetComponentInChildren<RuntimeInspector>(true);

            if (runtimeInspector && !runtimeInspector.gameObject.activeInHierarchy)
            {
                SetActiveDeactive(runtimeInspector.gameObject);
            }

            runtimeConsole = GetComponentInChildren<RuntimeConsole>(true);
            RuntimeConsole.instance = runtimeConsole;

            if (runtimeConsole && !runtimeConsole.gameObject.activeInHierarchy)
            {
                SetActiveDeactive(runtimeConsole.gameObject);
            }

            if (!runtimeConsole && runtimeInspector) RuntimeConsole.accessLevel = AccessLevel.Admin;

            if (runtimeInspector || runtimeConsole)
            {
                RuntimeInspector.InitAssemblies();
            }

            HtmlDebug.timeSinceStartup.Stop();
            HtmlDebug.timeSinceStartup.Start();
        }

        void SetActiveDeactive(GameObject go)
        {
            go.SetActive(true);
            go.SetActive(false);
        }

        void OnDestroy()
        {
            if (instance == this) instance = null;
        }

        void Update()
        {
            HtmlDebug.frameTime = HtmlDebug.timeSinceStartup.ElapsedMilliseconds / 1000f;
            HtmlDebug.currentFrame = Time.frameCount;

            if (HtmlDebug.instance) HtmlDebug.instance.UpdateLogs();
            if (RuntimeConsole.instance) RuntimeConsole.instance.ManualUpdate();
            if (RuntimeInspector.instance) RuntimeInspector.instance.ManualUpdate();
        }

        void OnGUI()
        {
            float guiScale = windowData.guiScale.value;
            GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(guiScale, guiScale, guiScale));
            Event currentEvent = Event.current;

            if (currentEvent.type == EventType.Layout) GUIChangeBool.ApplyUpdates();

            mousePos = currentEvent.mousePosition;

            if (runtimeInspector) runtimeInspector.MyOnGUI();
            if (runtimeConsole) runtimeConsole.MyOnGUI();

            if (DoWindowsContainMousePos())
            {
                eventHandling.OnEnter();
            }
            else eventHandling.OnExit();

            if (runtimeInspector)
            {
                if (runtimeInspector.windowData.showTooltip) DrawTooltip(mousePos);
            }

            GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);
        }

        public static bool DoWindowsContainMousePos()
        {
            if (instance.runtimeInspector)
            {
                WindowSettings hierarchyWindow = instance.runtimeInspector.windowData.hierarchyWindow;
                WindowSettings inspectorWindow = instance.runtimeInspector.windowData.inspectorWindow;

                if (RuntimeInspector.show && (hierarchyWindow.ContainsMousePos(mousePos) || (RuntimeInspector.ShouldInspectorWindowShow() && inspectorWindow.ContainsMousePos(mousePos)))) return true;
            }
            if (instance.runtimeConsole) 
            {
                WindowSettings consoleWindow = instance.runtimeConsole.windowData.consoleWindow;
                if (RuntimeConsole.show && consoleWindow.ContainsMousePos(mousePos)) return true;
            }

            return false;
        }

        public static void SetToolTip(int windowId)
        {
            if (GUI.tooltip.Length > 0)
            {
                lastWindowId = windowId;
                tooltip = GUI.tooltip; 
            }
            else
            {
                if (windowId == lastWindowId) tooltip = string.Empty;
            }
        }

        void DrawTooltip(Vector2 mousePos)
        {
            if (tooltip.Length > 0)
            {
                size = GUI.skin.label.CalcSize(Helper.GetGUIContent(tooltip));
                size.x += 10;
                size.y += 2.5f;
                Rect rect = new Rect(mousePos.x, mousePos.y, size.x, size.y);
                rect.x += 5;
                rect.y += 20;

                rect = GUI.Window(423423, rect, DoWindow, GUIContent.none);
            }
        }

        void DoWindow(int windowId)
        {
            GUI.BringWindowToFront(windowId);
            Rect rect = new Rect(0, 0, size.x, size.y);
            GUI.backgroundColor = new Color(0.1f, 0.1f, 0.1f, 0.5f);
            GUI.Box(rect, GUIContent.none);
            GUI.backgroundColor = Color.white;
            rect.x += 5;
            GUI.Label(rect, tooltip);
        }

        public class EventHandling
        {
            FastList<EventSystem> eventSystemList = new FastList<EventSystem>();
            public bool hasEntered;

            public void OnEnter()
            {
                if (hasEntered) return;
                hasEntered = true;

                EventSystem eventSystem = EventSystem.current;
                if (eventSystem == null) return;

                if (eventSystem.enabled)
                {
                    eventSystem.enabled = false;
                    eventSystemList.Add(eventSystem);
                }
            }

            public void OnExit()
            {
                if (!hasEntered) return;
                hasEntered = false;

                for (int i = 0; i < eventSystemList.Count; i++)
                {
                    eventSystemList.items[i].enabled = true;
                }

                eventSystemList.Clear();
            }
        }
    }
}
