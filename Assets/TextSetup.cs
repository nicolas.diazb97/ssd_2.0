using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Data.Scripts;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class TextSetup : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI TextToDisplay;

    private string Longtext;
    private string Shorttext;
    private List<string[]> TextSplited = new List<string[]>();
    private bool Changetextbool = false;
    [SerializeField] private RectTransform readingContent;
    private string textUnformat;
    private string theWord;

    public void SetupText(GameScript.ParagraphStruct TextToSetup, string changevalue, string longtext)
    {
        transform.localScale = Vector3.one;
        var temp123 = changevalue.Substring(0, 1).ToUpper() + changevalue.Substring(1).ToLower();
        textUnformat = TextToSetup.Text;
        theWord = changevalue;
        Longtext = longtext.Replace(changevalue, $"<mark=yellow>{changevalue}</mark>"); ; ;
        Longtext = Longtext.Replace(temp123, $"<mark=yellow>{temp123}</mark>"); ; ;
        Shorttext = TextToSetup.Text.Replace(changevalue, $"<mark=yellow>{changevalue}</mark>"); ;
        Shorttext = Shorttext.Replace(temp123, $"<mark=yellow>{temp123}</mark>"); ;

        /// Shorttext = Shorttext + "...";

        //   string[] temp = Shorttext.Split('.');
        /*    List<string> temp = new List<string>();

          var palabras = Shorttext.Split(' ');
          int index = 0;
          foreach (var palabra in palabras)
          {
              string tempToadd ="";
              if (index < 15)
              {
                  tempToadd = tempToadd + palabra;
                  index++;
              }
              else
              {
                  temp.Add(tempToadd);
                  index = 0;
                  tempToadd = "";
                  tempToadd = tempToadd + palabra;
              }
          } */

        string[] temp = Shorttext.Split(',');
        foreach (var VARIABLE in temp)
        {
            if (VARIABLE.Contains(changevalue) || VARIABLE.Contains(temp123))
            {
                if (!string.IsNullOrEmpty(VARIABLE))
                {
                    // TextToDisplay.SetText(VARIABLE);
                    TextToDisplay.SetText(Shorttext);
                    break;
                }
            }
        }
    }

    public void ChangeText()
    {
        /*
        if (!Changetextbool)
        {
            TextToDisplay.SetText(Longtext);
            Changetextbool = true;
            Debug.Log("ShowLongText");
        }
        else
        {

            TextToDisplay.SetText(Shorttext);
            Changetextbool = false;
            Debug.Log("ShowShortText");
        }
        */
        StartCoroutine(JumpText());

    }

    IEnumerator JumpText()
    {
        TextToDisplay.text = "";
        //trae todo el texto y lo pasa a variable
        string mainString = ScriptsOrganizer.main.DescriptionTemplate();
        //encuentra el indice del primer caracter de la coincidencia de la cadena a buscar
        int first = mainString.IndexOf(textUnformat);
        //pone la primera parte del texto
        TextToDisplay.text = mainString.Substring(0, first);
        yield return new WaitForEndOfFrame();
        //retorna cuantas lineas van hasta ahi
        var lines = TextToDisplay.textInfo.lineCount;
        //agrega la parte que se busco, resaltandola
        textUnformat = textUnformat.Replace(theWord, $"<mark=yellow>{theWord}</mark>");
        TextToDisplay.text += textUnformat;
        //agrega la parte final
        Debug.Log($"total: {mainString.Length} start: {first + textUnformat.Length} last: {mainString.Length}");
        //convierte la cadena principal a char array
        char[] ch = new char[mainString.Length];

        for (int i = 0; i < mainString.Length; i++)
        {
            ch[i] = mainString[i];
        }
        var lastString = "";

        for (int i = (first + textUnformat.Length); i < ch.Length; i++)
        {
            lastString += ch[i];
        }
        Debug.Log($"lastSting: {lastString}");
        TextToDisplay.text += lastString;

        yield return new WaitForEndOfFrame();

        Transform thePather = transform.parent;
        int son = transform.GetSiblingIndex();
        for (int i = 0; i < thePather.childCount; i++)
        {
            if (i != son)
            {
                Destroy(transform.parent.GetChild(i).gameObject);
            }
        }

        yield return new WaitForEndOfFrame();

        readingContent = TextToDisplay.GetComponentInParent<RectTransform>();
        readingContent.localPosition = new Vector2(readingContent.localPosition.x, readingContent.localPosition.y) + new Vector2(0, 63 * (lines - 10));


    }

    public void SetText(string TextToset)
    {

        TextToDisplay.SetText(TextToset);
        var buton = transform.GetComponent<UnityEngine.UI.Button>();
        buton.interactable = false;
    }

    public float downScroll;
    [ContextMenu("DownScroll")]
    public void DownScroll()
    {
        Debug.Log($"line space: {TextToDisplay.lineSpacing}");
        readingContent.localPosition = new Vector2(readingContent.localPosition.x, readingContent.localPosition.y) + new Vector2(0, downScroll);
    }

}
