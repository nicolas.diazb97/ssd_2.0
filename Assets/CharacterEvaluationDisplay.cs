using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Managers;

public class CharacterEvaluationDisplay : MonoBehaviour
{
    [SerializeField] private Image characterImage;
    [SerializeField] private Image mpvImage;
    [SerializeField] private Image mvpImage;
    [SerializeField] private Button voteButton;
    [SerializeField] private TextMeshProUGUI characterName;
    [SerializeField] private TextMeshProUGUI points;
    [SerializeField] private TextMeshProUGUI exp;
    private EvaluationScreen evScreen;
    public int score;
    public string characterNameString;

    public Button VoteButton => voteButton;

    public void SetUp(Sprite characterImage, string characterName, string points, string exp, EvaluationScreen evScreen)
    {
        this.characterImage.sprite = characterImage;
        this.characterName.text = characterName;
        this.points.text = points;
        this.exp.text = exp;
        this.evScreen = evScreen;
        voteButton.onClick.AddListener(SendVote);
        DataManager.main.OnMvpVote += UpdateMVP;
        int number;
        this.characterNameString = characterName;
    }

    private void OnDestroy()
    {
        DataManager.main.OnMvpVote -= UpdateMVP;
    }

    public void UpdateMVP()
    {
        //mpvImage.enabled = DataManager.main.GetMostVotedForMvp().name == characterName.text;
    }
    public void AssignMVPState(bool _state)
    {
        mvpImage.enabled = _state;
        Debug.LogError("asignado" + _state);
    }
    public void SendVote()
    {
        Debug.LogError("send voteeee");
        evScreen.DisableAllLikes();
        Assignation assignation = new Assignation
        {
            name = characterName.text,
            votes = 1
        };
        PunManager.Instance.AddMVPVote(JsonUtility.ToJson(assignation));
    }
}
