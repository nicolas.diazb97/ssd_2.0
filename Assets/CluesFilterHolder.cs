using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CluesFilterHolder : Singleton<CluesFilterHolder>
{
    public List<FilterVisual> filters;
    // Start is called before the first frame update
    private void OnEnable()
    {
        filters = GetComponentsInChildren<FilterVisual>().ToList();
    }
    public void OnFilterSelected(string _name)
    {
        filters = GetComponentsInChildren<FilterVisual>().ToList();
        filters.ForEach(f => 
        {
            if(f.filtername != _name)
            {
                f.ResetFilter();
            }
        });
    }
}
