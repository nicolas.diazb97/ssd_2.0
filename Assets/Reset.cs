using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reset : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    private void Update()
    {
        
        Reconnect();
    }

    public void Reconnect()
    {
        Debug.Log("reconnect scene screen");
        if (!PhotonNetwork.IsConnected)
        {
            SceneManager.LoadScene("LoginScreen");
        }
    }
    
    
}
