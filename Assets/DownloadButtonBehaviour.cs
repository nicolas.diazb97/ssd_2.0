using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Data.Scripts;
using Managers;
using TMPro;

public class DownloadButtonBehaviour : MonoBehaviour
{
    public RemoteDownload remoteHandler;

    public UnityEvent OnDownloadFinish;

    public TextMeshProUGUI downloadPercentageText;

    private bool isStillDownloading;

    int counter = 0;


    private void OnEnable()
    {
        remoteHandler = FindObjectOfType<RemoteDownload>();
        remoteHandler.CheckDownload(DataManager.main.GetCurrScript().scriptIndex, OnDownloadFinish);
    }
    // Start is called before the first frame update
    public void DownloadScript()
    {
        isStillDownloading = true;
        remoteHandler.TryToLoadScriptAuto(DataManager.main.GetCurrScript().scriptIndex, OnDownloadFinish);
        StartCoroutine(StartDownloading());
        OnDownloadFinish.AddListener(() =>
        {
            isStillDownloading = false;
        });
    }
    public void DownloadScript2()
    {
        isStillDownloading = true;
        remoteHandler.CheckDownload2(DataManager.main.GetCurrScript().scriptIndex, OnDownloadFinish);
    }
    IEnumerator StartDownloading()
    {
        yield return new WaitForSecondsRealtime(0.3f);
        if (isStillDownloading)
        {
            counter++;
            downloadPercentageText.text = counter + "%";
            if (counter < 100)
            {
                StartCoroutine(StartDownloading());
            }
        }
    }
}
