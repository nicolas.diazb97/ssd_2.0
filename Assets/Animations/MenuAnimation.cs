using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAnimation : MonoBehaviour
{
    public Animator homeAnimator;
    public Animator scriptsAnimator;
    public Animator profileAnimator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void HomeButtonGrows()
    {
        homeAnimator.SetBool("State", true);
        if(scriptsAnimator.GetBool("State") == true)
        {
            scriptsAnimator.SetBool("State", false);
        }
        if (profileAnimator.GetBool("State") == true)
        {
            profileAnimator.SetBool("State", false);
        }
    }
    public void ScriptsButtonGrows()
    {
        scriptsAnimator.SetBool("State", true);
        if (homeAnimator.GetBool("State") == true)
        {
            homeAnimator.SetBool("State", false);
        }
        if (profileAnimator.GetBool("State") == true)
        {
            profileAnimator.SetBool("State", false);
        }
    }
    public void ProfileButtonGrows()
    {
        profileAnimator.SetBool("State", true);
        if (homeAnimator.GetBool("State") == true)
        {
            homeAnimator.SetBool("State", false);
        }
        if (scriptsAnimator.GetBool("State") == true)
        {
            scriptsAnimator.SetBool("State", false);
        }
    }
}
