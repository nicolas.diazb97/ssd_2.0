using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FilterVisual : MonoBehaviour
{
    public bool selected = false;
    public string filtername;
    // Start is called before the first frame update
    public void ResetFilter()
    {
        if (filtername == "allclues")
        {
            selected = false;
            GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
        }
    }
    public void Selected(bool state)
    {
        if (filtername == "allclues")
        {

            if (state)
            {
                GetComponentInChildren<TextMeshProUGUI>().color = Color.black;
            }
            else
            {

                GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
            }
        }
    }
}
